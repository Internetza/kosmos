FUNCTION_BLOCK getpybool
  VAR
    getpy : python_gear;
    latch : SR;
  END_VAR
  VAR_INPUT
    name : STRING;
  END_VAR
  VAR_OUTPUT
    val : BOOL;
  END_VAR
  VAR
    EQ41_ENO : BOOL;
    EQ41_OUT : BOOL;
    EQ3_ENO : BOOL;
    EQ3_OUT : BOOL;
  END_VAR

  getpy(N := 100, TRIG := BOOL#TRUE, CODE := name);
  EQ41_OUT := EQ(EN := getpy.ACK, IN1 := getpy.RESULT, IN2 := 'True', ENO => EQ41_ENO);
  EQ3_OUT := EQ(EN := getpy.ACK, IN1 := getpy.RESULT, IN2 := 'False', ENO => EQ3_ENO);
  latch(S1 := EQ41_OUT, R := EQ3_OUT);
  val := latch.Q1;
END_FUNCTION_BLOCK

PROGRAM main
  VAR
    Done : BOOL;
  END_VAR
  VAR
    SlaveX AT %IW1.0.3.0 : AXIS_REF;
  END_VAR
  VAR
    SlaveY : AXIS_REF;
  END_VAR
  VAR
    Master AT %IW1.0.4.0 : AXIS_REF;
  END_VAR
  VAR
    switch : MC_Power;
    readpos : MC_ReadActualPosition;
    XPn : LREAL;
    YPn : LREAL;
    mPn : LREAL;
    XSetPoint : LREAL;
    XActualPos : LREAL;
    YVn : LREAL;
    mVn : LREAL;
    setpos : MC_SetPosition;
    readspeed : MC_ReadParameter;
    bt1 : getpybool;
    Block1 : getpybool;
    MC_Power0 : MC_Power;
    MC_ReadActualPosition0 : MC_ReadActualPosition;
    MC_SetPosition0 : MC_SetPosition;
    MC_ReadParameter0 : MC_ReadParameter;
    MC_MoveAbsolute0 : MC_MoveAbsolute;
    MC_CAM_REF_FROM_CSV0 : MC_CAM_REF_FROM_CSV;
    MC_CamTableSelect0 : MC_CamTableSelect;
    MC_CamIn0 : MC_CamIn;
    MC_CamOut0 : MC_CamOut;
    python_poll0 : python_gear;
    MC_CamTableSelect1 : MC_CamTableSelect;
    MC_CamIn1 : MC_CamIn;
    MC_CamOut1 : MC_CamOut;
    MC_Power1 : MC_Power;
    MC_ReadActualPosition1 : MC_ReadActualPosition;
    MC_SetPosition1 : MC_SetPosition;
    MC_ReadParameter1 : MC_ReadParameter;
    MC_Halt0 : MC_Halt;
    MC_ReadParameter2 : MC_ReadParameter;
    MC_Sim0 : MC_Sim;
    getpybool0 : getpybool;
    readpos_Axis : AXIS_REF;
    readspeed_Axis : AXIS_REF;
    MC_ReadActualPosition1_Axis : AXIS_REF;
    MC_ReadActualPosition0_Axis : AXIS_REF;
    switch_Axis : AXIS_REF;
    MC_Power0_Axis : AXIS_REF;
    MC_Power1_Axis : AXIS_REF;
    F_TRIG1 : F_TRIG;
    MC_CAM_REF_FROM_CSV0_CamTable : MC_CAM_REF;
    MC_CamTableSelect1_Master : AXIS_REF;
    MC_CamTableSelect1_Slave : AXIS_REF;
    F_TRIG2 : F_TRIG;
    LREAL_TO_STRING32_OUT : STRING;
    LREAL_TO_STRING29_OUT : STRING;
    LREAL_TO_STRING68_OUT : STRING;
    CONCAT7_OUT : STRING;
    MC_CamTableSelect0_Master : AXIS_REF;
    MC_CamTableSelect0_Slave : AXIS_REF;
    F_TRIG3 : F_TRIG;
  END_VAR

  MC_Sim0();
  SlaveY := MC_Sim0.Axis;
  readpos(Axis := SlaveX, Enable := BOOL#TRUE);
  readpos_Axis := readpos.Axis;
  readspeed(Axis := readpos_Axis, Enable := BOOL#TRUE, ParameterNumber := 1001);
  XSetPoint := readspeed.Value;
  readspeed_Axis := readspeed.Axis;
  MC_ReadParameter2(Axis := readspeed_Axis, Enable := BOOL#TRUE, ParameterNumber := 1008);
  XActualPos := MC_ReadParameter2.Value;
  MC_ReadActualPosition1(Axis := SlaveY, Enable := BOOL#TRUE);
  MC_ReadActualPosition1_Axis := MC_ReadActualPosition1.Axis;
  MC_ReadParameter1(Axis := MC_ReadActualPosition1_Axis, Enable := BOOL#TRUE, ParameterNumber := 10);
  YVn := MC_ReadParameter1.Value;
  MC_ReadActualPosition0(Axis := Master, Enable := BOOL#TRUE);
  MC_ReadActualPosition0_Axis := MC_ReadActualPosition0.Axis;
  MC_ReadParameter0(Axis := MC_ReadActualPosition0_Axis, Enable := BOOL#TRUE, ParameterNumber := 10);
  mVn := MC_ReadParameter0.Value;
  XPn := readpos.Position;
  mPn := MC_ReadActualPosition0.Position;
  YPn := MC_ReadActualPosition1.Position;
  bt1(name := 'wxglade_hmi.button_1.GetValue()');
  Block1(name := 'Cycle');
  getpybool0(name := 'SetPos');
  MC_MoveAbsolute0(Axis := Master, Execute := Block1.val, Position := 3600, Velocity := 10.0, Acceleration := 10.0, Deceleration := 10.0);
  switch(Axis := SlaveX, Enable := bt1.val);
  switch_Axis := switch.Axis;
  setpos(Axis := switch_Axis, Execute := getpybool0.val, Position := 0.0);
  MC_Power0(Axis := Master);
  MC_Power0_Axis := MC_Power0.Axis;
  MC_SetPosition0(Axis := MC_Power0_Axis, Execute := getpybool0.val, Position := 0.0);
  MC_Power1(Axis := SlaveY, Enable := bt1.val);
  MC_Power1_Axis := MC_Power1.Axis;
  MC_SetPosition1(Axis := MC_Power1_Axis, Execute := getpybool0.val, Position := 0.0);
  F_TRIG1(CLK := Block1.val);
  MC_Halt0(Axis := Master, Execute := F_TRIG1.Q, Deceleration := 100.0);
  MC_CAM_REF_FROM_CSV0(FileName := 'test.csv');
  MC_CAM_REF_FROM_CSV0_CamTable := MC_CAM_REF_FROM_CSV0.CamTable;
  MC_CamTableSelect1(Master := Master, Slave := SlaveX, CamTable := MC_CAM_REF_FROM_CSV0_CamTable, Execute := MC_CAM_REF_FROM_CSV0.Valid, Periodic := BOOL#TRUE, MasterAbsolute := BOOL#TRUE, SlaveAbsolute := BOOL#TRUE);
  MC_CamTableSelect1_Master := MC_CamTableSelect1.Master;
  MC_CamTableSelect1_Slave := MC_CamTableSelect1.Slave;
  MC_CamIn1(Master := MC_CamTableSelect1_Master, Slave := MC_CamTableSelect1_Slave, Execute := Block1.val, SlaveOffset := 0.0, SlaveScaling := 180.0, MasterValueSource := mcActualValue, CamTableID := MC_CamTableSelect1.CamTableID);
  F_TRIG2(CLK := Block1.val);
  MC_CamOut1(Slave := SlaveX, Execute := F_TRIG2.Q);
  LREAL_TO_STRING32_OUT := LREAL_TO_STRING(mPn);
  LREAL_TO_STRING29_OUT := LREAL_TO_STRING(XPn);
  LREAL_TO_STRING68_OUT := LREAL_TO_STRING(YPn);
  CONCAT7_OUT := CONCAT('SetAngle(', LREAL_TO_STRING32_OUT, ',', LREAL_TO_STRING29_OUT, ',', LREAL_TO_STRING68_OUT, ')');
  python_poll0(N := 50, TRIG := BOOL#TRUE, CODE := CONCAT7_OUT);
  MC_CamTableSelect0(Master := Master, Slave := SlaveY, CamTable := MC_CAM_REF_FROM_CSV0_CamTable, Execute := MC_CAM_REF_FROM_CSV0.Valid, Periodic := BOOL#TRUE, MasterAbsolute := BOOL#TRUE, SlaveAbsolute := BOOL#TRUE);
  MC_CamTableSelect0_Master := MC_CamTableSelect0.Master;
  MC_CamTableSelect0_Slave := MC_CamTableSelect0.Slave;
  MC_CamIn0(Master := MC_CamTableSelect0_Master, Slave := MC_CamTableSelect0_Slave, Execute := Block1.val, SlaveOffset := 0.0, SlaveScaling := 180.0, MasterValueSource := mcActualValue, CamTableID := MC_CamTableSelect0.CamTableID);
  F_TRIG3(CLK := Block1.val);
  MC_CamOut0(Slave := SlaveY, Execute := F_TRIG3.Q);
END_PROGRAM


CONFIGURATION config
  VAR_GLOBAL
    GetTorqueLimit_1_0_3 : EtherLabGetTorqueLimit;
    SetTorqueLimit_1_0_3 : EtherLabSetTorqueLimit;
    GetTorqueLimit_1_0_4 : EtherLabGetTorqueLimit;
    SetTorqueLimit_1_0_4 : EtherLabSetTorqueLimit;
  END_VAR

  RESOURCE resource1 ON PLC
    TASK tsk1(INTERVAL := T#1MS,PRIORITY := 0);
    PROGRAM inst1 WITH tsk1 : main;
  END_RESOURCE
END_CONFIGURATION
