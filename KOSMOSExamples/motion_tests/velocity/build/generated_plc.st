PROGRAM main
  VAR
    clock : ULINT;
    Done : BOOL;
    Axis : AXIS_REF;
    switch : MC_Power;
    readpos : MC_ReadActualPosition;
    Pn : LREAL;
    Vn : LREAL;
    setpos : MC_SetPosition;
    readspeed : MC_ReadParameter;
    my_simulated_axis : MC_Sim;
    speeed : MC_MoveVelocity;
    ADD6_OUT : ULINT;
    readpos_Axis : AXIS_REF;
    GT3_OUT : BOOL;
    switch_Axis : AXIS_REF;
    setpos_Axis : AXIS_REF;
  END_VAR

  my_simulated_axis();
  Axis := my_simulated_axis.Axis;
  ADD6_OUT := ADD(clock, 1);
  clock := ADD6_OUT;
  readpos(Axis := Axis, Enable := BOOL#TRUE);
  Pn := readpos.Position;
  readpos_Axis := readpos.Axis;
  readspeed(Axis := readpos_Axis, Enable := BOOL#TRUE, ParameterNumber := 10);
  Vn := readspeed.Value;
  GT3_OUT := GT(clock, 10);
  switch(Axis := Axis, Enable := GT3_OUT);
  switch_Axis := switch.Axis;
  setpos(Axis := switch_Axis, Execute := switch.Status, Position := 1.0);
  setpos_Axis := setpos.Axis;
  speeed(Axis := setpos_Axis, Execute := setpos.Done, Velocity := 5.0, Acceleration := 20.0, Deceleration := 20.0);
END_PROGRAM


CONFIGURATION conf

  RESOURCE res ON PLC
    TASK milisecond(INTERVAL := T#1ms,PRIORITY := 0);
    PROGRAM maininstance WITH milisecond : main;
  END_RESOURCE
END_CONFIGURATION
