PROGRAM main
  VAR
    clock : ULINT;
    Done : BOOL;
    PosAxis : AXIS_REF;
    NegAxis : AXIS_REF;
    moveA : MC_MoveAbsolute;
    switch : MC_Power;
    readpos : MC_ReadActualPosition;
    PnNeg : LREAL;
    PnPos : LREAL;
    VnNeg : LREAL;
    VnPos : LREAL;
    setpos : MC_SetPosition;
    readspeed : MC_ReadParameter;
    my_simulated_axis : MC_Sim;
    MC_MoveContinuousAbsolute0 : MC_MoveContinuousAbsolute;
    MC_MoveAbsolute0 : MC_MoveAbsolute;
    MC_Power0 : MC_Power;
    MC_ReadActualPosition0 : MC_ReadActualPosition;
    MC_SetPosition0 : MC_SetPosition;
    MC_ReadParameter0 : MC_ReadParameter;
    MC_Sim0 : MC_Sim;
    MC_MoveContinuousAbsolute1 : MC_MoveContinuousAbsolute;
    MC_MoveContinuousAbsolute2 : MC_MoveContinuousAbsolute;
    MC_MoveContinuousAbsolute3 : MC_MoveContinuousAbsolute;
    MC_SetOverride0 : MC_SetOverride;
    MC_SetOverride1 : MC_SetOverride;
    ADD6_OUT : ULINT;
    readpos_Axis : AXIS_REF;
    MC_ReadActualPosition0_Axis : AXIS_REF;
    GT3_OUT : BOOL;
    switch_Axis : AXIS_REF;
    setpos_Axis : AXIS_REF;
    GT9_OUT : BOOL;
    SEL39_OUT : LREAL;
    moveA_Axis : AXIS_REF;
    MC_MoveContinuousAbsolute0_Axis : AXIS_REF;
    GT81_OUT : BOOL;
    GT33_OUT : BOOL;
    MC_Power0_Axis : AXIS_REF;
    MC_SetPosition0_Axis : AXIS_REF;
    LT44_OUT : BOOL;
    SEL66_OUT : LREAL;
    MC_MoveAbsolute0_Axis : AXIS_REF;
    MC_MoveContinuousAbsolute1_Axis : AXIS_REF;
    LT88_OUT : BOOL;
  END_VAR

  my_simulated_axis();
  PosAxis := my_simulated_axis.Axis;
  ADD6_OUT := ADD(clock, ULINT#1);
  clock := ADD6_OUT;
  readpos(Axis := PosAxis, Enable := BOOL#TRUE);
  PnPos := readpos.Position;
  readpos_Axis := readpos.Axis;
  readspeed(Axis := readpos_Axis, Enable := BOOL#TRUE, ParameterNumber := 10);
  VnPos := readspeed.Value;
  MC_Sim0();
  NegAxis := MC_Sim0.Axis;
  MC_ReadActualPosition0(Axis := NegAxis, Enable := BOOL#TRUE);
  PnNeg := MC_ReadActualPosition0.Position;
  MC_ReadActualPosition0_Axis := MC_ReadActualPosition0.Axis;
  MC_ReadParameter0(Axis := MC_ReadActualPosition0_Axis, Enable := BOOL#TRUE, ParameterNumber := 10);
  VnNeg := MC_ReadParameter0.Value;
  GT3_OUT := GT(clock, 10);
  switch(Axis := PosAxis, Enable := GT3_OUT);
  switch_Axis := switch.Axis;
  setpos(Axis := switch_Axis, Execute := switch.Status, Position := 1.0);
  setpos_Axis := setpos.Axis;
  GT9_OUT := GT(PnPos, 25.0);
  SEL39_OUT := SEL(GT9_OUT, LREAL#5.0, LREAL#10.0);
  moveA(Axis := setpos_Axis, Execute := setpos.Done, ContinuousUpdate := BOOL#TRUE, Position := 50.0, Velocity := SEL39_OUT, Acceleration := 20.0, Deceleration := 10.0);
  moveA_Axis := moveA.Axis;
  MC_MoveContinuousAbsolute0(Axis := moveA_Axis, Execute := moveA.Done, Position := 55.0, EndVelocity := 20.0, Velocity := 20.0, Acceleration := 20.0, Deceleration := 10.0);
  MC_MoveContinuousAbsolute0_Axis := MC_MoveContinuousAbsolute0.Axis;
  MC_MoveContinuousAbsolute2(Axis := MC_MoveContinuousAbsolute0_Axis, Execute := MC_MoveContinuousAbsolute0.InEndVelocity, Position := 100.0, EndVelocity := -20.0, Velocity := 20.0, Acceleration := 20.0, Deceleration := 10.0);
  GT81_OUT := GT(PnPos, 10.0);
  MC_SetOverride0(Axis := PosAxis, Enable := GT81_OUT, VelFactor := 2.0, AccFactor := 2.0);
  GT33_OUT := GT(clock, 10);
  MC_Power0(Axis := NegAxis, Enable := GT33_OUT);
  MC_Power0_Axis := MC_Power0.Axis;
  MC_SetPosition0(Axis := MC_Power0_Axis, Execute := MC_Power0.Status, Position := -1.0);
  MC_SetPosition0_Axis := MC_SetPosition0.Axis;
  LT44_OUT := LT(PnNeg, -25.0);
  SEL66_OUT := SEL(LT44_OUT, LREAL#5.0, LREAL#10.0);
  MC_MoveAbsolute0(Axis := MC_SetPosition0_Axis, Execute := MC_SetPosition0.Done, ContinuousUpdate := BOOL#TRUE, Position := -50.0, Velocity := SEL66_OUT, Acceleration := 20.0, Deceleration := 10.0);
  MC_MoveAbsolute0_Axis := MC_MoveAbsolute0.Axis;
  MC_MoveContinuousAbsolute1(Axis := MC_MoveAbsolute0_Axis, Execute := MC_MoveAbsolute0.Done, Position := -55.0, EndVelocity := -20.0, Velocity := 20.0, Acceleration := 20.0, Deceleration := 10.0);
  MC_MoveContinuousAbsolute1_Axis := MC_MoveContinuousAbsolute1.Axis;
  MC_MoveContinuousAbsolute3(Axis := MC_MoveContinuousAbsolute1_Axis, Execute := MC_MoveContinuousAbsolute1.InEndVelocity, Position := -100.0, EndVelocity := 20.0, Velocity := 20.0, Acceleration := 20.0, Deceleration := 10.0);
  LT88_OUT := LT(PnNeg, -10.0);
  MC_SetOverride1(Axis := NegAxis, Enable := LT88_OUT, VelFactor := 2.0, AccFactor := 2.0);
END_PROGRAM


CONFIGURATION conf

  RESOURCE res ON PLC
    TASK milisecond(INTERVAL := T#10ms,PRIORITY := 0);
    PROGRAM maininstance WITH milisecond : main;
  END_RESOURCE
END_CONFIGURATION
