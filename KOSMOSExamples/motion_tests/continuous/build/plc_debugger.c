/*
 * DEBUGGER code
 * 
 * On "publish", when buffer is free, debugger stores arbitrary variables 
 * content into, and mark this buffer as filled
 * 
 * 
 * Buffer content is read asynchronously, (from non real time part), 
 * and then buffer marked free again.
 *  
 * 
 * */
#include "iec_types_all.h"
#include "POUS.h"
#include "retain.h"
/*for memcpy*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define BUFFER_SIZE 899

/* Atomically accessed variable for buffer state */
#define BUFFER_FREE 0
#define BUFFER_BUSY 1
static long buffer_state = BUFFER_FREE;

/* The buffer itself */
char debug_buffer[BUFFER_SIZE];

/* Buffer's cursor*/
static char* buffer_cursor = debug_buffer;

/* Retain's valuables */
static unsigned int retain_offset = 0;
static int retain_channel = 0;
#ifdef __XENO__
retain_struct_t *in_data;
int validate_flag;
#endif

/***
 * Declare programs 
 **/
extern MAIN RES__MAININSTANCE;

/***
 * Declare global variables from resources and conf 
 **/
extern MAIN RES__MAININSTANCE;

typedef const struct {
    void *ptr;
    __IEC_types_enum type;
} dbgvardsc_t;

static dbgvardsc_t dbgvardsc[] = {
{&(RES__MAININSTANCE.CLOCK), ULINT_ENUM},
{&(RES__MAININSTANCE.DONE), BOOL_ENUM},
{&(RES__MAININSTANCE.POSAXIS), INT_ENUM},
{&(RES__MAININSTANCE.NEGAXIS), INT_ENUM},
{&(RES__MAININSTANCE.MOVEA.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MOVEA.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MOVEA.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MOVEA.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.MOVEA.CONTINUOUSUPDATE), BOOL_ENUM},
{&(RES__MAININSTANCE.MOVEA.POSITION), LREAL_ENUM},
{&(RES__MAININSTANCE.MOVEA.VELOCITY), LREAL_ENUM},
{&(RES__MAININSTANCE.MOVEA.ACCELERATION), LREAL_ENUM},
{&(RES__MAININSTANCE.MOVEA.DECELERATION), LREAL_ENUM},
{&(RES__MAININSTANCE.MOVEA.JERK), LREAL_ENUM},
{&(RES__MAININSTANCE.MOVEA.DONE), BOOL_ENUM},
{&(RES__MAININSTANCE.MOVEA.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MOVEA.ACTIVE), BOOL_ENUM},
{&(RES__MAININSTANCE.MOVEA.COMMANDABORTED), BOOL_ENUM},
{&(RES__MAININSTANCE.MOVEA.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MOVEA.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MOVEA.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MOVEA.DONE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MOVEA.ACTIVE0), BOOL_ENUM},
{&(RES__MAININSTANCE.SWITCH.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.SWITCH.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.SWITCH.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.SWITCH.ENABLE), BOOL_ENUM},
{&(RES__MAININSTANCE.SWITCH.ENABLEPOSITIVE), BOOL_ENUM},
{&(RES__MAININSTANCE.SWITCH.ENABLENEGATIVE), BOOL_ENUM},
{&(RES__MAININSTANCE.SWITCH.STATUS), BOOL_ENUM},
{&(RES__MAININSTANCE.SWITCH.VALID), BOOL_ENUM},
{&(RES__MAININSTANCE.SWITCH.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.SWITCH.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.READPOS.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.READPOS.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.READPOS.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.READPOS.ENABLE), BOOL_ENUM},
{&(RES__MAININSTANCE.READPOS.VALID), BOOL_ENUM},
{&(RES__MAININSTANCE.READPOS.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.READPOS.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.READPOS.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.READPOS.POSITION), LREAL_ENUM},
{&(RES__MAININSTANCE.READPOS.ENABLE0), BOOL_ENUM},
{&(RES__MAININSTANCE.PNNEG), LREAL_ENUM},
{&(RES__MAININSTANCE.PNPOS), LREAL_ENUM},
{&(RES__MAININSTANCE.VNNEG), LREAL_ENUM},
{&(RES__MAININSTANCE.VNPOS), LREAL_ENUM},
{&(RES__MAININSTANCE.SETPOS.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.SETPOS.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.SETPOS.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.SETPOS.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.SETPOS.POSITION), LREAL_ENUM},
{&(RES__MAININSTANCE.SETPOS.RELATIVE), BOOL_ENUM},
{&(RES__MAININSTANCE.SETPOS.DONE), BOOL_ENUM},
{&(RES__MAININSTANCE.SETPOS.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.SETPOS.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.SETPOS.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.SETPOS.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.SETPOS.DONE0), BOOL_ENUM},
{&(RES__MAININSTANCE.READSPEED.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.READSPEED.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.READSPEED.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.READSPEED.ENABLE), BOOL_ENUM},
{&(RES__MAININSTANCE.READSPEED.PARAMETERNUMBER), INT_ENUM},
{&(RES__MAININSTANCE.READSPEED.VALID), BOOL_ENUM},
{&(RES__MAININSTANCE.READSPEED.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.READSPEED.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.READSPEED.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.READSPEED.VALUE), LREAL_ENUM},
{&(RES__MAININSTANCE.READSPEED.ENABLE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.MAXVEL), LREAL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.MAXACC), LREAL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.MAXDEC), LREAL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.SIMPARAM.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.SIMPARAM.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.SIMPARAM.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.SIMPARAM.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.SIMPARAM.PARAMETERNUMBER), INT_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.SIMPARAM.VALUE), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.SIMPARAM.DONE), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.SIMPARAM.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.SIMPARAM.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.SIMPARAM.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.SIMPARAM.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.SIMPARAM.DONE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.VELPARAM.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.VELPARAM.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.VELPARAM.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.VELPARAM.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.VELPARAM.PARAMETERNUMBER), INT_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.VELPARAM.VALUE), LREAL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.VELPARAM.DONE), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.VELPARAM.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.VELPARAM.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.VELPARAM.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.VELPARAM.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.VELPARAM.DONE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.ACCPARAM.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.ACCPARAM.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.ACCPARAM.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.ACCPARAM.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.ACCPARAM.PARAMETERNUMBER), INT_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.ACCPARAM.VALUE), LREAL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.ACCPARAM.DONE), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.ACCPARAM.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.ACCPARAM.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.ACCPARAM.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.ACCPARAM.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.ACCPARAM.DONE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.DECPARAM.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.DECPARAM.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.DECPARAM.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.DECPARAM.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.DECPARAM.PARAMETERNUMBER), INT_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.DECPARAM.VALUE), LREAL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.DECPARAM.DONE), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.DECPARAM.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.DECPARAM.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.DECPARAM.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.DECPARAM.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.DECPARAM.DONE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MY_SIMULATED_AXIS.DONE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.CONTINUOUSUPDATE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.POSITION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.ENDVELOCITY), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.VELOCITY), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.ACCELERATION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.DECELERATION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.JERK), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.INENDVELOCITY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.ACTIVE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.COMMANDABORTED), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.INENDVELOCITY0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0.ACTIVE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.CONTINUOUSUPDATE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.POSITION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.VELOCITY), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.ACCELERATION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.DECELERATION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.JERK), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.DONE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.ACTIVE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.COMMANDABORTED), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.DONE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0.ACTIVE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_POWER0.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_POWER0.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_POWER0.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_POWER0.ENABLE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_POWER0.ENABLEPOSITIVE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_POWER0.ENABLENEGATIVE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_POWER0.STATUS), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_POWER0.VALID), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_POWER0.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_POWER0.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MC_READACTUALPOSITION0.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_READACTUALPOSITION0.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_READACTUALPOSITION0.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_READACTUALPOSITION0.ENABLE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_READACTUALPOSITION0.VALID), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_READACTUALPOSITION0.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_READACTUALPOSITION0.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_READACTUALPOSITION0.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MC_READACTUALPOSITION0.POSITION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_READACTUALPOSITION0.ENABLE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETPOSITION0.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETPOSITION0.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETPOSITION0.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_SETPOSITION0.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETPOSITION0.POSITION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_SETPOSITION0.RELATIVE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETPOSITION0.DONE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETPOSITION0.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETPOSITION0.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETPOSITION0.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MC_SETPOSITION0.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETPOSITION0.DONE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_READPARAMETER0.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_READPARAMETER0.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_READPARAMETER0.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_READPARAMETER0.ENABLE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_READPARAMETER0.PARAMETERNUMBER), INT_ENUM},
{&(RES__MAININSTANCE.MC_READPARAMETER0.VALID), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_READPARAMETER0.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_READPARAMETER0.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_READPARAMETER0.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MC_READPARAMETER0.VALUE), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_READPARAMETER0.ENABLE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.MAXVEL), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.MAXACC), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.MAXDEC), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.SIMPARAM.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.SIMPARAM.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.SIMPARAM.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.SIMPARAM.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.SIMPARAM.PARAMETERNUMBER), INT_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.SIMPARAM.VALUE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.SIMPARAM.DONE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.SIMPARAM.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.SIMPARAM.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.SIMPARAM.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.SIMPARAM.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.SIMPARAM.DONE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.VELPARAM.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.VELPARAM.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.VELPARAM.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.VELPARAM.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.VELPARAM.PARAMETERNUMBER), INT_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.VELPARAM.VALUE), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.VELPARAM.DONE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.VELPARAM.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.VELPARAM.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.VELPARAM.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.VELPARAM.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.VELPARAM.DONE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.ACCPARAM.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.ACCPARAM.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.ACCPARAM.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.ACCPARAM.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.ACCPARAM.PARAMETERNUMBER), INT_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.ACCPARAM.VALUE), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.ACCPARAM.DONE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.ACCPARAM.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.ACCPARAM.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.ACCPARAM.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.ACCPARAM.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.ACCPARAM.DONE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.DECPARAM.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.DECPARAM.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.DECPARAM.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.DECPARAM.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.DECPARAM.PARAMETERNUMBER), INT_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.DECPARAM.VALUE), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.DECPARAM.DONE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.DECPARAM.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.DECPARAM.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.DECPARAM.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.DECPARAM.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.DECPARAM.DONE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SIM0.DONE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.CONTINUOUSUPDATE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.POSITION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.ENDVELOCITY), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.VELOCITY), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.ACCELERATION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.DECELERATION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.JERK), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.INENDVELOCITY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.ACTIVE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.COMMANDABORTED), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.INENDVELOCITY0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1.ACTIVE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.CONTINUOUSUPDATE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.POSITION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.ENDVELOCITY), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.VELOCITY), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.ACCELERATION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.DECELERATION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.JERK), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.INENDVELOCITY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.ACTIVE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.COMMANDABORTED), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.INENDVELOCITY0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE2.ACTIVE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.EXECUTE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.CONTINUOUSUPDATE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.POSITION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.ENDVELOCITY), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.VELOCITY), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.ACCELERATION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.DECELERATION), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.JERK), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.INENDVELOCITY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.ACTIVE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.COMMANDABORTED), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.EXECUTE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.INENDVELOCITY0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE3.ACTIVE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE0.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE0.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE0.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE0.ENABLE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE0.VELFACTOR), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE0.ACCFACTOR), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE0.JERKFACTOR), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE0.ENABLED), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE0.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE0.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE0.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE0.ENABLE0), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE1.EN), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE1.ENO), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE1.AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE1.ENABLE), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE1.VELFACTOR), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE1.ACCFACTOR), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE1.JERKFACTOR), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE1.ENABLED), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE1.BUSY), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE1.ERROR), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE1.ERRORID), WORD_ENUM},
{&(RES__MAININSTANCE.MC_SETOVERRIDE1.ENABLE0), BOOL_ENUM},
{&(RES__MAININSTANCE.ADD6_OUT), ULINT_ENUM},
{&(RES__MAININSTANCE.READPOS_AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_READACTUALPOSITION0_AXIS), INT_ENUM},
{&(RES__MAININSTANCE.GT3_OUT), BOOL_ENUM},
{&(RES__MAININSTANCE.SWITCH_AXIS), INT_ENUM},
{&(RES__MAININSTANCE.SETPOS_AXIS), INT_ENUM},
{&(RES__MAININSTANCE.GT9_OUT), BOOL_ENUM},
{&(RES__MAININSTANCE.SEL39_OUT), LREAL_ENUM},
{&(RES__MAININSTANCE.MOVEA_AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE0_AXIS), INT_ENUM},
{&(RES__MAININSTANCE.GT81_OUT), BOOL_ENUM},
{&(RES__MAININSTANCE.GT33_OUT), BOOL_ENUM},
{&(RES__MAININSTANCE.MC_POWER0_AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_SETPOSITION0_AXIS), INT_ENUM},
{&(RES__MAININSTANCE.LT44_OUT), BOOL_ENUM},
{&(RES__MAININSTANCE.SEL66_OUT), LREAL_ENUM},
{&(RES__MAININSTANCE.MC_MOVEABSOLUTE0_AXIS), INT_ENUM},
{&(RES__MAININSTANCE.MC_MOVECONTINUOUSABSOLUTE1_AXIS), INT_ENUM},
{&(RES__MAININSTANCE.LT88_OUT), BOOL_ENUM}
};

typedef void(*__for_each_variable_do_fp)(dbgvardsc_t*);
void __for_each_variable_do(__for_each_variable_do_fp fp)
{
    int i;
    
    for(i = 0; i < sizeof(dbgvardsc)/sizeof(dbgvardsc_t); i++){
        dbgvardsc_t *dsc = &dbgvardsc[i];
        if(dsc->type != UNKNOWN_ENUM) 
            (*fp)(dsc);
    }
}

#define __Unpack_case_t(TYPENAME) \
        case TYPENAME##_ENUM :\
            *flags = ((__IEC_##TYPENAME##_t *)varp)->flags;\
            forced_value_p = *real_value_p = &((__IEC_##TYPENAME##_t *)varp)->value;\
            break;

#define __Unpack_case_p(TYPENAME)\
        case TYPENAME##_O_ENUM :\
            *flags = __IEC_OUTPUT_FLAG;\
        case TYPENAME##_P_ENUM :\
            *flags |= ((__IEC_##TYPENAME##_p *)varp)->flags;\
            *real_value_p = ((__IEC_##TYPENAME##_p *)varp)->value;\
            forced_value_p = &((__IEC_##TYPENAME##_p *)varp)->fvalue;\
            break;

void* UnpackVar(dbgvardsc_t *dsc, void **real_value_p, char *flags)
{
    void *varp = dsc->ptr;
    void *forced_value_p = NULL;
    *flags = 0;
    /* find data to copy*/
    switch(dsc->type){
        __ANY(__Unpack_case_t)
        __ANY(__Unpack_case_p)
    default:
        break;
    }
    if (*flags & __IEC_FORCE_FLAG)
        return forced_value_p;
    return *real_value_p;
}

void Remind(unsigned int offset, unsigned int count, void * p, int ch);

void RemindIterator(dbgvardsc_t *dsc)
{
    void *real_value_p = NULL;
    char flags = 0;
    UnpackVar(dsc, &real_value_p, &flags);

    if(flags & __IEC_RETAIN_FLAG){
        USINT size = __get_type_enum_size(dsc->type);
        /* compute next cursor positon*/
        unsigned int next_retain_offset = retain_offset + size;
        /* if buffer not full */
        Remind(retain_offset, size, real_value_p, retain_channel);
        /* increment cursor according size*/
        retain_offset = next_retain_offset;

    }
}

extern int CheckRetainBuffer(int ch);

void __init_debug(void)
{
    /* init local static vars */
    buffer_cursor = debug_buffer;
	buffer_state = BUFFER_FREE;
	#ifdef __XENO__
	/* init retain value */
	/*
    retain_offset = 0;
	retain_channel = retain_channel_open();
	in_data = (retain_struct_t *)malloc(sizeof(retain_struct_t));
	in_data -> size = 0;
	in_data -> value = (uint64_t *)malloc(sizeof(uint64_t));
	*/
	#endif
    /* Iterate over all variables to fill debug buffer */
	/*
    if(CheckRetainBuffer(retain_channel))
        __for_each_variable_do(RemindIterator);
	*/
    //__for_each_variable_do(RemindIterator);
}

extern void InitiateDebugTransfer(void);

extern unsigned long __tick;

void __cleanup_debug(void)
{
    buffer_cursor = debug_buffer;
    InitiateDebugTransfer();
	#ifdef __XENO__
	//retain_channel_close(retain_channel);
	#endif
}

void __retrieve_debug(void)
{
}

void Retain(unsigned int offset, unsigned int count, void * p, int ch);

inline void BufferIterator(dbgvardsc_t *dsc, int do_debug)
{
    void *real_value_p = NULL;
    void *visible_value_p = NULL;
    char flags = 0;

    visible_value_p = UnpackVar(dsc, &real_value_p, &flags);

    if(flags & ( __IEC_DEBUG_FLAG | __IEC_RETAIN_FLAG)){
        USINT size = __get_type_enum_size(dsc->type);
        if(flags & __IEC_DEBUG_FLAG){
            /* copy visible variable to buffer */;
            if(do_debug){
                /* compute next cursor positon.
                   No need to check overflow, as BUFFER_SIZE
                   is computed large enough */
                if(dsc->type == STRING_ENUM){
                    /* optimization for strings */
                    size = ((STRING*)visible_value_p)->len + 1;
                }
                char* next_cursor = buffer_cursor + size;
                /* copy data to the buffer */
                memcpy(buffer_cursor, visible_value_p, size);
                /* increment cursor according size*/
                buffer_cursor = next_cursor;
            }
            /* re-force real value of outputs (M and Q)*/
            if((flags & __IEC_FORCE_FLAG) && (flags & __IEC_OUTPUT_FLAG)){
                memcpy(real_value_p, visible_value_p, size);
            }
        }

        if(flags & __IEC_RETAIN_FLAG){
            /* compute next cursor positon*/
            unsigned int next_retain_offset = retain_offset + size;
            /* if buffer not full */
            //Retain(retain_offset, size, real_value_p, retain_channel);
            Retain(retain_offset, size, real_value_p, 0);
            /* increment cursor according size*/
            retain_offset = next_retain_offset;
        }
    }
}

void DebugIterator(dbgvardsc_t *dsc){
    BufferIterator(dsc, 1);
}

void RetainIterator(dbgvardsc_t *dsc){
    BufferIterator(dsc, 0);
}

extern void PLC_GetTime(IEC_TIME*);
extern int TryEnterDebugSection(void);
extern long AtomicCompareExchange(long*, long, long);
extern long long AtomicCompareExchange64(long long* , long long , long long);
extern void LeaveDebugSection(void);
extern void ValidateRetainBuffer(int ch);
extern void InValidateRetainBuffer(int ch);

void __publish_debug(void)
{
    retain_offset = 0;
    //ValidateRetainBuffer(retain_channel);
    ValidateRetainBuffer(0);
    
    // Check there is no running debugger re-configuration
    if(TryEnterDebugSection()){
        // Lock buffer
        long latest_state = AtomicCompareExchange(
            &buffer_state,
            BUFFER_FREE,
            BUFFER_BUSY);
            
        // If buffer was free
        if(latest_state == BUFFER_FREE)
        {
            // Reset buffer cursor 
            buffer_cursor = debug_buffer;
            // Iterate over all variables to fill debug buffer 
            __for_each_variable_do(DebugIterator);

            // Leave debug section,
            // Trigger asynchronous transmission 
            // (returns immediately)
            InitiateDebugTransfer(); // size
        }else{
            // when not debugging, do only retain
            __for_each_variable_do(RetainIterator);
        }
        LeaveDebugSection();
    }else{
        // when not debugging, do only retain 
        __for_each_variable_do(RetainIterator);
    }
    
    //InValidateRetainBuffer(retain_channel);
    InValidateRetainBuffer(0);
}

#define __RegisterDebugVariable_case_t(TYPENAME) \
        case TYPENAME##_ENUM :\
            ((__IEC_##TYPENAME##_t *)varp)->flags |= flags;\
            if(force)\
             ((__IEC_##TYPENAME##_t *)varp)->value = *((TYPENAME *)force);\
            break;
#define __RegisterDebugVariable_case_p(TYPENAME)\
        case TYPENAME##_P_ENUM :\
            ((__IEC_##TYPENAME##_p *)varp)->flags |= flags;\
            if(force)\
             ((__IEC_##TYPENAME##_p *)varp)->fvalue = *((TYPENAME *)force);\
            break;\
        case TYPENAME##_O_ENUM :\
            ((__IEC_##TYPENAME##_p *)varp)->flags |= flags;\
            if(force){\
             ((__IEC_##TYPENAME##_p *)varp)->fvalue = *((TYPENAME *)force);\
             *(((__IEC_##TYPENAME##_p *)varp)->value) = *((TYPENAME *)force);\
            }\
            break;

void RegisterDebugVariable(int idx, void* force)
{
    if(idx  < sizeof(dbgvardsc)/sizeof(dbgvardsc_t)){
        unsigned char flags = force ?
            __IEC_DEBUG_FLAG | __IEC_FORCE_FLAG :
            __IEC_DEBUG_FLAG;
        dbgvardsc_t *dsc = &dbgvardsc[idx];
        void *varp = dsc->ptr;
        switch(dsc->type){
            __ANY(__RegisterDebugVariable_case_t)
            __ANY(__RegisterDebugVariable_case_p)
        default:
            break;
        }
    }
}

#define __ResetDebugVariablesIterator_case_t(TYPENAME) \
        case TYPENAME##_ENUM :\
            ((__IEC_##TYPENAME##_t *)varp)->flags &= ~(__IEC_DEBUG_FLAG|__IEC_FORCE_FLAG);\
            break;

#define __ResetDebugVariablesIterator_case_p(TYPENAME)\
        case TYPENAME##_P_ENUM :\
        case TYPENAME##_O_ENUM :\
            ((__IEC_##TYPENAME##_p *)varp)->flags &= ~(__IEC_DEBUG_FLAG|__IEC_FORCE_FLAG);\
            break;

void ResetDebugVariablesIterator(dbgvardsc_t *dsc)
{
    /* force debug flag to 0*/
    void *varp = dsc->ptr;
    switch(dsc->type){
        __ANY(__ResetDebugVariablesIterator_case_t)
        __ANY(__ResetDebugVariablesIterator_case_p)
    default:
        break;
    }
}

void ResetDebugVariables(void)
{
    __for_each_variable_do(ResetDebugVariablesIterator);
}

void FreeDebugData(void)
{
    /* atomically mark buffer as free */
    AtomicCompareExchange(
        &buffer_state,
        BUFFER_BUSY,
        BUFFER_FREE);
}

int WaitDebugData(unsigned long *tick);
/* Wait until debug data ready and return pointer to it */
int GetDebugData(unsigned long *tick, unsigned long *size, void **buffer){
    int wait_error = WaitDebugData(tick);
    if(!wait_error){
        *size = buffer_cursor - debug_buffer;
        *buffer = debug_buffer;
    }
    return wait_error;
}

/************************** Modbus Section **********************************/

int *idx_list;
// The buffer itself
char modbus_read_buffer[20000];
char modbus_write_buffer[20000];
int size_sum = 0;

// Buffer's cursor
static char* modbus_read_buffer_cursor = modbus_read_buffer;
static char* modbus_write_buffer_cursor = modbus_write_buffer;
extern void WriteModbusData(void *buffer, int size);
extern int ReadModbusData(void *buffer, int size);

void* UnpackVar_test(dbgvardsc_t *dsc, void **real_value_p, char *flags)
{
    void *varp = dsc->ptr;
    void *forced_value_p = NULL;
    *flags = 0;
    switch(dsc->type){
        __ANY(__Unpack_case_t)
        __ANY(__Unpack_case_p)
    default:
        break;
    }
    //if (*flags & __IEC_FORCE_FLAG)
    //    return forced_value_p;
    return *real_value_p;
}

void __for_each_modbus_read_variable_do(int length, int data[])
{
    int i;
    void *real_value_p = NULL;
    void *visible_value_p = NULL;
    char flags = 0;
    size_sum = 0;

    for(i = 0; i < length; i++){
        dbgvardsc_t *dsc = &dbgvardsc[data[i]];
        if(dsc->type != UNKNOWN_ENUM){ 
            visible_value_p = UnpackVar_test(dsc, &real_value_p, &flags);
        }
        USINT size = __get_type_enum_size(dsc->type);

        if(dsc->type == STRING_ENUM){
            // optimization for strings 
            size = ((STRING*)visible_value_p)->len + 1;
        }
        char* next_cursor = modbus_write_buffer_cursor + size;
        size_sum = size_sum + size;
        // copy data to the buffer 
        memcpy(modbus_write_buffer_cursor, visible_value_p, size);
        // increment cursor according size
        modbus_write_buffer_cursor = next_cursor;
    }
}

void __for_each_modbus_write_variable_do(void)
{
    void *real_value_p = NULL;
    void *visible_value_p = NULL;
    char flags = 0;
    
    int *idx = (int *)malloc(sizeof(int));
    memcpy(idx, modbus_read_buffer_cursor, sizeof(int));

    /*
    float *idx = (float *)malloc(sizeof(float));
    memcpy(idx, modbus_read_buffer_cursor, sizeof(float));
    SLOGF(LOG_CRITICAL, "idx %d\n", (int)*idx);
    //*/

    dbgvardsc_t *dsc = &dbgvardsc[(int)(*idx)];
    if(dsc->type != UNKNOWN_ENUM){ 
        visible_value_p = UnpackVar_test(dsc, &real_value_p, &flags);
    }
    USINT size = __get_type_enum_size(dsc->type);

    if(dsc->type == STRING_ENUM){
        // optimization for strings 
        size = ((STRING*)visible_value_p)->len + 1;
    }
    char* next_cursor = modbus_read_buffer_cursor + sizeof(int);
    // copy data to the buffer  
    if(!((flags & __IEC_FORCE_FLAG) && (flags & __IEC_OUTPUT_FLAG))){
        memcpy(visible_value_p, next_cursor, size);
    }
}

void __retrieve_modbus(void)
{
    int res;
    // Read Variable from Pipe
    res = ReadModbusData(modbus_read_buffer, 1024);
    // Reset buffer cursor 
    modbus_read_buffer_cursor = modbus_read_buffer;
    // Iterate about ModbusRTU variables to overwrite IDE Variable
    if (res){
        __for_each_modbus_write_variable_do();
    }
}

void __publish_modbus(int length, int data[])
{
    // Reset buffer cursor 
    modbus_write_buffer_cursor = modbus_write_buffer;
    // Iterate about ModbusRTU variables to fill debug buffer 
    __for_each_modbus_read_variable_do(length, data);
    // Write Variable to Pipe
    WriteModbusData(modbus_write_buffer, size_sum);

}
