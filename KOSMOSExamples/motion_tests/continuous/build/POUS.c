



void R_TRIG_init__(R_TRIG *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CLK,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->M,__BOOL_LITERAL(FALSE),1)
}

// Code part
void R_TRIG_body__(R_TRIG *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,Q,(__GET_VAR(data__->CLK) && !(__GET_VAR(data__->M))));
  __SET_VAR(data__->,M,__GET_VAR(data__->CLK));

  goto __end;

__end:
  return;
} // R_TRIG_body__() 





void F_TRIG_init__(F_TRIG *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CLK,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->M,__BOOL_LITERAL(FALSE),1)
}

// Code part
void F_TRIG_body__(F_TRIG *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,Q,(!(__GET_VAR(data__->CLK)) && !(__GET_VAR(data__->M))));
  __SET_VAR(data__->,M,!(__GET_VAR(data__->CLK)));

  goto __end;

__end:
  return;
} // F_TRIG_body__() 





void SR_init__(SR *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->S1,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->R,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->Q1,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void SR_body__(SR *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,Q1,(__GET_VAR(data__->S1) || (!(__GET_VAR(data__->R)) && __GET_VAR(data__->Q1))));

  goto __end;

__end:
  return;
} // SR_body__() 





void RS_init__(RS *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->S,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->R1,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->Q1,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void RS_body__(RS *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,Q1,(!(__GET_VAR(data__->R1)) && (__GET_VAR(data__->S) || __GET_VAR(data__->Q1))));

  goto __end;

__end:
  return;
} // RS_body__() 





void CTU_init__(CTU *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CU,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->R,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CV,0,retain)
  R_TRIG_init__(&data__->CU_T,retain);
}

// Code part
void CTU_body__(CTU *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->CU_T.,CLK,__GET_VAR(data__->CU));
  R_TRIG_body__(&data__->CU_T);
  if (__GET_VAR(data__->R)) {
    __SET_VAR(data__->,CV,0);
  } else if ((__GET_VAR(data__->CU_T.Q) && (__GET_VAR(data__->CV) < __GET_VAR(data__->PV)))) {
    __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) + 1));
  };
  __SET_VAR(data__->,Q,(__GET_VAR(data__->CV) >= __GET_VAR(data__->PV)));

  goto __end;

__end:
  return;
} // CTU_body__() 





void CTU_DINT_init__(CTU_DINT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CU,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->R,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CV,0,retain)
  R_TRIG_init__(&data__->CU_T,retain);
}

// Code part
void CTU_DINT_body__(CTU_DINT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->CU_T.,CLK,__GET_VAR(data__->CU));
  R_TRIG_body__(&data__->CU_T);
  if (__GET_VAR(data__->R)) {
    __SET_VAR(data__->,CV,0);
  } else if ((__GET_VAR(data__->CU_T.Q) && (__GET_VAR(data__->CV) < __GET_VAR(data__->PV)))) {
    __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) + 1));
  };
  __SET_VAR(data__->,Q,(__GET_VAR(data__->CV) >= __GET_VAR(data__->PV)));

  goto __end;

__end:
  return;
} // CTU_DINT_body__() 





void CTU_LINT_init__(CTU_LINT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CU,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->R,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CV,0,retain)
  R_TRIG_init__(&data__->CU_T,retain);
}

// Code part
void CTU_LINT_body__(CTU_LINT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->CU_T.,CLK,__GET_VAR(data__->CU));
  R_TRIG_body__(&data__->CU_T);
  if (__GET_VAR(data__->R)) {
    __SET_VAR(data__->,CV,0);
  } else if ((__GET_VAR(data__->CU_T.Q) && (__GET_VAR(data__->CV) < __GET_VAR(data__->PV)))) {
    __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) + 1));
  };
  __SET_VAR(data__->,Q,(__GET_VAR(data__->CV) >= __GET_VAR(data__->PV)));

  goto __end;

__end:
  return;
} // CTU_LINT_body__() 





void CTU_UDINT_init__(CTU_UDINT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CU,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->R,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CV,0,retain)
  R_TRIG_init__(&data__->CU_T,retain);
}

// Code part
void CTU_UDINT_body__(CTU_UDINT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->CU_T.,CLK,__GET_VAR(data__->CU));
  R_TRIG_body__(&data__->CU_T);
  if (__GET_VAR(data__->R)) {
    __SET_VAR(data__->,CV,0);
  } else if ((__GET_VAR(data__->CU_T.Q) && (__GET_VAR(data__->CV) < __GET_VAR(data__->PV)))) {
    __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) + 1));
  };
  __SET_VAR(data__->,Q,(__GET_VAR(data__->CV) >= __GET_VAR(data__->PV)));

  goto __end;

__end:
  return;
} // CTU_UDINT_body__() 





void CTU_ULINT_init__(CTU_ULINT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CU,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->R,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CV,0,retain)
  R_TRIG_init__(&data__->CU_T,retain);
}

// Code part
void CTU_ULINT_body__(CTU_ULINT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->CU_T.,CLK,__GET_VAR(data__->CU));
  R_TRIG_body__(&data__->CU_T);
  if (__GET_VAR(data__->R)) {
    __SET_VAR(data__->,CV,0);
  } else if ((__GET_VAR(data__->CU_T.Q) && (__GET_VAR(data__->CV) < __GET_VAR(data__->PV)))) {
    __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) + 1));
  };
  __SET_VAR(data__->,Q,(__GET_VAR(data__->CV) >= __GET_VAR(data__->PV)));

  goto __end;

__end:
  return;
} // CTU_ULINT_body__() 





void CTD_init__(CTD *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CV,0,retain)
  R_TRIG_init__(&data__->CD_T,retain);
}

// Code part
void CTD_body__(CTD *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->CD_T.,CLK,__GET_VAR(data__->CD));
  R_TRIG_body__(&data__->CD_T);
  if (__GET_VAR(data__->LD)) {
    __SET_VAR(data__->,CV,__GET_VAR(data__->PV));
  } else if ((__GET_VAR(data__->CD_T.Q) && (__GET_VAR(data__->CV) > 0))) {
    __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) - 1));
  };
  __SET_VAR(data__->,Q,(__GET_VAR(data__->CV) <= 0));

  goto __end;

__end:
  return;
} // CTD_body__() 





void CTD_DINT_init__(CTD_DINT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CV,0,retain)
  R_TRIG_init__(&data__->CD_T,retain);
}

// Code part
void CTD_DINT_body__(CTD_DINT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->CD_T.,CLK,__GET_VAR(data__->CD));
  R_TRIG_body__(&data__->CD_T);
  if (__GET_VAR(data__->LD)) {
    __SET_VAR(data__->,CV,__GET_VAR(data__->PV));
  } else if ((__GET_VAR(data__->CD_T.Q) && (__GET_VAR(data__->CV) > 0))) {
    __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) - 1));
  };
  __SET_VAR(data__->,Q,(__GET_VAR(data__->CV) <= 0));

  goto __end;

__end:
  return;
} // CTD_DINT_body__() 





void CTD_LINT_init__(CTD_LINT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CV,0,retain)
  R_TRIG_init__(&data__->CD_T,retain);
}

// Code part
void CTD_LINT_body__(CTD_LINT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->CD_T.,CLK,__GET_VAR(data__->CD));
  R_TRIG_body__(&data__->CD_T);
  if (__GET_VAR(data__->LD)) {
    __SET_VAR(data__->,CV,__GET_VAR(data__->PV));
  } else if ((__GET_VAR(data__->CD_T.Q) && (__GET_VAR(data__->CV) > 0))) {
    __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) - 1));
  };
  __SET_VAR(data__->,Q,(__GET_VAR(data__->CV) <= 0));

  goto __end;

__end:
  return;
} // CTD_LINT_body__() 





void CTD_UDINT_init__(CTD_UDINT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CV,0,retain)
  R_TRIG_init__(&data__->CD_T,retain);
}

// Code part
void CTD_UDINT_body__(CTD_UDINT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->CD_T.,CLK,__GET_VAR(data__->CD));
  R_TRIG_body__(&data__->CD_T);
  if (__GET_VAR(data__->LD)) {
    __SET_VAR(data__->,CV,__GET_VAR(data__->PV));
  } else if ((__GET_VAR(data__->CD_T.Q) && (__GET_VAR(data__->CV) > 0))) {
    __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) - 1));
  };
  __SET_VAR(data__->,Q,(__GET_VAR(data__->CV) <= 0));

  goto __end;

__end:
  return;
} // CTD_UDINT_body__() 





void CTD_ULINT_init__(CTD_ULINT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CV,0,retain)
  R_TRIG_init__(&data__->CD_T,retain);
}

// Code part
void CTD_ULINT_body__(CTD_ULINT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->CD_T.,CLK,__GET_VAR(data__->CD));
  R_TRIG_body__(&data__->CD_T);
  if (__GET_VAR(data__->LD)) {
    __SET_VAR(data__->,CV,__GET_VAR(data__->PV));
  } else if ((__GET_VAR(data__->CD_T.Q) && (__GET_VAR(data__->CV) > 0))) {
    __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) - 1));
  };
  __SET_VAR(data__->,Q,(__GET_VAR(data__->CV) <= 0));

  goto __end;

__end:
  return;
} // CTD_ULINT_body__() 





void CTUD_init__(CTUD *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CU,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->R,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->QU,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->QD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CV,0,retain)
  R_TRIG_init__(&data__->CD_T,retain);
  R_TRIG_init__(&data__->CU_T,retain);
}

// Code part
void CTUD_body__(CTUD *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->CD_T.,CLK,__GET_VAR(data__->CD));
  R_TRIG_body__(&data__->CD_T);
  __SET_VAR(data__->CU_T.,CLK,__GET_VAR(data__->CU));
  R_TRIG_body__(&data__->CU_T);
  if (__GET_VAR(data__->R)) {
    __SET_VAR(data__->,CV,0);
  } else if (__GET_VAR(data__->LD)) {
    __SET_VAR(data__->,CV,__GET_VAR(data__->PV));
  } else {
    if (!((__GET_VAR(data__->CU_T.Q) && __GET_VAR(data__->CD_T.Q)))) {
      if ((__GET_VAR(data__->CU_T.Q) && (__GET_VAR(data__->CV) < __GET_VAR(data__->PV)))) {
        __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) + 1));
      } else if ((__GET_VAR(data__->CD_T.Q) && (__GET_VAR(data__->CV) > 0))) {
        __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) - 1));
      };
    };
  };
  __SET_VAR(data__->,QU,(__GET_VAR(data__->CV) >= __GET_VAR(data__->PV)));
  __SET_VAR(data__->,QD,(__GET_VAR(data__->CV) <= 0));

  goto __end;

__end:
  return;
} // CTUD_body__() 





void CTUD_DINT_init__(CTUD_DINT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CU,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->R,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->QU,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->QD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CV,0,retain)
  R_TRIG_init__(&data__->CD_T,retain);
  R_TRIG_init__(&data__->CU_T,retain);
}

// Code part
void CTUD_DINT_body__(CTUD_DINT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->CD_T.,CLK,__GET_VAR(data__->CD));
  R_TRIG_body__(&data__->CD_T);
  __SET_VAR(data__->CU_T.,CLK,__GET_VAR(data__->CU));
  R_TRIG_body__(&data__->CU_T);
  if (__GET_VAR(data__->R)) {
    __SET_VAR(data__->,CV,0);
  } else if (__GET_VAR(data__->LD)) {
    __SET_VAR(data__->,CV,__GET_VAR(data__->PV));
  } else {
    if (!((__GET_VAR(data__->CU_T.Q) && __GET_VAR(data__->CD_T.Q)))) {
      if ((__GET_VAR(data__->CU_T.Q) && (__GET_VAR(data__->CV) < __GET_VAR(data__->PV)))) {
        __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) + 1));
      } else if ((__GET_VAR(data__->CD_T.Q) && (__GET_VAR(data__->CV) > 0))) {
        __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) - 1));
      };
    };
  };
  __SET_VAR(data__->,QU,(__GET_VAR(data__->CV) >= __GET_VAR(data__->PV)));
  __SET_VAR(data__->,QD,(__GET_VAR(data__->CV) <= 0));

  goto __end;

__end:
  return;
} // CTUD_DINT_body__() 





void CTUD_LINT_init__(CTUD_LINT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CU,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->R,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->QU,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->QD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CV,0,retain)
  R_TRIG_init__(&data__->CD_T,retain);
  R_TRIG_init__(&data__->CU_T,retain);
}

// Code part
void CTUD_LINT_body__(CTUD_LINT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->CD_T.,CLK,__GET_VAR(data__->CD));
  R_TRIG_body__(&data__->CD_T);
  __SET_VAR(data__->CU_T.,CLK,__GET_VAR(data__->CU));
  R_TRIG_body__(&data__->CU_T);
  if (__GET_VAR(data__->R)) {
    __SET_VAR(data__->,CV,0);
  } else if (__GET_VAR(data__->LD)) {
    __SET_VAR(data__->,CV,__GET_VAR(data__->PV));
  } else {
    if (!((__GET_VAR(data__->CU_T.Q) && __GET_VAR(data__->CD_T.Q)))) {
      if ((__GET_VAR(data__->CU_T.Q) && (__GET_VAR(data__->CV) < __GET_VAR(data__->PV)))) {
        __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) + 1));
      } else if ((__GET_VAR(data__->CD_T.Q) && (__GET_VAR(data__->CV) > 0))) {
        __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) - 1));
      };
    };
  };
  __SET_VAR(data__->,QU,(__GET_VAR(data__->CV) >= __GET_VAR(data__->PV)));
  __SET_VAR(data__->,QD,(__GET_VAR(data__->CV) <= 0));

  goto __end;

__end:
  return;
} // CTUD_LINT_body__() 





void CTUD_UDINT_init__(CTUD_UDINT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CU,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->R,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->QU,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->QD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CV,0,retain)
  R_TRIG_init__(&data__->CD_T,retain);
  R_TRIG_init__(&data__->CU_T,retain);
}

// Code part
void CTUD_UDINT_body__(CTUD_UDINT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->CD_T.,CLK,__GET_VAR(data__->CD));
  R_TRIG_body__(&data__->CD_T);
  __SET_VAR(data__->CU_T.,CLK,__GET_VAR(data__->CU));
  R_TRIG_body__(&data__->CU_T);
  if (__GET_VAR(data__->R)) {
    __SET_VAR(data__->,CV,0);
  } else if (__GET_VAR(data__->LD)) {
    __SET_VAR(data__->,CV,__GET_VAR(data__->PV));
  } else {
    if (!((__GET_VAR(data__->CU_T.Q) && __GET_VAR(data__->CD_T.Q)))) {
      if ((__GET_VAR(data__->CU_T.Q) && (__GET_VAR(data__->CV) < __GET_VAR(data__->PV)))) {
        __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) + 1));
      } else if ((__GET_VAR(data__->CD_T.Q) && (__GET_VAR(data__->CV) > 0))) {
        __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) - 1));
      };
    };
  };
  __SET_VAR(data__->,QU,(__GET_VAR(data__->CV) >= __GET_VAR(data__->PV)));
  __SET_VAR(data__->,QD,(__GET_VAR(data__->CV) <= 0));

  goto __end;

__end:
  return;
} // CTUD_UDINT_body__() 





void CTUD_ULINT_init__(CTUD_ULINT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CU,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->R,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->QU,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->QD,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CV,0,retain)
  R_TRIG_init__(&data__->CD_T,retain);
  R_TRIG_init__(&data__->CU_T,retain);
}

// Code part
void CTUD_ULINT_body__(CTUD_ULINT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->CD_T.,CLK,__GET_VAR(data__->CD));
  R_TRIG_body__(&data__->CD_T);
  __SET_VAR(data__->CU_T.,CLK,__GET_VAR(data__->CU));
  R_TRIG_body__(&data__->CU_T);
  if (__GET_VAR(data__->R)) {
    __SET_VAR(data__->,CV,0);
  } else if (__GET_VAR(data__->LD)) {
    __SET_VAR(data__->,CV,__GET_VAR(data__->PV));
  } else {
    if (!((__GET_VAR(data__->CU_T.Q) && __GET_VAR(data__->CD_T.Q)))) {
      if ((__GET_VAR(data__->CU_T.Q) && (__GET_VAR(data__->CV) < __GET_VAR(data__->PV)))) {
        __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) + 1));
      } else if ((__GET_VAR(data__->CD_T.Q) && (__GET_VAR(data__->CV) > 0))) {
        __SET_VAR(data__->,CV,(__GET_VAR(data__->CV) - 1));
      };
    };
  };
  __SET_VAR(data__->,QU,(__GET_VAR(data__->CV) >= __GET_VAR(data__->PV)));
  __SET_VAR(data__->,QD,(__GET_VAR(data__->CV) <= 0));

  goto __end;

__end:
  return;
} // CTUD_ULINT_body__() 





void TP_init__(TP *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->IN,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PT,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ET,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->STATE,0,retain)
  __INIT_VAR(data__->PREV_IN,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CURRENT_TIME,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->START_TIME,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
}

// Code part
void TP_body__(TP *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
__SET_VAR(data__->,CURRENT_TIME,__CURRENT_TIME)
  #undef GetFbVar
  #undef SetFbVar
;
  if ((((__GET_VAR(data__->STATE) == 0) && !(__GET_VAR(data__->PREV_IN))) && __GET_VAR(data__->IN))) {
    __SET_VAR(data__->,STATE,1);
    __SET_VAR(data__->,Q,__BOOL_LITERAL(TRUE));
    __SET_VAR(data__->,START_TIME,__GET_VAR(data__->CURRENT_TIME));
  } else if ((__GET_VAR(data__->STATE) == 1)) {
    if (LE_TIME(__BOOL_LITERAL(TRUE), NULL, 2, __time_add(__GET_VAR(data__->START_TIME), __GET_VAR(data__->PT)), __GET_VAR(data__->CURRENT_TIME))) {
      __SET_VAR(data__->,STATE,2);
      __SET_VAR(data__->,Q,__BOOL_LITERAL(FALSE));
      __SET_VAR(data__->,ET,__GET_VAR(data__->PT));
    } else {
      __SET_VAR(data__->,ET,__time_sub(__GET_VAR(data__->CURRENT_TIME), __GET_VAR(data__->START_TIME)));
    };
  };
  if (((__GET_VAR(data__->STATE) == 2) && !(__GET_VAR(data__->IN)))) {
    __SET_VAR(data__->,ET,__time_to_timespec(1, 0, 0, 0, 0, 0));
    __SET_VAR(data__->,STATE,0);
  };
  __SET_VAR(data__->,PREV_IN,__GET_VAR(data__->IN));

  goto __end;

__end:
  return;
} // TP_body__() 





void TON_init__(TON *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->IN,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PT,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ET,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->STATE,0,retain)
  __INIT_VAR(data__->PREV_IN,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CURRENT_TIME,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->START_TIME,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
}

// Code part
void TON_body__(TON *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
__SET_VAR(data__->,CURRENT_TIME,__CURRENT_TIME)
  #undef GetFbVar
  #undef SetFbVar
;
  if ((((__GET_VAR(data__->STATE) == 0) && !(__GET_VAR(data__->PREV_IN))) && __GET_VAR(data__->IN))) {
    __SET_VAR(data__->,STATE,1);
    __SET_VAR(data__->,Q,__BOOL_LITERAL(FALSE));
    __SET_VAR(data__->,START_TIME,__GET_VAR(data__->CURRENT_TIME));
  } else {
    if (!(__GET_VAR(data__->IN))) {
      __SET_VAR(data__->,ET,__time_to_timespec(1, 0, 0, 0, 0, 0));
      __SET_VAR(data__->,Q,__BOOL_LITERAL(FALSE));
      __SET_VAR(data__->,STATE,0);
    } else if ((__GET_VAR(data__->STATE) == 1)) {
      if (LE_TIME(__BOOL_LITERAL(TRUE), NULL, 2, __time_add(__GET_VAR(data__->START_TIME), __GET_VAR(data__->PT)), __GET_VAR(data__->CURRENT_TIME))) {
        __SET_VAR(data__->,STATE,2);
        __SET_VAR(data__->,Q,__BOOL_LITERAL(TRUE));
        __SET_VAR(data__->,ET,__GET_VAR(data__->PT));
      } else {
        __SET_VAR(data__->,ET,__time_sub(__GET_VAR(data__->CURRENT_TIME), __GET_VAR(data__->START_TIME)));
      };
    };
  };
  __SET_VAR(data__->,PREV_IN,__GET_VAR(data__->IN));

  goto __end;

__end:
  return;
} // TON_body__() 





void TOF_init__(TOF *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->IN,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PT,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ET,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->STATE,0,retain)
  __INIT_VAR(data__->PREV_IN,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CURRENT_TIME,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->START_TIME,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
}

// Code part
void TOF_body__(TOF *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
__SET_VAR(data__->,CURRENT_TIME,__CURRENT_TIME)
  #undef GetFbVar
  #undef SetFbVar
;
  if ((((__GET_VAR(data__->STATE) == 0) && __GET_VAR(data__->PREV_IN)) && !(__GET_VAR(data__->IN)))) {
    __SET_VAR(data__->,STATE,1);
    __SET_VAR(data__->,START_TIME,__GET_VAR(data__->CURRENT_TIME));
  } else {
    if (__GET_VAR(data__->IN)) {
      __SET_VAR(data__->,ET,__time_to_timespec(1, 0, 0, 0, 0, 0));
      __SET_VAR(data__->,STATE,0);
    } else if ((__GET_VAR(data__->STATE) == 1)) {
      if (LE_TIME(__BOOL_LITERAL(TRUE), NULL, 2, __time_add(__GET_VAR(data__->START_TIME), __GET_VAR(data__->PT)), __GET_VAR(data__->CURRENT_TIME))) {
        __SET_VAR(data__->,STATE,2);
        __SET_VAR(data__->,ET,__GET_VAR(data__->PT));
      } else {
        __SET_VAR(data__->,ET,__time_sub(__GET_VAR(data__->CURRENT_TIME), __GET_VAR(data__->START_TIME)));
      };
    };
  };
  __SET_VAR(data__->,Q,(__GET_VAR(data__->IN) || (__GET_VAR(data__->STATE) == 1)));
  __SET_VAR(data__->,PREV_IN,__GET_VAR(data__->IN));

  goto __end;

__end:
  return;
} // TOF_body__() 





void DERIVATIVE_init__(DERIVATIVE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->RUN,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->XIN,0,retain)
  __INIT_VAR(data__->CYCLE,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->XOUT,0,retain)
  __INIT_VAR(data__->X1,0,retain)
  __INIT_VAR(data__->X2,0,retain)
  __INIT_VAR(data__->X3,0,retain)
}

// Code part
void DERIVATIVE_body__(DERIVATIVE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if (__GET_VAR(data__->RUN)) {
    __SET_VAR(data__->,XOUT,((((3.0 * (__GET_VAR(data__->XIN) - __GET_VAR(data__->X3))) + __GET_VAR(data__->X1)) - __GET_VAR(data__->X2)) / (10.0 * TIME_TO_REAL((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (TIME)__GET_VAR(data__->CYCLE)))));
    __SET_VAR(data__->,X3,__GET_VAR(data__->X2));
    __SET_VAR(data__->,X2,__GET_VAR(data__->X1));
    __SET_VAR(data__->,X1,__GET_VAR(data__->XIN));
  } else {
    __SET_VAR(data__->,XOUT,0.0);
    __SET_VAR(data__->,X1,__GET_VAR(data__->XIN));
    __SET_VAR(data__->,X2,__GET_VAR(data__->XIN));
    __SET_VAR(data__->,X3,__GET_VAR(data__->XIN));
  };

  goto __end;

__end:
  return;
} // DERIVATIVE_body__() 





void HYSTERESIS_init__(HYSTERESIS *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->XIN1,0,retain)
  __INIT_VAR(data__->XIN2,0,retain)
  __INIT_VAR(data__->EPS,0,retain)
  __INIT_VAR(data__->Q,0,retain)
}

// Code part
void HYSTERESIS_body__(HYSTERESIS *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if (__GET_VAR(data__->Q)) {
    if ((__GET_VAR(data__->XIN1) < (__GET_VAR(data__->XIN2) - __GET_VAR(data__->EPS)))) {
      __SET_VAR(data__->,Q,0);
    };
  } else if ((__GET_VAR(data__->XIN1) > (__GET_VAR(data__->XIN2) + __GET_VAR(data__->EPS)))) {
    __SET_VAR(data__->,Q,1);
  };

  goto __end;

__end:
  return;
} // HYSTERESIS_body__() 





void INTEGRAL_init__(INTEGRAL *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->RUN,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->R1,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->XIN,0,retain)
  __INIT_VAR(data__->X0,0,retain)
  __INIT_VAR(data__->CYCLE,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->XOUT,0,retain)
}

// Code part
void INTEGRAL_body__(INTEGRAL *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,Q,!(__GET_VAR(data__->R1)));
  if (__GET_VAR(data__->R1)) {
    __SET_VAR(data__->,XOUT,__GET_VAR(data__->X0));
  } else if (__GET_VAR(data__->RUN)) {
    __SET_VAR(data__->,XOUT,(__GET_VAR(data__->XOUT) + (__GET_VAR(data__->XIN) * TIME_TO_REAL((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (TIME)__GET_VAR(data__->CYCLE)))));
  };

  goto __end;

__end:
  return;
} // INTEGRAL_body__() 





void PID_init__(PID *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AUTO,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PV,0,retain)
  __INIT_VAR(data__->SP,0,retain)
  __INIT_VAR(data__->X0,0,retain)
  __INIT_VAR(data__->KP,0,retain)
  __INIT_VAR(data__->TR,0,retain)
  __INIT_VAR(data__->TD,0,retain)
  __INIT_VAR(data__->CYCLE,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->XOUT,0,retain)
  __INIT_VAR(data__->ERROR,0,retain)
  INTEGRAL_init__(&data__->ITERM,retain);
  DERIVATIVE_init__(&data__->DTERM,retain);
}

// Code part
void PID_body__(PID *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,ERROR,(__GET_VAR(data__->PV) - __GET_VAR(data__->SP)));
  __SET_VAR(data__->ITERM.,RUN,__GET_VAR(data__->AUTO));
  __SET_VAR(data__->ITERM.,R1,!(__GET_VAR(data__->AUTO)));
  __SET_VAR(data__->ITERM.,XIN,__GET_VAR(data__->ERROR));
  __SET_VAR(data__->ITERM.,X0,(__GET_VAR(data__->TR) * (__GET_VAR(data__->X0) - __GET_VAR(data__->ERROR))));
  __SET_VAR(data__->ITERM.,CYCLE,__GET_VAR(data__->CYCLE));
  INTEGRAL_body__(&data__->ITERM);
  __SET_VAR(data__->DTERM.,RUN,__GET_VAR(data__->AUTO));
  __SET_VAR(data__->DTERM.,XIN,__GET_VAR(data__->ERROR));
  __SET_VAR(data__->DTERM.,CYCLE,__GET_VAR(data__->CYCLE));
  DERIVATIVE_body__(&data__->DTERM);
  __SET_VAR(data__->,XOUT,(__GET_VAR(data__->KP) * ((__GET_VAR(data__->ERROR) + (__GET_VAR(data__->ITERM.XOUT) / __GET_VAR(data__->TR))) + (__GET_VAR(data__->DTERM.XOUT) * __GET_VAR(data__->TD)))));

  goto __end;

__end:
  return;
} // PID_body__() 





void RAMP_init__(RAMP *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->RUN,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->X0,0,retain)
  __INIT_VAR(data__->X1,0,retain)
  __INIT_VAR(data__->TR,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->CYCLE,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->XOUT,0.0,retain)
  __INIT_VAR(data__->XI,0,retain)
  __INIT_VAR(data__->T,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
}

// Code part
void RAMP_body__(RAMP *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,BUSY,__GET_VAR(data__->RUN));
  if (__GET_VAR(data__->RUN)) {
    if (GE_TIME(__BOOL_LITERAL(TRUE), NULL, 2, __GET_VAR(data__->T), __GET_VAR(data__->TR))) {
      __SET_VAR(data__->,BUSY,0);
      __SET_VAR(data__->,XOUT,__GET_VAR(data__->X1));
    } else {
      __SET_VAR(data__->,XOUT,(__GET_VAR(data__->XI) + (((__GET_VAR(data__->X1) - __GET_VAR(data__->XI)) * TIME_TO_REAL((BOOL)__BOOL_LITERAL(TRUE),
        NULL,
        (TIME)__GET_VAR(data__->T))) / TIME_TO_REAL((BOOL)__BOOL_LITERAL(TRUE),
        NULL,
        (TIME)__GET_VAR(data__->TR)))));
      __SET_VAR(data__->,T,__time_add(__GET_VAR(data__->T), __GET_VAR(data__->CYCLE)));
    };
  } else {
    __SET_VAR(data__->,XOUT,__GET_VAR(data__->X0));
    __SET_VAR(data__->,XI,__GET_VAR(data__->X0));
    __SET_VAR(data__->,T,__time_to_timespec(1, 0, 0, 0, 0, 0));
  };

  goto __end;

__end:
  return;
} // RAMP_body__() 





void RTC_init__(RTC *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->IN,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PDT,__dt_to_timespec(0, 0, 0, 1, 1, 1970),retain)
  __INIT_VAR(data__->Q,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CDT,__dt_to_timespec(0, 0, 0, 1, 1, 1970),retain)
  __INIT_VAR(data__->PREV_IN,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->OFFSET,__time_to_timespec(1, 0, 0, 0, 0, 0),retain)
  __INIT_VAR(data__->CURRENT_TIME,__dt_to_timespec(0, 0, 0, 1, 1, 1970),retain)
}

// Code part
void RTC_body__(RTC *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
__SET_VAR(data__->,CURRENT_TIME,__CURRENT_TIME)
  #undef GetFbVar
  #undef SetFbVar
;
  if (__GET_VAR(data__->IN)) {
    if (!(__GET_VAR(data__->PREV_IN))) {
      __SET_VAR(data__->,OFFSET,__time_sub(__GET_VAR(data__->PDT), __GET_VAR(data__->CURRENT_TIME)));
    };
    __SET_VAR(data__->,CDT,__time_add(__GET_VAR(data__->CURRENT_TIME), __GET_VAR(data__->OFFSET)));
  } else {
    __SET_VAR(data__->,CDT,__GET_VAR(data__->CURRENT_TIME));
  };
  __SET_VAR(data__->,Q,__GET_VAR(data__->IN));
  __SET_VAR(data__->,PREV_IN,__GET_VAR(data__->IN));

  goto __end;

__end:
  return;
} // RTC_body__() 





void SEMA_init__(SEMA *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CLAIM,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->RELEASE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->Q_INTERNAL,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void SEMA_body__(SEMA *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,Q_INTERNAL,(__GET_VAR(data__->CLAIM) || (__GET_VAR(data__->Q_INTERNAL) && !(__GET_VAR(data__->RELEASE)))));
  __SET_VAR(data__->,BUSY,__GET_VAR(data__->Q_INTERNAL));

  goto __end;

__end:
  return;
} // SEMA_body__() 





void LOGGER_init__(LOGGER *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->TRIG,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->MSG,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->LEVEL,LOGLEVEL__INFO,retain)
  __INIT_VAR(data__->TRIG0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void LOGGER_body__(LOGGER *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if ((__GET_VAR(data__->TRIG) && !(__GET_VAR(data__->TRIG0)))) {
        #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

   LogMessage(GetFbVar(LEVEL),(char*)GetFbVar(MSG, .body),GetFbVar(MSG, .len));
  
    #undef GetFbVar
    #undef SetFbVar
;
  };
  __SET_VAR(data__->,TRIG0,__GET_VAR(data__->TRIG));

  goto __end;

__end:
  return;
} // LOGGER_body__() 





void PYTHON_EVAL_init__(PYTHON_EVAL *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->TRIG,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CODE,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->ACK,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->RESULT,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->STATE,0,retain)
  __INIT_VAR(data__->BUFFER,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->PREBUFFER,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->TRIGM1,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TRIGGED,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void PYTHON_EVAL_body__(PYTHON_EVAL *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
extern void __PythonEvalFB(int, PYTHON_EVAL*);__PythonEvalFB(0, data__);
  #undef GetFbVar
  #undef SetFbVar
;

  goto __end;

__end:
  return;
} // PYTHON_EVAL_body__() 





void PYTHON_POLL_init__(PYTHON_POLL *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->TRIG,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CODE,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->ACK,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->RESULT,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->STATE,0,retain)
  __INIT_VAR(data__->BUFFER,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->PREBUFFER,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->TRIGM1,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TRIGGED,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void PYTHON_POLL_body__(PYTHON_POLL *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
extern void __PythonEvalFB(int, PYTHON_EVAL*);__PythonEvalFB(1,(PYTHON_EVAL*)(void*)data__);
  #undef GetFbVar
  #undef SetFbVar
;

  goto __end;

__end:
  return;
} // PYTHON_POLL_body__() 





void PYTHON_GEAR_init__(PYTHON_GEAR *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->N,0,retain)
  __INIT_VAR(data__->TRIG,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CODE,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->ACK,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->RESULT,__STRING_LITERAL(0,""),retain)
  PYTHON_EVAL_init__(&data__->PY_EVAL,retain);
  __INIT_VAR(data__->COUNTER,0,retain)
  __INIT_VAR(data__->ADD10_OUT,0,retain)
  __INIT_VAR(data__->EQ13_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->SEL15_OUT,0,retain)
  __INIT_VAR(data__->AND7_OUT,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void PYTHON_GEAR_body__(PYTHON_GEAR *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,ADD10_OUT,ADD__USINT__USINT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (USINT)__GET_VAR(data__->COUNTER),
    (USINT)__USINT_LITERAL(1)));
  __SET_VAR(data__->,EQ13_OUT,EQ__BOOL__USINT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (USINT)__GET_VAR(data__->N),
    (USINT)__GET_VAR(data__->ADD10_OUT)));
  __SET_VAR(data__->,SEL15_OUT,SEL__USINT__BOOL__USINT__USINT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->EQ13_OUT),
    (USINT)__GET_VAR(data__->ADD10_OUT),
    (USINT)__USINT_LITERAL(0)));
  __SET_VAR(data__->,COUNTER,__GET_VAR(data__->SEL15_OUT));
  __SET_VAR(data__->,AND7_OUT,AND__BOOL__BOOL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)__GET_VAR(data__->EQ13_OUT),
    (BOOL)__GET_VAR(data__->TRIG)));
  __SET_VAR(data__->PY_EVAL.,TRIG,__GET_VAR(data__->AND7_OUT));
  __SET_VAR(data__->PY_EVAL.,CODE,__GET_VAR(data__->CODE));
  PYTHON_EVAL_body__(&data__->PY_EVAL);
  __SET_VAR(data__->,ACK,__GET_VAR(data__->PY_EVAL.ACK));
  __SET_VAR(data__->,RESULT,__GET_VAR(data__->PY_EVAL.RESULT));

  goto __end;

__end:
  return;
} // PYTHON_GEAR_body__() 





void GETBOOLSTRING_init__(GETBOOLSTRING *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->VALUE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CODE,__STRING_LITERAL(0,""),retain)
}

// Code part
void GETBOOLSTRING_body__(GETBOOLSTRING *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if (__GET_VAR(data__->VALUE)) {
    __SET_VAR(data__->,CODE,__STRING_LITERAL(4,"True"));
  } else {
    __SET_VAR(data__->,CODE,__STRING_LITERAL(5,"False"));
  };

  goto __end;

__end:
  return;
} // GETBOOLSTRING_body__() 





void BUTTON_init__(BUTTON *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ID,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->BACK_ID,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->SELE_ID,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->TOGGLE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->SET_STATE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->STATE_IN,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->STATE_OUT,__BOOL_LITERAL(FALSE),retain)
  PYTHON_EVAL_init__(&data__->INIT_COMMAND,retain);
  GETBOOLSTRING_init__(&data__->GETBUTTONSTATE,retain);
  PYTHON_EVAL_init__(&data__->SETSTATE_COMMAND,retain);
  PYTHON_POLL_init__(&data__->GETSTATE_COMMAND,retain);
  GETBOOLSTRING_init__(&data__->GETBUTTONTOGGLE,retain);
  __INIT_VAR(data__->CONCAT2_OUT,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->CONCAT22_OUT,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->STRING_TO_INT25_OUT,0,retain)
  __INIT_VAR(data__->INT_TO_BOOL26_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->AND31_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONCAT7_OUT,__STRING_LITERAL(0,""),retain)
}

// Code part
void BUTTON_body__(BUTTON *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->GETBUTTONTOGGLE.,VALUE,__GET_VAR(data__->TOGGLE));
  GETBOOLSTRING_body__(&data__->GETBUTTONTOGGLE);
  __SET_VAR(data__->,CONCAT2_OUT,CONCAT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)7,
    (STRING)__STRING_LITERAL(37,"createSVGUIControl(\"button\",back_id=\""),
    (STRING)__GET_VAR(data__->BACK_ID),
    (STRING)__STRING_LITERAL(11,"\",sele_id=\""),
    (STRING)__GET_VAR(data__->SELE_ID),
    (STRING)__STRING_LITERAL(9,"\",toggle="),
    (STRING)__GET_VAR(data__->GETBUTTONTOGGLE.CODE),
    (STRING)__STRING_LITERAL(13,",active=True)")));
  __SET_VAR(data__->INIT_COMMAND.,TRIG,__BOOL_LITERAL(TRUE));
  __SET_VAR(data__->INIT_COMMAND.,CODE,__GET_VAR(data__->CONCAT2_OUT));
  PYTHON_EVAL_body__(&data__->INIT_COMMAND);
  __SET_VAR(data__->,ID,__GET_VAR(data__->INIT_COMMAND.RESULT));
  __SET_VAR(data__->,CONCAT22_OUT,CONCAT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)3,
    (STRING)__STRING_LITERAL(12,"int(getAttr("),
    (STRING)__GET_VAR(data__->ID),
    (STRING)__STRING_LITERAL(16,",\"state\",False))")));
  __SET_VAR(data__->GETSTATE_COMMAND.,TRIG,__GET_VAR(data__->INIT_COMMAND.ACK));
  __SET_VAR(data__->GETSTATE_COMMAND.,CODE,__GET_VAR(data__->CONCAT22_OUT));
  PYTHON_POLL_body__(&data__->GETSTATE_COMMAND);
  __SET_VAR(data__->,STRING_TO_INT25_OUT,STRING_TO_INT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (STRING)__GET_VAR(data__->GETSTATE_COMMAND.RESULT)));
  __SET_VAR(data__->,INT_TO_BOOL26_OUT,INT_TO_BOOL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (INT)__GET_VAR(data__->STRING_TO_INT25_OUT)));
  __SET_VAR(data__->,STATE_OUT,__GET_VAR(data__->INT_TO_BOOL26_OUT));
  __SET_VAR(data__->,AND31_OUT,AND__BOOL__BOOL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)__GET_VAR(data__->INIT_COMMAND.ACK),
    (BOOL)__GET_VAR(data__->SET_STATE)));
  __SET_VAR(data__->GETBUTTONSTATE.,VALUE,__GET_VAR(data__->STATE_IN));
  GETBOOLSTRING_body__(&data__->GETBUTTONSTATE);
  __SET_VAR(data__->,CONCAT7_OUT,CONCAT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)5,
    (STRING)__STRING_LITERAL(8,"setAttr("),
    (STRING)__GET_VAR(data__->ID),
    (STRING)__STRING_LITERAL(9,",\"state\","),
    (STRING)__GET_VAR(data__->GETBUTTONSTATE.CODE),
    (STRING)__STRING_LITERAL(1,")")));
  __SET_VAR(data__->SETSTATE_COMMAND.,TRIG,__GET_VAR(data__->AND31_OUT));
  __SET_VAR(data__->SETSTATE_COMMAND.,CODE,__GET_VAR(data__->CONCAT7_OUT));
  PYTHON_EVAL_body__(&data__->SETSTATE_COMMAND);

  goto __end;

__end:
  return;
} // BUTTON_body__() 





void LED_init__(LED *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ID,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->BACK_ID,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->SELE_ID,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->STATE_IN,__BOOL_LITERAL(FALSE),retain)
  PYTHON_EVAL_init__(&data__->INIT_COMMAND,retain);
  PYTHON_POLL_init__(&data__->SETSTATE_COMMAND,retain);
  GETBOOLSTRING_init__(&data__->GETLEDSTATE,retain);
  __INIT_VAR(data__->CONCAT2_OUT,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->CONCAT7_OUT,__STRING_LITERAL(0,""),retain)
}

// Code part
void LED_body__(LED *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,CONCAT2_OUT,CONCAT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)5,
    (STRING)__STRING_LITERAL(37,"createSVGUIControl(\"button\",back_id=\""),
    (STRING)__GET_VAR(data__->BACK_ID),
    (STRING)__STRING_LITERAL(11,"\",sele_id=\""),
    (STRING)__GET_VAR(data__->SELE_ID),
    (STRING)__STRING_LITERAL(27,"\",toggle=True,active=False)")));
  __SET_VAR(data__->INIT_COMMAND.,TRIG,__BOOL_LITERAL(TRUE));
  __SET_VAR(data__->INIT_COMMAND.,CODE,__GET_VAR(data__->CONCAT2_OUT));
  PYTHON_EVAL_body__(&data__->INIT_COMMAND);
  __SET_VAR(data__->,ID,__GET_VAR(data__->INIT_COMMAND.RESULT));
  __SET_VAR(data__->GETLEDSTATE.,VALUE,__GET_VAR(data__->STATE_IN));
  GETBOOLSTRING_body__(&data__->GETLEDSTATE);
  __SET_VAR(data__->,CONCAT7_OUT,CONCAT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)5,
    (STRING)__STRING_LITERAL(8,"setAttr("),
    (STRING)__GET_VAR(data__->ID),
    (STRING)__STRING_LITERAL(9,",\"state\","),
    (STRING)__GET_VAR(data__->GETLEDSTATE.CODE),
    (STRING)__STRING_LITERAL(1,")")));
  __SET_VAR(data__->SETSTATE_COMMAND.,TRIG,__GET_VAR(data__->INIT_COMMAND.ACK));
  __SET_VAR(data__->SETSTATE_COMMAND.,CODE,__GET_VAR(data__->CONCAT7_OUT));
  PYTHON_POLL_body__(&data__->SETSTATE_COMMAND);

  goto __end;

__end:
  return;
} // LED_body__() 





void TEXTCTRL_init__(TEXTCTRL *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ID,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->BACK_ID,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->SET_TEXT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TEXT,__STRING_LITERAL(0,""),retain)
  PYTHON_EVAL_init__(&data__->SVGUI_TEXTCTRL,retain);
  PYTHON_EVAL_init__(&data__->SETSTATE_COMMAND,retain);
  __INIT_VAR(data__->CONCAT1_OUT,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->AND31_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONCAT12_OUT,__STRING_LITERAL(0,""),retain)
}

// Code part
void TEXTCTRL_body__(TEXTCTRL *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,CONCAT1_OUT,CONCAT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)3,
    (STRING)__STRING_LITERAL(43,"createSVGUIControl(\"textControl\", back_id=\""),
    (STRING)__GET_VAR(data__->BACK_ID),
    (STRING)__STRING_LITERAL(2,"\")")));
  __SET_VAR(data__->SVGUI_TEXTCTRL.,TRIG,__BOOL_LITERAL(TRUE));
  __SET_VAR(data__->SVGUI_TEXTCTRL.,CODE,__GET_VAR(data__->CONCAT1_OUT));
  PYTHON_EVAL_body__(&data__->SVGUI_TEXTCTRL);
  __SET_VAR(data__->,ID,__GET_VAR(data__->SVGUI_TEXTCTRL.RESULT));
  __SET_VAR(data__->,AND31_OUT,AND__BOOL__BOOL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)__GET_VAR(data__->SVGUI_TEXTCTRL.ACK),
    (BOOL)__GET_VAR(data__->SET_TEXT)));
  __SET_VAR(data__->,CONCAT12_OUT,CONCAT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)5,
    (STRING)__STRING_LITERAL(8,"setAttr("),
    (STRING)__GET_VAR(data__->ID),
    (STRING)__STRING_LITERAL(9,",\"text\",\""),
    (STRING)__GET_VAR(data__->TEXT),
    (STRING)__STRING_LITERAL(2,"\")")));
  __SET_VAR(data__->SETSTATE_COMMAND.,TRIG,__GET_VAR(data__->AND31_OUT));
  __SET_VAR(data__->SETSTATE_COMMAND.,CODE,__GET_VAR(data__->CONCAT12_OUT));
  PYTHON_EVAL_body__(&data__->SETSTATE_COMMAND);

  goto __end;

__end:
  return;
} // TEXTCTRL_body__() 





void ETHERLABSDODOWNLOAD_init__(ETHERLABSDODOWNLOAD *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->POS,0,retain)
  __INIT_VAR(data__->INDEX,0,retain)
  __INIT_VAR(data__->SUBINDEX,0,retain)
  __INIT_VAR(data__->VARTYPE,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->VALUE,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->ACK,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  PYTHON_EVAL_init__(&data__->PY0,retain);
  PYTHON_EVAL_init__(&data__->PY1,retain);
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->STATE,0,retain)
}

// Code part
void ETHERLABSDODOWNLOAD_body__(ETHERLABSDODOWNLOAD *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
extern int AcquireSDOLock();
  #undef GetFbVar
  #undef SetFbVar
;
    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
extern int HasAnswer();
  #undef GetFbVar
  #undef SetFbVar
;
    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
extern void ReleaseSDOLock();
  #undef GetFbVar
  #undef SetFbVar
;
  {
    INT __case_expression = __GET_VAR(data__->STATE);
    switch (__case_expression) {
      case 0:
        if ((__GET_VAR(data__->EXECUTE) && !(__GET_VAR(data__->EXECUTE0)))) {
          __SET_VAR(data__->,STATE,1);
          __SET_VAR(data__->,ACK,0);
          __SET_VAR(data__->,ERROR,0);
        };
        break;
      case 1:
                #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
        #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
if (AcquireSDOLock()) __SET_VAR(data__->,STATE, 2)
        #undef GetFbVar
        #undef SetFbVar
;
        break;
      case 2:
        if (__GET_VAR(data__->PY0.ACK)) {
          __SET_VAR(data__->,STATE,3);
        };
        break;
      case 3:
                #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
        #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
if (HasAnswer()) __SET_VAR(data__->,STATE, 4)
        #undef GetFbVar
        #undef SetFbVar
;
        break;
      case 4:
        if (__GET_VAR(data__->PY1.ACK)) {
          __SET_VAR(data__->,ACK,1);
          __SET_VAR(data__->,ERROR,EQ__BOOL__STRING((BOOL)__BOOL_LITERAL(TRUE),
            NULL,
            (UINT)2,
            (STRING)__GET_VAR(data__->PY1.RESULT),
            (STRING)__STRING_LITERAL(5,"False")));
          __SET_VAR(data__->,STATE,0);
                    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
          #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
ReleaseSDOLock();
          #undef GetFbVar
          #undef SetFbVar
;
        };
        break;
      default:
        __SET_VAR(data__->,STATE,0);
        break;
    }
  };
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->PY0.,TRIG,(__GET_VAR(data__->STATE) == 2));
  __SET_VAR(data__->PY0.,CODE,CONCAT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)11,
    (STRING)__STRING_LITERAL(20,"EthercatSDODownload("),
    (STRING)INT_TO_STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (INT)__GET_VAR(data__->POS)),
    (STRING)__STRING_LITERAL(1,","),
    (STRING)UINT_TO_STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (UINT)__GET_VAR(data__->INDEX)),
    (STRING)__STRING_LITERAL(1,","),
    (STRING)USINT_TO_STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (USINT)__GET_VAR(data__->SUBINDEX)),
    (STRING)__STRING_LITERAL(2,",\""),
    (STRING)__GET_VAR(data__->VARTYPE),
    (STRING)__STRING_LITERAL(2,"\","),
    (STRING)__GET_VAR(data__->VALUE),
    (STRING)__STRING_LITERAL(1,")")));
  PYTHON_EVAL_body__(&data__->PY0);
  __SET_VAR(data__->PY1.,TRIG,(__GET_VAR(data__->STATE) == 4));
  __SET_VAR(data__->PY1.,CODE,__STRING_LITERAL(11,"GetResult()"));
  PYTHON_EVAL_body__(&data__->PY1);

  goto __end;

__end:
  return;
} // ETHERLABSDODOWNLOAD_body__() 





void ETHERLABSDOUPLOAD_init__(ETHERLABSDOUPLOAD *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->POS,0,retain)
  __INIT_VAR(data__->INDEX,0,retain)
  __INIT_VAR(data__->SUBINDEX,0,retain)
  __INIT_VAR(data__->VARTYPE,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->ACK,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VALUE,__STRING_LITERAL(0,""),retain)
  PYTHON_EVAL_init__(&data__->PY0,retain);
  PYTHON_EVAL_init__(&data__->PY1,retain);
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->STATE,0,retain)
}

// Code part
void ETHERLABSDOUPLOAD_body__(ETHERLABSDOUPLOAD *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
extern int AcquireSDOLock();
  #undef GetFbVar
  #undef SetFbVar
;
    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
extern int HasAnswer();
  #undef GetFbVar
  #undef SetFbVar
;
    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
extern void ReleaseSDOLock();
  #undef GetFbVar
  #undef SetFbVar
;
  {
    INT __case_expression = __GET_VAR(data__->STATE);
    switch (__case_expression) {
      case 0:
        if ((__GET_VAR(data__->EXECUTE) && !(__GET_VAR(data__->EXECUTE0)))) {
          __SET_VAR(data__->,STATE,1);
          __SET_VAR(data__->,ACK,0);
          __SET_VAR(data__->,VALID,0);
          __SET_VAR(data__->,VALUE,__STRING_LITERAL(4,"None"));
        };
        break;
      case 1:
                #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
        #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
if (AcquireSDOLock()) __SET_VAR(data__->,STATE, 2)
        #undef GetFbVar
        #undef SetFbVar
;
        break;
      case 2:
        if (__GET_VAR(data__->PY0.ACK)) {
          __SET_VAR(data__->,STATE,3);
        };
        break;
      case 3:
                #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
        #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
if (HasAnswer()) __SET_VAR(data__->,STATE, 4)
        #undef GetFbVar
        #undef SetFbVar
;
        break;
      case 4:
        if (__GET_VAR(data__->PY1.ACK)) {
          __SET_VAR(data__->,ACK,1);
          __SET_VAR(data__->,VALUE,__GET_VAR(data__->PY1.RESULT));
          __SET_VAR(data__->,VALID,NE__BOOL__STRING__STRING((BOOL)__BOOL_LITERAL(TRUE),
            NULL,
            (STRING)__GET_VAR(data__->VALUE),
            (STRING)__STRING_LITERAL(4,"None")));
          __SET_VAR(data__->,STATE,0);
                    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
          #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)
ReleaseSDOLock();
          #undef GetFbVar
          #undef SetFbVar
;
        };
        break;
      default:
        __SET_VAR(data__->,STATE,0);
        break;
    }
  };
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->PY0.,TRIG,(__GET_VAR(data__->STATE) == 2));
  __SET_VAR(data__->PY0.,CODE,CONCAT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)9,
    (STRING)__STRING_LITERAL(18,"EthercatSDOUpload("),
    (STRING)INT_TO_STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (INT)__GET_VAR(data__->POS)),
    (STRING)__STRING_LITERAL(1,","),
    (STRING)UINT_TO_STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (UINT)__GET_VAR(data__->INDEX)),
    (STRING)__STRING_LITERAL(1,","),
    (STRING)USINT_TO_STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (USINT)__GET_VAR(data__->SUBINDEX)),
    (STRING)__STRING_LITERAL(2,",\""),
    (STRING)__GET_VAR(data__->VARTYPE),
    (STRING)__STRING_LITERAL(2,"\")")));
  PYTHON_EVAL_body__(&data__->PY0);
  __SET_VAR(data__->PY1.,TRIG,(__GET_VAR(data__->STATE) == 4));
  __SET_VAR(data__->PY1.,CODE,__STRING_LITERAL(11,"GetResult()"));
  PYTHON_EVAL_body__(&data__->PY1);

  goto __end;

__end:
  return;
} // ETHERLABSDOUPLOAD_body__() 





void ETHERLABGETTORQUELIMIT_init__(ETHERLABGETTORQUELIMIT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->POS,0,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->TORQUELIMITPOS,0,retain)
  __INIT_VAR(data__->TORQUELIMITNEG,0,retain)
  ETHERLABSDOUPLOAD_init__(&data__->ETHERLABSDOUPLOAD0,retain);
  ETHERLABSDOUPLOAD_init__(&data__->ETHERLABSDOUPLOAD1,retain);
  RS_init__(&data__->RS0,retain);
  __INIT_VAR(data__->AND16_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->STRING_TO_UINT12_OUT,0,retain)
  __INIT_VAR(data__->OR18_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->AND19_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->STRING_TO_UINT13_OUT,0,retain)
}

// Code part
void ETHERLABGETTORQUELIMIT_body__(ETHERLABGETTORQUELIMIT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->ETHERLABSDOUPLOAD0.,EXECUTE,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->ETHERLABSDOUPLOAD0.,POS,__GET_VAR(data__->POS));
  __SET_VAR(data__->ETHERLABSDOUPLOAD0.,INDEX,0x60E0);
  __SET_VAR(data__->ETHERLABSDOUPLOAD0.,SUBINDEX,0);
  __SET_VAR(data__->ETHERLABSDOUPLOAD0.,VARTYPE,__STRING_LITERAL(6,"uint16"));
  ETHERLABSDOUPLOAD_body__(&data__->ETHERLABSDOUPLOAD0);
  __SET_VAR(data__->ETHERLABSDOUPLOAD1.,EXECUTE,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->ETHERLABSDOUPLOAD1.,POS,__GET_VAR(data__->POS));
  __SET_VAR(data__->ETHERLABSDOUPLOAD1.,INDEX,0x60E1);
  __SET_VAR(data__->ETHERLABSDOUPLOAD1.,SUBINDEX,0);
  __SET_VAR(data__->ETHERLABSDOUPLOAD1.,VARTYPE,__STRING_LITERAL(6,"uint16"));
  ETHERLABSDOUPLOAD_body__(&data__->ETHERLABSDOUPLOAD1);
  __SET_VAR(data__->,AND16_OUT,AND__BOOL__BOOL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)__GET_VAR(data__->ETHERLABSDOUPLOAD0.ACK),
    (BOOL)__GET_VAR(data__->ETHERLABSDOUPLOAD1.ACK)));
  __SET_VAR(data__->,DONE,__GET_VAR(data__->AND16_OUT));
  __SET_VAR(data__->,STRING_TO_UINT12_OUT,STRING_TO_UINT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (STRING)__GET_VAR(data__->ETHERLABSDOUPLOAD0.VALUE)));
  __SET_VAR(data__->,TORQUELIMITPOS,__GET_VAR(data__->STRING_TO_UINT12_OUT));
  __SET_VAR(data__->,OR18_OUT,OR__BOOL__BOOL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)!(__GET_VAR(data__->ETHERLABSDOUPLOAD0.VALID)),
    (BOOL)!(__GET_VAR(data__->ETHERLABSDOUPLOAD1.VALID))));
  __SET_VAR(data__->,AND19_OUT,AND__BOOL__BOOL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)__GET_VAR(data__->AND16_OUT),
    (BOOL)__GET_VAR(data__->OR18_OUT)));
  __SET_VAR(data__->,ERROR,__GET_VAR(data__->AND19_OUT));
  __SET_VAR(data__->,STRING_TO_UINT13_OUT,STRING_TO_UINT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (STRING)__GET_VAR(data__->ETHERLABSDOUPLOAD1.VALUE)));
  __SET_VAR(data__->,TORQUELIMITNEG,__GET_VAR(data__->STRING_TO_UINT13_OUT));
  __SET_VAR(data__->RS0.,S,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->RS0.,R1,__GET_VAR(data__->DONE));
  RS_body__(&data__->RS0);
  __SET_VAR(data__->,BUSY,__GET_VAR(data__->RS0.Q1));

  goto __end;

__end:
  return;
} // ETHERLABGETTORQUELIMIT_body__() 





void ETHERLABSETTORQUELIMIT_init__(ETHERLABSETTORQUELIMIT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->POS,0,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TORQUELIMITPOS,0,retain)
  __INIT_VAR(data__->TORQUELIMITNEG,0,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  ETHERLABSDODOWNLOAD_init__(&data__->ETHERLABSDOUPLOAD0,retain);
  ETHERLABSDODOWNLOAD_init__(&data__->ETHERLABSDOUPLOAD1,retain);
  RS_init__(&data__->RS0,retain);
  __INIT_VAR(data__->UINT_TO_STRING25_OUT,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->UINT_TO_STRING12_OUT,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->AND16_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->OR18_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->AND19_OUT,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void ETHERLABSETTORQUELIMIT_body__(ETHERLABSETTORQUELIMIT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,UINT_TO_STRING25_OUT,UINT_TO_STRING((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)__GET_VAR(data__->TORQUELIMITPOS)));
  __SET_VAR(data__->ETHERLABSDOUPLOAD0.,EXECUTE,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->ETHERLABSDOUPLOAD0.,POS,__GET_VAR(data__->POS));
  __SET_VAR(data__->ETHERLABSDOUPLOAD0.,INDEX,0x60E0);
  __SET_VAR(data__->ETHERLABSDOUPLOAD0.,SUBINDEX,0);
  __SET_VAR(data__->ETHERLABSDOUPLOAD0.,VARTYPE,__STRING_LITERAL(6,"uint16"));
  __SET_VAR(data__->ETHERLABSDOUPLOAD0.,VALUE,__GET_VAR(data__->UINT_TO_STRING25_OUT));
  ETHERLABSDODOWNLOAD_body__(&data__->ETHERLABSDOUPLOAD0);
  __SET_VAR(data__->,UINT_TO_STRING12_OUT,UINT_TO_STRING((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)__GET_VAR(data__->TORQUELIMITNEG)));
  __SET_VAR(data__->ETHERLABSDOUPLOAD1.,EXECUTE,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->ETHERLABSDOUPLOAD1.,POS,__GET_VAR(data__->POS));
  __SET_VAR(data__->ETHERLABSDOUPLOAD1.,INDEX,0x60E1);
  __SET_VAR(data__->ETHERLABSDOUPLOAD1.,SUBINDEX,0);
  __SET_VAR(data__->ETHERLABSDOUPLOAD1.,VARTYPE,__STRING_LITERAL(6,"uint16"));
  __SET_VAR(data__->ETHERLABSDOUPLOAD1.,VALUE,__GET_VAR(data__->UINT_TO_STRING12_OUT));
  ETHERLABSDODOWNLOAD_body__(&data__->ETHERLABSDOUPLOAD1);
  __SET_VAR(data__->,AND16_OUT,AND__BOOL__BOOL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)__GET_VAR(data__->ETHERLABSDOUPLOAD0.ACK),
    (BOOL)__GET_VAR(data__->ETHERLABSDOUPLOAD1.ACK)));
  __SET_VAR(data__->,DONE,__GET_VAR(data__->AND16_OUT));
  __SET_VAR(data__->,OR18_OUT,OR__BOOL__BOOL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)__GET_VAR(data__->ETHERLABSDOUPLOAD0.ERROR),
    (BOOL)__GET_VAR(data__->ETHERLABSDOUPLOAD1.ERROR)));
  __SET_VAR(data__->,AND19_OUT,AND__BOOL__BOOL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)__GET_VAR(data__->AND16_OUT),
    (BOOL)__GET_VAR(data__->OR18_OUT)));
  __SET_VAR(data__->,ERROR,__GET_VAR(data__->AND19_OUT));
  __SET_VAR(data__->RS0.,S,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->RS0.,R1,__GET_VAR(data__->DONE));
  RS_body__(&data__->RS0);
  __SET_VAR(data__->,BUSY,__GET_VAR(data__->RS0.Q1));

  goto __end;

__end:
  return;
} // ETHERLABSETTORQUELIMIT_body__() 





void MC_CAMSWITCH_REF_FROM_CSV_init__(MC_CAMSWITCH_REF_FROM_CSV *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->FILENAME,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CAMSWITCHLIST,-1,retain)
  PYTHON_EVAL_init__(&data__->PY1,retain);
}

// Code part
void MC_CAMSWITCH_REF_FROM_CSV_body__(MC_CAMSWITCH_REF_FROM_CSV *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if (!(__GET_VAR(data__->ACTIVE))) {
        #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

      extern int __MK_Alloc_MC_CAMSWITCH_REF();
      int res=__MK_Alloc_MC_CAMSWITCH_REF();
      SetFbVar(CAMSWITCHLIST,res);
    
    #undef GetFbVar
    #undef SetFbVar
;
    __SET_VAR(data__->,ACTIVE,__BOOL_LITERAL(TRUE));
  };
  __SET_VAR(data__->PY1.,TRIG,__BOOL_LITERAL(TRUE));
  __SET_VAR(data__->PY1.,CODE,CONCAT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)5,
    (STRING)__STRING_LITERAL(29,"MK_Load_MC_CAMSWITCH_REF_CSV("),
    (STRING)INT_TO_STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (INT)__GET_VAR(data__->CAMSWITCHLIST)),
    (STRING)__STRING_LITERAL(2,",\""),
    (STRING)__GET_VAR(data__->FILENAME),
    (STRING)__STRING_LITERAL(2,"\")")));
  PYTHON_EVAL_body__(&data__->PY1);
  if (AND__BOOL__BOOL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)__GET_VAR(data__->PY1.ACK),
    (BOOL)EQ__BOOL__STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (UINT)2,
      (STRING)__GET_VAR(data__->PY1.RESULT),
      (STRING)__STRING_LITERAL(4,"True")))) {
        #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

      extern uint8_t __MK_CheckPublic_MC_CAMSWITCH_REF(int);
      if (__MK_CheckPublic_MC_CAMSWITCH_REF(GetFbVar(CAMSWITCHLIST))){
          SetFbVar(VALID,TRUE);
      }
    
    #undef GetFbVar
    #undef SetFbVar
;
  };

  goto __end;

__end:
  return;
} // MC_CAMSWITCH_REF_FROM_CSV_body__() 





void MC_STOP_init__(MC_STOP *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DECELERATION,0,retain)
  __INIT_VAR(data__->JERK,0,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_STOP_body__(MC_STOP *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_Stop(MC_STOP*);
    __mcl_cmd_MC_Stop(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_STOP_body__() 





void MC_POWER_init__(MC_POWER *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->ENABLE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ENABLEPOSITIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ENABLENEGATIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->STATUS,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
}

// Code part
void MC_POWER_body__(MC_POWER *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_Power(MC_POWER*);
    __mcl_cmd_MC_Power(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;

  goto __end;

__end:
  return;
} // MC_POWER_body__() 





void MC_READACTUALTORQUE_init__(MC_READACTUALTORQUE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->ENABLE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->TORQUE,0,retain)
  __INIT_VAR(data__->ENABLE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_READACTUALTORQUE_body__(MC_READACTUALTORQUE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_ReadActualTorque(MC_READACTUALTORQUE*);
    __mcl_cmd_MC_ReadActualTorque(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,ENABLE0,__GET_VAR(data__->ENABLE));

  goto __end;

__end:
  return;
} // MC_READACTUALTORQUE_body__() 





void MC_GETTORQUELIMIT_init__(MC_GETTORQUELIMIT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->TORQUELIMITPOS,0,retain)
  __INIT_VAR(data__->TORQUELIMITNEG,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_GETTORQUELIMIT_body__(MC_GETTORQUELIMIT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_GetTorqueLimit(MC_GETTORQUELIMIT*);
    __mcl_cmd_MC_GetTorqueLimit(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_GETTORQUELIMIT_body__() 





void MC_READAXISERROR_init__(MC_READAXISERROR *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->ENABLE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->AXISERRORID,0,retain)
  __INIT_VAR(data__->ENABLE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_READAXISERROR_body__(MC_READAXISERROR *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_ReadAxisError(MC_READAXISERROR*);
    __mcl_cmd_MC_ReadAxisError(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,ENABLE0,__GET_VAR(data__->ENABLE));

  goto __end;

__end:
  return;
} // MC_READAXISERROR_body__() 





void MC_WRITEBOOLPARAMETER_init__(MC_WRITEBOOLPARAMETER *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PARAMETERNUMBER,0,retain)
  __INIT_VAR(data__->VALUE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->EXECUTIONMODE,MC_EXECUTION_MODE__MCIMMEDIATELY,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_WRITEBOOLPARAMETER_body__(MC_WRITEBOOLPARAMETER *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_WriteBoolParameter(MC_WRITEBOOLPARAMETER*);
    __mcl_cmd_MC_WriteBoolParameter(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_WRITEBOOLPARAMETER_body__() 





void MC_WRITEPARAMETER_init__(MC_WRITEPARAMETER *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PARAMETERNUMBER,0,retain)
  __INIT_VAR(data__->VALUE,0,retain)
  __INIT_VAR(data__->EXECUTIONMODE,MC_EXECUTION_MODE__MCIMMEDIATELY,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_WRITEPARAMETER_body__(MC_WRITEPARAMETER *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_WriteParameter(MC_WRITEPARAMETER*);
    __mcl_cmd_MC_WriteParameter(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_WRITEPARAMETER_body__() 





void MC_SIM_init__(MC_SIM *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->MAXVEL,1.0,retain)
  __INIT_VAR(data__->MAXACC,1.0,retain)
  __INIT_VAR(data__->MAXDEC,1.0,retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  MC_WRITEBOOLPARAMETER_init__(&data__->SIMPARAM,retain);
  MC_WRITEPARAMETER_init__(&data__->VELPARAM,retain);
  MC_WRITEPARAMETER_init__(&data__->ACCPARAM,retain);
  MC_WRITEPARAMETER_init__(&data__->DECPARAM,retain);
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_SIM_body__(MC_SIM *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if (!(__GET_VAR(data__->DONE))) {
        #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

      extern int __MK_Alloc_AXIS_REF(); 
      int res=__MK_Alloc_AXIS_REF();
      __SET_VAR(data__->,AXIS,res);
    
    #undef GetFbVar
    #undef SetFbVar
;
    __SET_VAR(data__->SIMPARAM.,AXIS,__GET_VAR(data__->AXIS));
    __SET_VAR(data__->SIMPARAM.,EXECUTE,__BOOL_LITERAL(TRUE));
    __SET_VAR(data__->SIMPARAM.,PARAMETERNUMBER,1000);
    __SET_VAR(data__->SIMPARAM.,VALUE,__BOOL_LITERAL(TRUE));
    MC_WRITEBOOLPARAMETER_body__(&data__->SIMPARAM);
    __SET_VAR(data__->,AXIS,__GET_VAR(data__->SIMPARAM.AXIS));
    __SET_VAR(data__->VELPARAM.,AXIS,__GET_VAR(data__->AXIS));
    __SET_VAR(data__->VELPARAM.,EXECUTE,__BOOL_LITERAL(TRUE));
    __SET_VAR(data__->VELPARAM.,PARAMETERNUMBER,9);
    __SET_VAR(data__->VELPARAM.,VALUE,__GET_VAR(data__->MAXVEL));
    MC_WRITEPARAMETER_body__(&data__->VELPARAM);
    __SET_VAR(data__->,AXIS,__GET_VAR(data__->VELPARAM.AXIS));
    __SET_VAR(data__->ACCPARAM.,AXIS,__GET_VAR(data__->AXIS));
    __SET_VAR(data__->ACCPARAM.,EXECUTE,__BOOL_LITERAL(TRUE));
    __SET_VAR(data__->ACCPARAM.,PARAMETERNUMBER,13);
    __SET_VAR(data__->ACCPARAM.,VALUE,__GET_VAR(data__->MAXACC));
    MC_WRITEPARAMETER_body__(&data__->ACCPARAM);
    __SET_VAR(data__->,AXIS,__GET_VAR(data__->ACCPARAM.AXIS));
    __SET_VAR(data__->DECPARAM.,AXIS,__GET_VAR(data__->AXIS));
    __SET_VAR(data__->DECPARAM.,EXECUTE,__BOOL_LITERAL(TRUE));
    __SET_VAR(data__->DECPARAM.,PARAMETERNUMBER,15);
    __SET_VAR(data__->DECPARAM.,VALUE,__GET_VAR(data__->MAXDEC));
    MC_WRITEPARAMETER_body__(&data__->DECPARAM);
    __SET_VAR(data__->,AXIS,__GET_VAR(data__->DECPARAM.AXIS));
    __SET_VAR(data__->,DONE,__BOOL_LITERAL(TRUE));
  };

  goto __end;

__end:
  return;
} // MC_SIM_body__() 





void MC_MOVERELATIVE_init__(MC_MOVERELATIVE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONTINUOUSUPDATE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DISTANCE,0,retain)
  __INIT_VAR(data__->VELOCITY,0,retain)
  __INIT_VAR(data__->ACCELERATION,0,retain)
  __INIT_VAR(data__->DECELERATION,0,retain)
  __INIT_VAR(data__->JERK,0,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_MOVERELATIVE_body__(MC_MOVERELATIVE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_MoveRelative(MC_MOVERELATIVE*);
    __mcl_cmd_MC_MoveRelative(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_MOVERELATIVE_body__() 





void MC_READBOOLPARAMETER_init__(MC_READBOOLPARAMETER *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->ENABLE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PARAMETERNUMBER,0,retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->VALUE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ENABLE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_READBOOLPARAMETER_body__(MC_READBOOLPARAMETER *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_ReadBoolParameter(MC_READBOOLPARAMETER*);
    __mcl_cmd_MC_ReadBoolParameter(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,ENABLE0,__GET_VAR(data__->ENABLE));

  goto __end;

__end:
  return;
} // MC_READBOOLPARAMETER_body__() 





void MC_READDIGITALINPUT_init__(MC_READDIGITALINPUT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->INPUT,-1,retain)
  __INIT_VAR(data__->ENABLE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->INPUTNUMBER,0,retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->VALUE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ENABLE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_READDIGITALINPUT_body__(MC_READDIGITALINPUT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_ReadDigitalInput(MC_READDIGITALINPUT*);
    __mcl_cmd_MC_ReadDigitalInput(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,ENABLE0,__GET_VAR(data__->ENABLE));

  goto __end;

__end:
  return;
} // MC_READDIGITALINPUT_body__() 





void MC_CAMOUT_init__(MC_CAMOUT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->SLAVE,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_CAMOUT_body__(MC_CAMOUT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_CamOut(MC_CAMOUT*);
    __mcl_cmd_MC_CamOut(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_CAMOUT_body__() 





void MC_MOVEVELOCITY_init__(MC_MOVEVELOCITY *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONTINUOUSUPDATE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VELOCITY,0,retain)
  __INIT_VAR(data__->ACCELERATION,0,retain)
  __INIT_VAR(data__->DECELERATION,0,retain)
  __INIT_VAR(data__->JERK,0,retain)
  __INIT_VAR(data__->DIRECTION,MC_DIRECTION__MCABSOLUTEPOSITION,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->INVELOCITY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->INVELOCITY0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_MOVEVELOCITY_body__(MC_MOVEVELOCITY *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_MoveVelocity(MC_MOVEVELOCITY*);
    __mcl_cmd_MC_MoveVelocity(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,INVELOCITY0,__GET_VAR(data__->INVELOCITY));

  goto __end;

__end:
  return;
} // MC_MOVEVELOCITY_body__() 





void MC_PHASINGABSOLUTE_init__(MC_PHASINGABSOLUTE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->MASTER,-1,retain)
  __INIT_VAR(data__->SLAVE,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PHASESHIFT,0,retain)
  __INIT_VAR(data__->VELOCITY,0,retain)
  __INIT_VAR(data__->ACCELERATION,0,retain)
  __INIT_VAR(data__->DECELERATION,0,retain)
  __INIT_VAR(data__->JERK,0,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->ABSOLUTEPHASESHIFT,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_PHASINGABSOLUTE_body__(MC_PHASINGABSOLUTE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_PhasingAbsolute(MC_PHASINGABSOLUTE*);
    __mcl_cmd_MC_PhasingAbsolute(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_PHASINGABSOLUTE_body__() 





void MC_SETPOSITION_init__(MC_SETPOSITION *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->POSITION,0,retain)
  __INIT_VAR(data__->RELATIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->EXECUTIONMODE,MC_EXECUTION_MODE__MCIMMEDIATELY,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_SETPOSITION_body__(MC_SETPOSITION *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_SetPosition(MC_SETPOSITION*);
    __mcl_cmd_MC_SetPosition(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_SETPOSITION_body__() 





void MC_READDIGITALOUTPUT_init__(MC_READDIGITALOUTPUT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->OUTPUT,-1,retain)
  __INIT_VAR(data__->ENABLE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->OUTPUTNUMBER,0,retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->VALUE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ENABLE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_READDIGITALOUTPUT_body__(MC_READDIGITALOUTPUT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_ReadDigitalOutput(MC_READDIGITALOUTPUT*);
    __mcl_cmd_MC_ReadDigitalOutput(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,ENABLE0,__GET_VAR(data__->ENABLE));

  goto __end;

__end:
  return;
} // MC_READDIGITALOUTPUT_body__() 





void MC_CAMTABLESELECT_init__(MC_CAMTABLESELECT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->MASTER,-1,retain)
  __INIT_VAR(data__->SLAVE,-1,retain)
  __INIT_VAR(data__->CAMTABLE,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PERIODIC,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->MASTERABSOLUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->SLAVEABSOLUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->EXECUTIONMODE,MC_EXECUTION_MODE__MCIMMEDIATELY,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->CAMTABLEID,-1,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->REFINIT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CAMTABLEIDREF,-1,retain)
}

// Code part
void MC_CAMTABLESELECT_body__(MC_CAMTABLESELECT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if (!(__GET_VAR(data__->REFINIT))) {
        #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

          extern int __MK_Alloc_MC_CAM_ID();
          int res=__MK_Alloc_MC_CAM_ID();
          SetFbVar(CAMTABLEID,res);
          SetFbVar(CAMTABLEIDREF,res);
      
    #undef GetFbVar
    #undef SetFbVar
;
    __SET_VAR(data__->,REFINIT,__BOOL_LITERAL(TRUE));
  };
    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_CamTableSelect(MC_CAMTABLESELECT*);
    __mcl_cmd_MC_CamTableSelect(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_CAMTABLESELECT_body__() 





void MC_POSITIONPROFILE_init__(MC_POSITIONPROFILE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->TIMEPOSITION,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONTINUOUSUPDATE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TIMESCALE,1.0,retain)
  __INIT_VAR(data__->POSITIONSCALE,1.0,retain)
  __INIT_VAR(data__->OFFSET,0,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_POSITIONPROFILE_body__(MC_POSITIONPROFILE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_PositionProfile(MC_POSITIONPROFILE*);
    __mcl_cmd_MC_PositionProfile(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_POSITIONPROFILE_body__() 





void MC_READSTATUS_init__(MC_READSTATUS *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->ENABLE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->ERRORSTOP,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DISABLED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->STOPPING,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->HOMING,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->STANDSTILL,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DISCRETEMOTION,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONTINUOUSMOTION,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->SYNCHRONIZEDMOTION,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ENABLE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_READSTATUS_body__(MC_READSTATUS *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_ReadStatus(MC_READSTATUS*);
    __mcl_cmd_MC_ReadStatus(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,ENABLE0,__GET_VAR(data__->ENABLE));

  goto __end;

__end:
  return;
} // MC_READSTATUS_body__() 





void MC_TV_REF_FROM_CSV_init__(MC_TV_REF_FROM_CSV *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->FILENAME,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TIMEPROFILE,-1,retain)
  PYTHON_EVAL_init__(&data__->PY1,retain);
}

// Code part
void MC_TV_REF_FROM_CSV_body__(MC_TV_REF_FROM_CSV *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if (!(__GET_VAR(data__->ACTIVE))) {
        #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

      extern int __MK_Alloc_MC_TV_REF();
      int res=__MK_Alloc_MC_TV_REF();
      SetFbVar(TIMEPROFILE,res);
    
    #undef GetFbVar
    #undef SetFbVar
;
    __SET_VAR(data__->,ACTIVE,__BOOL_LITERAL(TRUE));
  };
  __SET_VAR(data__->PY1.,TRIG,__BOOL_LITERAL(TRUE));
  __SET_VAR(data__->PY1.,CODE,CONCAT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)5,
    (STRING)__STRING_LITERAL(22,"MK_Load_MC_TV_REF_CSV("),
    (STRING)INT_TO_STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (INT)__GET_VAR(data__->TIMEPROFILE)),
    (STRING)__STRING_LITERAL(2,",\""),
    (STRING)__GET_VAR(data__->FILENAME),
    (STRING)__STRING_LITERAL(2,"\")")));
  PYTHON_EVAL_body__(&data__->PY1);
  if (AND__BOOL__BOOL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)__GET_VAR(data__->PY1.ACK),
    (BOOL)EQ__BOOL__STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (UINT)2,
      (STRING)__GET_VAR(data__->PY1.RESULT),
      (STRING)__STRING_LITERAL(4,"True")))) {
        #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

      extern uint8_t __MK_CheckPublic_MC_TV_REF(int);
      if (__MK_CheckPublic_MC_TV_REF(GetFbVar(TIMEPROFILE))){
          SetFbVar(VALID,TRUE);
      }
    
    #undef GetFbVar
    #undef SetFbVar
;
  };

  goto __end;

__end:
  return;
} // MC_TV_REF_FROM_CSV_body__() 





void MC_MOVECONTINUOUSRELATIVE_init__(MC_MOVECONTINUOUSRELATIVE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONTINUOUSUPDATE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DISTANCE,0,retain)
  __INIT_VAR(data__->ENDVELOCITY,0,retain)
  __INIT_VAR(data__->VELOCITY,0,retain)
  __INIT_VAR(data__->ACCELERATION,0,retain)
  __INIT_VAR(data__->DECELERATION,0,retain)
  __INIT_VAR(data__->JERK,0,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->INENDVELOCITY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->INENDVELOCITY0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_MOVECONTINUOUSRELATIVE_body__(MC_MOVECONTINUOUSRELATIVE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_MoveContinuousRelative(MC_MOVECONTINUOUSRELATIVE*);
    __mcl_cmd_MC_MoveContinuousRelative(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,INENDVELOCITY0,__GET_VAR(data__->INENDVELOCITY));

  goto __end;

__end:
  return;
} // MC_MOVECONTINUOUSRELATIVE_body__() 





void MC_TOINTARRAY_init__(MC_TOINTARRAY *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  {
    static const MC_INT_ARRAY temp = {{0,0,0,0,0,0}};
    __SET_VAR(data__->,INTARRAY,temp);
  }
  __INIT_VAR(data__->X,0,retain)
  __INIT_VAR(data__->Y,0,retain)
  __INIT_VAR(data__->Z,0,retain)
  __INIT_VAR(data__->U,0,retain)
  __INIT_VAR(data__->V,0,retain)
  __INIT_VAR(data__->W,0,retain)
}

// Code part
void MC_TOINTARRAY_body__(MC_TOINTARRAY *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,INTARRAY,__GET_VAR(data__->X),.table[(1) - (1)]);
  __SET_VAR(data__->,INTARRAY,__GET_VAR(data__->Y),.table[(2) - (1)]);
  __SET_VAR(data__->,INTARRAY,__GET_VAR(data__->Z),.table[(3) - (1)]);
  __SET_VAR(data__->,INTARRAY,__GET_VAR(data__->U),.table[(4) - (1)]);
  __SET_VAR(data__->,INTARRAY,__GET_VAR(data__->V),.table[(5) - (1)]);
  __SET_VAR(data__->,INTARRAY,__GET_VAR(data__->W),.table[(6) - (1)]);

  goto __end;

__end:
  return;
} // MC_TOINTARRAY_body__() 





void MC_CAMIN_init__(MC_CAMIN *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->MASTER,-1,retain)
  __INIT_VAR(data__->SLAVE,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONTINUOUSUPDATE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->MASTEROFFSET,0,retain)
  __INIT_VAR(data__->SLAVEOFFSET,0,retain)
  __INIT_VAR(data__->MASTERSCALING,1.0,retain)
  __INIT_VAR(data__->SLAVESCALING,1.0,retain)
  __INIT_VAR(data__->MASTERSTARTDISTANCE,0,retain)
  __INIT_VAR(data__->MASTERSYNCPOSITION,0,retain)
  __INIT_VAR(data__->STARTMODE,-1,retain)
  __INIT_VAR(data__->MASTERVALUESOURCE,MC_SOURCE__MCACTUALVALUE,retain)
  __INIT_VAR(data__->CAMTABLEID,-1,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->INSYNC,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->ENDOFPROFILE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->INSYNC0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_CAMIN_body__(MC_CAMIN *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_CamIn(MC_CAMIN*);
    __mcl_cmd_MC_CamIn(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,INSYNC0,__GET_VAR(data__->INSYNC));

  goto __end;

__end:
  return;
} // MC_CAMIN_body__() 





void MC_READAXISINFO_init__(MC_READAXISINFO *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->ENABLE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->HOMEABSSWITCH,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LIMITSWITCHPOS,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->LIMITSWITCHNEG,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->SIMULATION,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMUNICATIONREADY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->READYFORPOWERON,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->POWERON,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ISHOMED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->AXISWARNING,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ENABLE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_READAXISINFO_body__(MC_READAXISINFO *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_ReadAxisInfo(MC_READAXISINFO*);
    __mcl_cmd_MC_ReadAxisInfo(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,ENABLE0,__GET_VAR(data__->ENABLE));

  goto __end;

__end:
  return;
} // MC_READAXISINFO_body__() 





void MC_READPARAMETER_init__(MC_READPARAMETER *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->ENABLE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PARAMETERNUMBER,0,retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->VALUE,0,retain)
  __INIT_VAR(data__->ENABLE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_READPARAMETER_body__(MC_READPARAMETER *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_ReadParameter(MC_READPARAMETER*);
    __mcl_cmd_MC_ReadParameter(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,ENABLE0,__GET_VAR(data__->ENABLE));

  goto __end;

__end:
  return;
} // MC_READPARAMETER_body__() 





void MC_SETTORQUELIMIT_init__(MC_SETTORQUELIMIT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->TORQUELIMITPOS,0,retain)
  __INIT_VAR(data__->TORQUELIMITNEG,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_SETTORQUELIMIT_body__(MC_SETTORQUELIMIT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_SetTorqueLimit(MC_SETTORQUELIMIT*);
    __mcl_cmd_MC_SetTorqueLimit(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_SETTORQUELIMIT_body__() 





void MC_TOUCHPROBE_init__(MC_TOUCHPROBE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->TRIGGERINPUT,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->WINDOWONLY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->FIRSTPOSITION,0,retain)
  __INIT_VAR(data__->LASTPOSITION,0,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->RECORDEDPOSITION,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_TOUCHPROBE_body__(MC_TOUCHPROBE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_TouchProbe(MC_TOUCHPROBE*);
    __mcl_cmd_MC_TouchProbe(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_TOUCHPROBE_body__() 





void MC_GEAROUT_init__(MC_GEAROUT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->SLAVE,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_GEAROUT_body__(MC_GEAROUT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_GearOut(MC_GEAROUT*);
    __mcl_cmd_MC_GearOut(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_GEAROUT_body__() 





void MC_RESET_init__(MC_RESET *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_RESET_body__(MC_RESET *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_Reset(MC_RESET*);
    __mcl_cmd_MC_Reset(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_RESET_body__() 





void MC_TP_REF_FROM_CSV_init__(MC_TP_REF_FROM_CSV *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->FILENAME,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TIMEPROFILE,-1,retain)
  PYTHON_EVAL_init__(&data__->PY1,retain);
}

// Code part
void MC_TP_REF_FROM_CSV_body__(MC_TP_REF_FROM_CSV *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if (!(__GET_VAR(data__->ACTIVE))) {
        #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

      extern int __MK_Alloc_MC_TP_REF();
      int res=__MK_Alloc_MC_TP_REF();
      SetFbVar(TIMEPROFILE,res);
    
    #undef GetFbVar
    #undef SetFbVar
;
    __SET_VAR(data__->,ACTIVE,__BOOL_LITERAL(TRUE));
  };
  __SET_VAR(data__->PY1.,TRIG,__BOOL_LITERAL(TRUE));
  __SET_VAR(data__->PY1.,CODE,CONCAT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)5,
    (STRING)__STRING_LITERAL(22,"MK_Load_MC_TP_REF_CSV("),
    (STRING)INT_TO_STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (INT)__GET_VAR(data__->TIMEPROFILE)),
    (STRING)__STRING_LITERAL(2,",\""),
    (STRING)__GET_VAR(data__->FILENAME),
    (STRING)__STRING_LITERAL(2,"\")")));
  PYTHON_EVAL_body__(&data__->PY1);
  if (AND__BOOL__BOOL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)__GET_VAR(data__->PY1.ACK),
    (BOOL)EQ__BOOL__STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (UINT)2,
      (STRING)__GET_VAR(data__->PY1.RESULT),
      (STRING)__STRING_LITERAL(4,"True")))) {
        #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

      extern uint8_t __MK_CheckPublic_MC_TP_REF(int);
      if (__MK_CheckPublic_MC_TP_REF(GetFbVar(TIMEPROFILE))){
          SetFbVar(VALID,TRUE);
      }
    
    #undef GetFbVar
    #undef SetFbVar
;
  };

  goto __end;

__end:
  return;
} // MC_TP_REF_FROM_CSV_body__() 





void MC_HALT_init__(MC_HALT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DECELERATION,0,retain)
  __INIT_VAR(data__->JERK,0,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_HALT_body__(MC_HALT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_Halt(MC_HALT*);
    __mcl_cmd_MC_Halt(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_HALT_body__() 





void MC_ABORTTRIGGER_init__(MC_ABORTTRIGGER *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->TRIGGERINPUT,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_ABORTTRIGGER_body__(MC_ABORTTRIGGER *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_AbortTrigger(MC_ABORTTRIGGER*);
    __mcl_cmd_MC_AbortTrigger(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_ABORTTRIGGER_body__() 





void MC_ACCELERATIONPROFILE_init__(MC_ACCELERATIONPROFILE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->TIMEACCELERATION,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONTINUOUSUPDATE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TIMESCALE,1.0,retain)
  __INIT_VAR(data__->ACCELERATIONSCALE,1.0,retain)
  __INIT_VAR(data__->OFFSET,0,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->PROFILECOMPLETED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PROFILECOMPLETED0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_ACCELERATIONPROFILE_body__(MC_ACCELERATIONPROFILE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_AccelerationProfile(MC_ACCELERATIONPROFILE*);
    __mcl_cmd_MC_AccelerationProfile(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,PROFILECOMPLETED0,__GET_VAR(data__->PROFILECOMPLETED));

  goto __end;

__end:
  return;
} // MC_ACCELERATIONPROFILE_body__() 





void MC_DIGITALCAMSWITCH_init__(MC_DIGITALCAMSWITCH *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->SWITCHES,-1,retain)
  __INIT_VAR(data__->OUTPUTS,-1,retain)
  __INIT_VAR(data__->TRACKOPTIONS,-1,retain)
  __INIT_VAR(data__->ENABLE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ENABLEMASK,0,retain)
  __INIT_VAR(data__->VALUESOURCE,MC_SOURCE__MCACTUALVALUE,retain)
  __INIT_VAR(data__->INOPERATION,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->ENABLE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->INOPERATION0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_DIGITALCAMSWITCH_body__(MC_DIGITALCAMSWITCH *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_DigitalCamSwitch(MC_DIGITALCAMSWITCH*);
    __mcl_cmd_MC_DigitalCamSwitch(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,ENABLE0,__GET_VAR(data__->ENABLE));
  __SET_VAR(data__->,INOPERATION0,__GET_VAR(data__->INOPERATION));

  goto __end;

__end:
  return;
} // MC_DIGITALCAMSWITCH_body__() 





void MC_TA_REF_FROM_CSV_init__(MC_TA_REF_FROM_CSV *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->FILENAME,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TIMEPROFILE,-1,retain)
  PYTHON_EVAL_init__(&data__->PY1,retain);
}

// Code part
void MC_TA_REF_FROM_CSV_body__(MC_TA_REF_FROM_CSV *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if (!(__GET_VAR(data__->ACTIVE))) {
        #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

      extern int __MK_Alloc_MC_TA_REF();
      int res=__MK_Alloc_MC_TA_REF();
      SetFbVar(TIMEPROFILE,res);
    
    #undef GetFbVar
    #undef SetFbVar
;
    __SET_VAR(data__->,ACTIVE,__BOOL_LITERAL(TRUE));
  };
  __SET_VAR(data__->PY1.,TRIG,__BOOL_LITERAL(TRUE));
  __SET_VAR(data__->PY1.,CODE,CONCAT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)5,
    (STRING)__STRING_LITERAL(22,"MK_Load_MC_TA_REF_CSV("),
    (STRING)INT_TO_STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (INT)__GET_VAR(data__->TIMEPROFILE)),
    (STRING)__STRING_LITERAL(2,",\""),
    (STRING)__GET_VAR(data__->FILENAME),
    (STRING)__STRING_LITERAL(2,"\")")));
  PYTHON_EVAL_body__(&data__->PY1);
  if (AND__BOOL__BOOL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)__GET_VAR(data__->PY1.ACK),
    (BOOL)EQ__BOOL__STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (UINT)2,
      (STRING)__GET_VAR(data__->PY1.RESULT),
      (STRING)__STRING_LITERAL(4,"True")))) {
        #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

      extern uint8_t __MK_CheckPublic_MC_TA_REF(int);
      if (__MK_CheckPublic_MC_TA_REF(GetFbVar(TIMEPROFILE))){
          SetFbVar(VALID,TRUE);
      }
    
    #undef GetFbVar
    #undef SetFbVar
;
  };

  goto __end;

__end:
  return;
} // MC_TA_REF_FROM_CSV_body__() 





void MC_HOME_init__(MC_HOME *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->POSITION,0,retain)
  __INIT_VAR(data__->HOMINGMODE,MC_HOMING_PROCEDURES__MCHOMEDIRECT,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_HOME_body__(MC_HOME *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_Home(MC_HOME*);
    __mcl_cmd_MC_Home(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_HOME_body__() 





void MC_PHASINGRELATIVE_init__(MC_PHASINGRELATIVE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->MASTER,-1,retain)
  __INIT_VAR(data__->SLAVE,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PHASESHIFT,0,retain)
  __INIT_VAR(data__->VELOCITY,0,retain)
  __INIT_VAR(data__->ACCELERATION,0,retain)
  __INIT_VAR(data__->DECELERATION,0,retain)
  __INIT_VAR(data__->JERK,0,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->COVEREDPHASESHIFT,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_PHASINGRELATIVE_body__(MC_PHASINGRELATIVE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_PhasingRelative(MC_PHASINGRELATIVE*);
    __mcl_cmd_MC_PhasingRelative(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_PHASINGRELATIVE_body__() 





void MC_HALTSUPERIMPOSED_init__(MC_HALTSUPERIMPOSED *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DECELERATION,0,retain)
  __INIT_VAR(data__->JERK,0,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_HALTSUPERIMPOSED_body__(MC_HALTSUPERIMPOSED *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_HaltSuperimposed(MC_HALTSUPERIMPOSED*);
    __mcl_cmd_MC_HaltSuperimposed(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_HALTSUPERIMPOSED_body__() 





void MC_READACTUALPOSITION_init__(MC_READACTUALPOSITION *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->ENABLE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->POSITION,0,retain)
  __INIT_VAR(data__->ENABLE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_READACTUALPOSITION_body__(MC_READACTUALPOSITION *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_ReadActualPosition(MC_READACTUALPOSITION*);
    __mcl_cmd_MC_ReadActualPosition(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,ENABLE0,__GET_VAR(data__->ENABLE));

  goto __end;

__end:
  return;
} // MC_READACTUALPOSITION_body__() 





void MC_GEARIN_init__(MC_GEARIN *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->MASTER,-1,retain)
  __INIT_VAR(data__->SLAVE,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONTINUOUSUPDATE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->RATIONUMERATOR,1,retain)
  __INIT_VAR(data__->RATIODENOMINATOR,1,retain)
  __INIT_VAR(data__->MASTERVALUESOURCE,MC_SOURCE__MCACTUALVALUE,retain)
  __INIT_VAR(data__->ACCELERATION,0,retain)
  __INIT_VAR(data__->DECELERATION,0,retain)
  __INIT_VAR(data__->JERK,0,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->INGEAR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->INGEAR0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_GEARIN_body__(MC_GEARIN *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_GearIn(MC_GEARIN*);
    __mcl_cmd_MC_GearIn(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,INGEAR0,__GET_VAR(data__->INGEAR));

  goto __end;

__end:
  return;
} // MC_GEARIN_body__() 





void MC_READOUTPUTBIT_init__(MC_READOUTPUTBIT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->BIT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->OUTPUT,-1,retain)
  __INIT_VAR(data__->OUTPUTNUMBER,0,retain)
}

// Code part
void MC_READOUTPUTBIT_body__(MC_READOUTPUTBIT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

  extern uint32_t* __MK_GetPublic_MC_OUTPUT_REF(int);
  SetFbVar(BIT, (*__MK_GetPublic_MC_OUTPUT_REF(GetFbVar(OUTPUT)) >> 
  GetFbVar(OUTPUTNUMBER)) & 1);
  
  #undef GetFbVar
  #undef SetFbVar
;

  goto __end;

__end:
  return;
} // MC_READOUTPUTBIT_body__() 





void MC_SETOVERRIDE_init__(MC_SETOVERRIDE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->ENABLE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VELFACTOR,1.0,retain)
  __INIT_VAR(data__->ACCFACTOR,1.0,retain)
  __INIT_VAR(data__->JERKFACTOR,1.0,retain)
  __INIT_VAR(data__->ENABLED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->ENABLE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_SETOVERRIDE_body__(MC_SETOVERRIDE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_SetOverride(MC_SETOVERRIDE*);
    __mcl_cmd_MC_SetOverride(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,ENABLE0,__GET_VAR(data__->ENABLE));

  goto __end;

__end:
  return;
} // MC_SETOVERRIDE_body__() 





void MC_CREATEOUTPUT_init__(MC_CREATEOUTPUT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->OUTPUT,-1,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_CREATEOUTPUT_body__(MC_CREATEOUTPUT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if (!(__GET_VAR(data__->DONE))) {
        #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

        extern int __MK_Alloc_MC_OUTPUT_REF();
        int res=__MK_Alloc_MC_OUTPUT_REF();
        SetFbVar(OUTPUT,res);
    
    #undef GetFbVar
    #undef SetFbVar
;
    __SET_VAR(data__->,DONE,__BOOL_LITERAL(TRUE));
  };

  goto __end;

__end:
  return;
} // MC_CREATEOUTPUT_body__() 





void MC_MOVEABSOLUTE_init__(MC_MOVEABSOLUTE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONTINUOUSUPDATE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->POSITION,0,retain)
  __INIT_VAR(data__->VELOCITY,0,retain)
  __INIT_VAR(data__->ACCELERATION,0,retain)
  __INIT_VAR(data__->DECELERATION,0,retain)
  __INIT_VAR(data__->JERK,0,retain)
  __INIT_VAR(data__->DIRECTION,MC_DIRECTION__MCABSOLUTEPOSITION,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_MOVEABSOLUTE_body__(MC_MOVEABSOLUTE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_MoveAbsolute(MC_MOVEABSOLUTE*);
    __mcl_cmd_MC_MoveAbsolute(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_MOVEABSOLUTE_body__() 





void MC_MOVEADDITIVE_init__(MC_MOVEADDITIVE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONTINUOUSUPDATE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DISTANCE,0,retain)
  __INIT_VAR(data__->VELOCITY,0,retain)
  __INIT_VAR(data__->ACCELERATION,0,retain)
  __INIT_VAR(data__->DECELERATION,0,retain)
  __INIT_VAR(data__->JERK,0,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_MOVEADDITIVE_body__(MC_MOVEADDITIVE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_MoveAdditive(MC_MOVEADDITIVE*);
    __mcl_cmd_MC_MoveAdditive(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_MOVEADDITIVE_body__() 





void MC_CREATETRIGGER_init__(MC_CREATETRIGGER *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->POSITIVEEDGE,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->NEGATIVEEDGE,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->TRIGGERINPUT,-1,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_CREATETRIGGER_body__(MC_CREATETRIGGER *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if (!(__GET_VAR(data__->DONE))) {
        #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern int __MK_Alloc_MC_TRIGGER_REF();
    extern char* __MK_GetPublic_MC_TRIGGER_REF(int);
    int res=__MK_Alloc_MC_TRIGGER_REF(); 
    if(res>=0) 
        *__MK_GetPublic_MC_TRIGGER_REF(res)=
           (GetFbVar(POSITIVEEDGE) ? 0x01 : 0)
          |(GetFbVar(NEGATIVEEDGE) ? 0x02 : 0);
    SetFbVar(TRIGGERINPUT,res); 
    
    #undef GetFbVar
    #undef SetFbVar
;
    __SET_VAR(data__->,DONE,__BOOL_LITERAL(TRUE));
  };

  goto __end;

__end:
  return;
} // MC_CREATETRIGGER_body__() 





void MC_GEARINPOS_init__(MC_GEARINPOS *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->MASTER,-1,retain)
  __INIT_VAR(data__->SLAVE,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->RATIONUMERATOR,1,retain)
  __INIT_VAR(data__->RATIODENOMINATOR,1,retain)
  __INIT_VAR(data__->MASTERVALUESOURCE,MC_SOURCE__MCACTUALVALUE,retain)
  __INIT_VAR(data__->MASTERSYNCPOSITION,0,retain)
  __INIT_VAR(data__->SLAVESYNCPOSITION,0,retain)
  __INIT_VAR(data__->SYNCMODE,-1,retain)
  __INIT_VAR(data__->MASTERSTARTDISTANCE,0,retain)
  __INIT_VAR(data__->VELOCITY,0,retain)
  __INIT_VAR(data__->ACCELERATION,0,retain)
  __INIT_VAR(data__->DECELERATION,0,retain)
  __INIT_VAR(data__->JERK,0,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->STARTSYNC,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->INSYNC,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->INSYNC0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_GEARINPOS_body__(MC_GEARINPOS *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_GearInPos(MC_GEARINPOS*);
    __mcl_cmd_MC_GearInPos(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,INSYNC0,__GET_VAR(data__->INSYNC));

  goto __end;

__end:
  return;
} // MC_GEARINPOS_body__() 





void MC_TORQUECONTROL_init__(MC_TORQUECONTROL *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONTINUOUSUPDATE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TORQUE,0,retain)
  __INIT_VAR(data__->TORQUERAMP,0,retain)
  __INIT_VAR(data__->VELOCITY,0,retain)
  __INIT_VAR(data__->ACCELERATION,0,retain)
  __INIT_VAR(data__->DECELERATION,0,retain)
  __INIT_VAR(data__->JERK,0,retain)
  __INIT_VAR(data__->DIRECTION,MC_DIRECTION__MCABSOLUTEPOSITION,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->INTORQUE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->INTORQUE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_TORQUECONTROL_body__(MC_TORQUECONTROL *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_TorqueControl(MC_TORQUECONTROL*);
    __mcl_cmd_MC_TorqueControl(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,INTORQUE0,__GET_VAR(data__->INTORQUE));

  goto __end;

__end:
  return;
} // MC_TORQUECONTROL_body__() 





void MC_READACTUALVELOCITY_init__(MC_READACTUALVELOCITY *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->ENABLE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->VELOCITY,0,retain)
  __INIT_VAR(data__->ENABLE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_READACTUALVELOCITY_body__(MC_READACTUALVELOCITY *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_ReadActualVelocity(MC_READACTUALVELOCITY*);
    __mcl_cmd_MC_ReadActualVelocity(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,ENABLE0,__GET_VAR(data__->ENABLE));

  goto __end;

__end:
  return;
} // MC_READACTUALVELOCITY_body__() 





void MC_COMBINEAXES_init__(MC_COMBINEAXES *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->MASTER1,-1,retain)
  __INIT_VAR(data__->MASTER2,-1,retain)
  __INIT_VAR(data__->SLAVE,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONTINUOUSUPDATE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMBINEMODE,MC_COMBINE_MODE__MCADDAXES,retain)
  __INIT_VAR(data__->GEARRATIONUMERATORM1,1,retain)
  __INIT_VAR(data__->GEARRATIODENOMINATORM1,1,retain)
  __INIT_VAR(data__->GEARRATIONUMERATORM2,1,retain)
  __INIT_VAR(data__->GEARRATIODENOMINATORM2,1,retain)
  __INIT_VAR(data__->MASTERVALUESOURCEM1,MC_SOURCE__MCACTUALVALUE,retain)
  __INIT_VAR(data__->MASTERVALUESOURCEM2,MC_SOURCE__MCACTUALVALUE,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->INSYNC,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->INSYNC0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_COMBINEAXES_body__(MC_COMBINEAXES *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_CombineAxes(MC_COMBINEAXES*);
    __mcl_cmd_MC_CombineAxes(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,INSYNC0,__GET_VAR(data__->INSYNC));

  goto __end;

__end:
  return;
} // MC_COMBINEAXES_body__() 





void MC_VELOCITYPROFILE_init__(MC_VELOCITYPROFILE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->TIMEVELOCITY,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONTINUOUSUPDATE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->TIMESCALE,1.0,retain)
  __INIT_VAR(data__->VELOCITYSCALE,1.0,retain)
  __INIT_VAR(data__->OFFSET,0,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->PROFILECOMPLETED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->PROFILECOMPLETED0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_VELOCITYPROFILE_body__(MC_VELOCITYPROFILE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_VelocityProfile(MC_VELOCITYPROFILE*);
    __mcl_cmd_MC_VelocityProfile(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,PROFILECOMPLETED0,__GET_VAR(data__->PROFILECOMPLETED));

  goto __end;

__end:
  return;
} // MC_VELOCITYPROFILE_body__() 





void MC_CAM_REF_FROM_CSV_init__(MC_CAM_REF_FROM_CSV *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->FILENAME,__STRING_LITERAL(0,""),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CAMTABLE,-1,retain)
  PYTHON_EVAL_init__(&data__->PY1,retain);
}

// Code part
void MC_CAM_REF_FROM_CSV_body__(MC_CAM_REF_FROM_CSV *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if (!(__GET_VAR(data__->ACTIVE))) {
        #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

      extern int __MK_Alloc_MC_CAM_REF();
      int res=__MK_Alloc_MC_CAM_REF();
      SetFbVar(CAMTABLE,res);
    
    #undef GetFbVar
    #undef SetFbVar
;
    __SET_VAR(data__->,ACTIVE,__BOOL_LITERAL(TRUE));
  };
  __SET_VAR(data__->PY1.,TRIG,__BOOL_LITERAL(TRUE));
  __SET_VAR(data__->PY1.,CODE,CONCAT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)5,
    (STRING)__STRING_LITERAL(23,"MK_Load_MC_CAM_REF_CSV("),
    (STRING)INT_TO_STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (INT)__GET_VAR(data__->CAMTABLE)),
    (STRING)__STRING_LITERAL(2,",\""),
    (STRING)__GET_VAR(data__->FILENAME),
    (STRING)__STRING_LITERAL(2,"\")")));
  PYTHON_EVAL_body__(&data__->PY1);
  if (AND__BOOL__BOOL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (BOOL)__GET_VAR(data__->PY1.ACK),
    (BOOL)EQ__BOOL__STRING((BOOL)__BOOL_LITERAL(TRUE),
      NULL,
      (UINT)2,
      (STRING)__GET_VAR(data__->PY1.RESULT),
      (STRING)__STRING_LITERAL(4,"True")))) {
        #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
    #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

      extern uint8_t __MK_CheckPublic_MC_CAM_REF(int);
      if (__MK_CheckPublic_MC_CAM_REF(GetFbVar(CAMTABLE))){
          SetFbVar(VALID,TRUE);
      }
    
    #undef GetFbVar
    #undef SetFbVar
;
  };

  goto __end;

__end:
  return;
} // MC_CAM_REF_FROM_CSV_body__() 





void MC_READOUTPUTBITS_init__(MC_READOUTPUTBITS *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->BITS,0,retain)
  __INIT_VAR(data__->OUTPUT,-1,retain)
}

// Code part
void MC_READOUTPUTBITS_body__(MC_READOUTPUTBITS *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

  extern uint32_t* __MK_GetPublic_MC_OUTPUT_REF(int);
  SetFbVar(BITS, *__MK_GetPublic_MC_OUTPUT_REF(GetFbVar(OUTPUT)));
  
  #undef GetFbVar
  #undef SetFbVar
;

  goto __end;

__end:
  return;
} // MC_READOUTPUTBITS_body__() 





void MC_WRITEDIGITALOUTPUT_init__(MC_WRITEDIGITALOUTPUT *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->OUTPUT,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->OUTPUTNUMBER,0,retain)
  __INIT_VAR(data__->VALUE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->EXECUTIONMODE,MC_EXECUTION_MODE__MCIMMEDIATELY,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_WRITEDIGITALOUTPUT_body__(MC_WRITEDIGITALOUTPUT *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_WriteDigitalOutput(MC_WRITEDIGITALOUTPUT*);
    __mcl_cmd_MC_WriteDigitalOutput(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_WRITEDIGITALOUTPUT_body__() 





void MC_MOVECONTINUOUSABSOLUTE_init__(MC_MOVECONTINUOUSABSOLUTE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONTINUOUSUPDATE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->POSITION,0,retain)
  __INIT_VAR(data__->ENDVELOCITY,0,retain)
  __INIT_VAR(data__->VELOCITY,0,retain)
  __INIT_VAR(data__->ACCELERATION,0,retain)
  __INIT_VAR(data__->DECELERATION,0,retain)
  __INIT_VAR(data__->JERK,0,retain)
  __INIT_VAR(data__->DIRECTION,MC_DIRECTION__MCABSOLUTEPOSITION,retain)
  __INIT_VAR(data__->BUFFERMODE,MC_BUFFER_MODE__MCABORTING,retain)
  __INIT_VAR(data__->INENDVELOCITY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->INENDVELOCITY0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_MOVECONTINUOUSABSOLUTE_body__(MC_MOVECONTINUOUSABSOLUTE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_MoveContinuousAbsolute(MC_MOVECONTINUOUSABSOLUTE*);
    __mcl_cmd_MC_MoveContinuousAbsolute(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,INENDVELOCITY0,__GET_VAR(data__->INENDVELOCITY));

  goto __end;

__end:
  return;
} // MC_MOVECONTINUOUSABSOLUTE_body__() 





void MC_READMOTIONSTATE_init__(MC_READMOTIONSTATE *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->ENABLE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->SOURCE,MC_SOURCE__MCACTUALVALUE,retain)
  __INIT_VAR(data__->VALID,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->CONSTANTVELOCITY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACCELERATING,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DECELERATING,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DIRECTIONPOSITIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DIRECTIONNEGATIVE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ENABLE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_READMOTIONSTATE_body__(MC_READMOTIONSTATE *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_ReadMotionState(MC_READMOTIONSTATE*);
    __mcl_cmd_MC_ReadMotionState(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,ENABLE0,__GET_VAR(data__->ENABLE));

  goto __end;

__end:
  return;
} // MC_READMOTIONSTATE_body__() 





void MC_MOVESUPERIMPOSED_init__(MC_MOVESUPERIMPOSED *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->AXIS,-1,retain)
  __INIT_VAR(data__->EXECUTE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CONTINUOUSUPDATE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DISTANCE,0,retain)
  __INIT_VAR(data__->VELOCITYDIFF,0,retain)
  __INIT_VAR(data__->ACCELERATION,0,retain)
  __INIT_VAR(data__->DECELERATION,0,retain)
  __INIT_VAR(data__->JERK,0,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->BUSY,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->COMMANDABORTED,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERROR,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ERRORID,0,retain)
  __INIT_VAR(data__->COVEREDDISTANCE,0,retain)
  __INIT_VAR(data__->EXECUTE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->DONE0,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->ACTIVE0,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MC_MOVESUPERIMPOSED_body__(MC_MOVESUPERIMPOSED *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

    #define GetFbVar(var,...) __GET_VAR(data__->var,__VA_ARGS__)
  #define SetFbVar(var,val,...) __SET_VAR(data__->,var,val,__VA_ARGS__)

    extern void __mcl_cmd_MC_MoveSuperimposed(MC_MOVESUPERIMPOSED*);
    __mcl_cmd_MC_MoveSuperimposed(data__);
    
  #undef GetFbVar
  #undef SetFbVar
;
  __SET_VAR(data__->,EXECUTE0,__GET_VAR(data__->EXECUTE));
  __SET_VAR(data__->,DONE0,__GET_VAR(data__->DONE));

  goto __end;

__end:
  return;
} // MC_MOVESUPERIMPOSED_body__() 





void MAIN_init__(MAIN *data__, BOOL retain) {
  __INIT_VAR(data__->CLOCK,0,retain)
  __INIT_VAR(data__->DONE,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->POSAXIS,-1,retain)
  __INIT_VAR(data__->NEGAXIS,-1,retain)
  MC_MOVEABSOLUTE_init__(&data__->MOVEA,retain);
  MC_POWER_init__(&data__->SWITCH,retain);
  MC_READACTUALPOSITION_init__(&data__->READPOS,retain);
  __INIT_VAR(data__->PNNEG,0,retain)
  __INIT_VAR(data__->PNPOS,0,retain)
  __INIT_VAR(data__->VNNEG,0,retain)
  __INIT_VAR(data__->VNPOS,0,retain)
  MC_SETPOSITION_init__(&data__->SETPOS,retain);
  MC_READPARAMETER_init__(&data__->READSPEED,retain);
  MC_SIM_init__(&data__->MY_SIMULATED_AXIS,retain);
  MC_MOVECONTINUOUSABSOLUTE_init__(&data__->MC_MOVECONTINUOUSABSOLUTE0,retain);
  MC_MOVEABSOLUTE_init__(&data__->MC_MOVEABSOLUTE0,retain);
  MC_POWER_init__(&data__->MC_POWER0,retain);
  MC_READACTUALPOSITION_init__(&data__->MC_READACTUALPOSITION0,retain);
  MC_SETPOSITION_init__(&data__->MC_SETPOSITION0,retain);
  MC_READPARAMETER_init__(&data__->MC_READPARAMETER0,retain);
  MC_SIM_init__(&data__->MC_SIM0,retain);
  MC_MOVECONTINUOUSABSOLUTE_init__(&data__->MC_MOVECONTINUOUSABSOLUTE1,retain);
  MC_MOVECONTINUOUSABSOLUTE_init__(&data__->MC_MOVECONTINUOUSABSOLUTE2,retain);
  MC_MOVECONTINUOUSABSOLUTE_init__(&data__->MC_MOVECONTINUOUSABSOLUTE3,retain);
  MC_SETOVERRIDE_init__(&data__->MC_SETOVERRIDE0,retain);
  MC_SETOVERRIDE_init__(&data__->MC_SETOVERRIDE1,retain);
  __INIT_VAR(data__->ADD6_OUT,0,retain)
  __INIT_VAR(data__->READPOS_AXIS,-1,retain)
  __INIT_VAR(data__->MC_READACTUALPOSITION0_AXIS,-1,retain)
  __INIT_VAR(data__->GT3_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->SWITCH_AXIS,-1,retain)
  __INIT_VAR(data__->SETPOS_AXIS,-1,retain)
  __INIT_VAR(data__->GT9_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->SEL39_OUT,0,retain)
  __INIT_VAR(data__->MOVEA_AXIS,-1,retain)
  __INIT_VAR(data__->MC_MOVECONTINUOUSABSOLUTE0_AXIS,-1,retain)
  __INIT_VAR(data__->GT81_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->GT33_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->MC_POWER0_AXIS,-1,retain)
  __INIT_VAR(data__->MC_SETPOSITION0_AXIS,-1,retain)
  __INIT_VAR(data__->LT44_OUT,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->SEL66_OUT,0,retain)
  __INIT_VAR(data__->MC_MOVEABSOLUTE0_AXIS,-1,retain)
  __INIT_VAR(data__->MC_MOVECONTINUOUSABSOLUTE1_AXIS,-1,retain)
  __INIT_VAR(data__->LT88_OUT,__BOOL_LITERAL(FALSE),retain)
}

// Code part
void MAIN_body__(MAIN *data__) {
  // Initialise TEMP variables

  MC_SIM_body__(&data__->MY_SIMULATED_AXIS);
  __SET_VAR(data__->,POSAXIS,__GET_VAR(data__->MY_SIMULATED_AXIS.AXIS));
  __SET_VAR(data__->,ADD6_OUT,ADD__ULINT__ULINT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->CLOCK),
    (ULINT)__ULINT_LITERAL(1)));
  __SET_VAR(data__->,CLOCK,__GET_VAR(data__->ADD6_OUT));
  __SET_VAR(data__->READPOS.,AXIS,__GET_VAR(data__->POSAXIS));
  __SET_VAR(data__->READPOS.,ENABLE,__BOOL_LITERAL(TRUE));
  MC_READACTUALPOSITION_body__(&data__->READPOS);
  __SET_VAR(data__->,POSAXIS,__GET_VAR(data__->READPOS.AXIS));
  __SET_VAR(data__->,PNPOS,__GET_VAR(data__->READPOS.POSITION));
  __SET_VAR(data__->,READPOS_AXIS,__GET_VAR(data__->READPOS.AXIS));
  __SET_VAR(data__->READSPEED.,AXIS,__GET_VAR(data__->READPOS_AXIS));
  __SET_VAR(data__->READSPEED.,ENABLE,__BOOL_LITERAL(TRUE));
  __SET_VAR(data__->READSPEED.,PARAMETERNUMBER,10);
  MC_READPARAMETER_body__(&data__->READSPEED);
  __SET_VAR(data__->,READPOS_AXIS,__GET_VAR(data__->READSPEED.AXIS));
  __SET_VAR(data__->,VNPOS,__GET_VAR(data__->READSPEED.VALUE));
  MC_SIM_body__(&data__->MC_SIM0);
  __SET_VAR(data__->,NEGAXIS,__GET_VAR(data__->MC_SIM0.AXIS));
  __SET_VAR(data__->MC_READACTUALPOSITION0.,AXIS,__GET_VAR(data__->NEGAXIS));
  __SET_VAR(data__->MC_READACTUALPOSITION0.,ENABLE,__BOOL_LITERAL(TRUE));
  MC_READACTUALPOSITION_body__(&data__->MC_READACTUALPOSITION0);
  __SET_VAR(data__->,NEGAXIS,__GET_VAR(data__->MC_READACTUALPOSITION0.AXIS));
  __SET_VAR(data__->,PNNEG,__GET_VAR(data__->MC_READACTUALPOSITION0.POSITION));
  __SET_VAR(data__->,MC_READACTUALPOSITION0_AXIS,__GET_VAR(data__->MC_READACTUALPOSITION0.AXIS));
  __SET_VAR(data__->MC_READPARAMETER0.,AXIS,__GET_VAR(data__->MC_READACTUALPOSITION0_AXIS));
  __SET_VAR(data__->MC_READPARAMETER0.,ENABLE,__BOOL_LITERAL(TRUE));
  __SET_VAR(data__->MC_READPARAMETER0.,PARAMETERNUMBER,10);
  MC_READPARAMETER_body__(&data__->MC_READPARAMETER0);
  __SET_VAR(data__->,MC_READACTUALPOSITION0_AXIS,__GET_VAR(data__->MC_READPARAMETER0.AXIS));
  __SET_VAR(data__->,VNNEG,__GET_VAR(data__->MC_READPARAMETER0.VALUE));
  __SET_VAR(data__->,GT3_OUT,GT__BOOL__ULINT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->CLOCK),
    (ULINT)10));
  __SET_VAR(data__->SWITCH.,AXIS,__GET_VAR(data__->POSAXIS));
  __SET_VAR(data__->SWITCH.,ENABLE,__GET_VAR(data__->GT3_OUT));
  MC_POWER_body__(&data__->SWITCH);
  __SET_VAR(data__->,POSAXIS,__GET_VAR(data__->SWITCH.AXIS));
  __SET_VAR(data__->,SWITCH_AXIS,__GET_VAR(data__->SWITCH.AXIS));
  __SET_VAR(data__->SETPOS.,AXIS,__GET_VAR(data__->SWITCH_AXIS));
  __SET_VAR(data__->SETPOS.,EXECUTE,__GET_VAR(data__->SWITCH.STATUS));
  __SET_VAR(data__->SETPOS.,POSITION,1.0);
  MC_SETPOSITION_body__(&data__->SETPOS);
  __SET_VAR(data__->,SWITCH_AXIS,__GET_VAR(data__->SETPOS.AXIS));
  __SET_VAR(data__->,SETPOS_AXIS,__GET_VAR(data__->SETPOS.AXIS));
  __SET_VAR(data__->,GT9_OUT,GT__BOOL__LREAL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (LREAL)__GET_VAR(data__->PNPOS),
    (LREAL)25.0));
  __SET_VAR(data__->,SEL39_OUT,SEL__LREAL__BOOL__LREAL__LREAL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->GT9_OUT),
    (LREAL)__LREAL_LITERAL(5.0),
    (LREAL)__LREAL_LITERAL(10.0)));
  __SET_VAR(data__->MOVEA.,AXIS,__GET_VAR(data__->SETPOS_AXIS));
  __SET_VAR(data__->MOVEA.,EXECUTE,__GET_VAR(data__->SETPOS.DONE));
  __SET_VAR(data__->MOVEA.,CONTINUOUSUPDATE,__BOOL_LITERAL(TRUE));
  __SET_VAR(data__->MOVEA.,POSITION,50.0);
  __SET_VAR(data__->MOVEA.,VELOCITY,__GET_VAR(data__->SEL39_OUT));
  __SET_VAR(data__->MOVEA.,ACCELERATION,20.0);
  __SET_VAR(data__->MOVEA.,DECELERATION,10.0);
  MC_MOVEABSOLUTE_body__(&data__->MOVEA);
  __SET_VAR(data__->,SETPOS_AXIS,__GET_VAR(data__->MOVEA.AXIS));
  __SET_VAR(data__->,MOVEA_AXIS,__GET_VAR(data__->MOVEA.AXIS));
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE0.,AXIS,__GET_VAR(data__->MOVEA_AXIS));
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE0.,EXECUTE,__GET_VAR(data__->MOVEA.DONE));
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE0.,POSITION,55.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE0.,ENDVELOCITY,20.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE0.,VELOCITY,20.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE0.,ACCELERATION,20.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE0.,DECELERATION,10.0);
  MC_MOVECONTINUOUSABSOLUTE_body__(&data__->MC_MOVECONTINUOUSABSOLUTE0);
  __SET_VAR(data__->,MOVEA_AXIS,__GET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE0.AXIS));
  __SET_VAR(data__->,MC_MOVECONTINUOUSABSOLUTE0_AXIS,__GET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE0.AXIS));
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE2.,AXIS,__GET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE0_AXIS));
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE2.,EXECUTE,__GET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE0.INENDVELOCITY));
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE2.,POSITION,100.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE2.,ENDVELOCITY,-20.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE2.,VELOCITY,20.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE2.,ACCELERATION,20.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE2.,DECELERATION,10.0);
  MC_MOVECONTINUOUSABSOLUTE_body__(&data__->MC_MOVECONTINUOUSABSOLUTE2);
  __SET_VAR(data__->,MC_MOVECONTINUOUSABSOLUTE0_AXIS,__GET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE2.AXIS));
  __SET_VAR(data__->,GT81_OUT,GT__BOOL__LREAL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (LREAL)__GET_VAR(data__->PNPOS),
    (LREAL)10.0));
  __SET_VAR(data__->MC_SETOVERRIDE0.,AXIS,__GET_VAR(data__->POSAXIS));
  __SET_VAR(data__->MC_SETOVERRIDE0.,ENABLE,__GET_VAR(data__->GT81_OUT));
  __SET_VAR(data__->MC_SETOVERRIDE0.,VELFACTOR,2.0);
  __SET_VAR(data__->MC_SETOVERRIDE0.,ACCFACTOR,2.0);
  MC_SETOVERRIDE_body__(&data__->MC_SETOVERRIDE0);
  __SET_VAR(data__->,POSAXIS,__GET_VAR(data__->MC_SETOVERRIDE0.AXIS));
  __SET_VAR(data__->,GT33_OUT,GT__BOOL__ULINT((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (ULINT)__GET_VAR(data__->CLOCK),
    (ULINT)10));
  __SET_VAR(data__->MC_POWER0.,AXIS,__GET_VAR(data__->NEGAXIS));
  __SET_VAR(data__->MC_POWER0.,ENABLE,__GET_VAR(data__->GT33_OUT));
  MC_POWER_body__(&data__->MC_POWER0);
  __SET_VAR(data__->,NEGAXIS,__GET_VAR(data__->MC_POWER0.AXIS));
  __SET_VAR(data__->,MC_POWER0_AXIS,__GET_VAR(data__->MC_POWER0.AXIS));
  __SET_VAR(data__->MC_SETPOSITION0.,AXIS,__GET_VAR(data__->MC_POWER0_AXIS));
  __SET_VAR(data__->MC_SETPOSITION0.,EXECUTE,__GET_VAR(data__->MC_POWER0.STATUS));
  __SET_VAR(data__->MC_SETPOSITION0.,POSITION,-1.0);
  MC_SETPOSITION_body__(&data__->MC_SETPOSITION0);
  __SET_VAR(data__->,MC_POWER0_AXIS,__GET_VAR(data__->MC_SETPOSITION0.AXIS));
  __SET_VAR(data__->,MC_SETPOSITION0_AXIS,__GET_VAR(data__->MC_SETPOSITION0.AXIS));
  __SET_VAR(data__->,LT44_OUT,LT__BOOL__LREAL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (LREAL)__GET_VAR(data__->PNNEG),
    (LREAL)-25.0));
  __SET_VAR(data__->,SEL66_OUT,SEL__LREAL__BOOL__LREAL__LREAL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->LT44_OUT),
    (LREAL)__LREAL_LITERAL(5.0),
    (LREAL)__LREAL_LITERAL(10.0)));
  __SET_VAR(data__->MC_MOVEABSOLUTE0.,AXIS,__GET_VAR(data__->MC_SETPOSITION0_AXIS));
  __SET_VAR(data__->MC_MOVEABSOLUTE0.,EXECUTE,__GET_VAR(data__->MC_SETPOSITION0.DONE));
  __SET_VAR(data__->MC_MOVEABSOLUTE0.,CONTINUOUSUPDATE,__BOOL_LITERAL(TRUE));
  __SET_VAR(data__->MC_MOVEABSOLUTE0.,POSITION,-50.0);
  __SET_VAR(data__->MC_MOVEABSOLUTE0.,VELOCITY,__GET_VAR(data__->SEL66_OUT));
  __SET_VAR(data__->MC_MOVEABSOLUTE0.,ACCELERATION,20.0);
  __SET_VAR(data__->MC_MOVEABSOLUTE0.,DECELERATION,10.0);
  MC_MOVEABSOLUTE_body__(&data__->MC_MOVEABSOLUTE0);
  __SET_VAR(data__->,MC_SETPOSITION0_AXIS,__GET_VAR(data__->MC_MOVEABSOLUTE0.AXIS));
  __SET_VAR(data__->,MC_MOVEABSOLUTE0_AXIS,__GET_VAR(data__->MC_MOVEABSOLUTE0.AXIS));
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE1.,AXIS,__GET_VAR(data__->MC_MOVEABSOLUTE0_AXIS));
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE1.,EXECUTE,__GET_VAR(data__->MC_MOVEABSOLUTE0.DONE));
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE1.,POSITION,-55.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE1.,ENDVELOCITY,-20.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE1.,VELOCITY,20.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE1.,ACCELERATION,20.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE1.,DECELERATION,10.0);
  MC_MOVECONTINUOUSABSOLUTE_body__(&data__->MC_MOVECONTINUOUSABSOLUTE1);
  __SET_VAR(data__->,MC_MOVEABSOLUTE0_AXIS,__GET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE1.AXIS));
  __SET_VAR(data__->,MC_MOVECONTINUOUSABSOLUTE1_AXIS,__GET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE1.AXIS));
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE3.,AXIS,__GET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE1_AXIS));
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE3.,EXECUTE,__GET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE1.INENDVELOCITY));
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE3.,POSITION,-100.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE3.,ENDVELOCITY,20.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE3.,VELOCITY,20.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE3.,ACCELERATION,20.0);
  __SET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE3.,DECELERATION,10.0);
  MC_MOVECONTINUOUSABSOLUTE_body__(&data__->MC_MOVECONTINUOUSABSOLUTE3);
  __SET_VAR(data__->,MC_MOVECONTINUOUSABSOLUTE1_AXIS,__GET_VAR(data__->MC_MOVECONTINUOUSABSOLUTE3.AXIS));
  __SET_VAR(data__->,LT88_OUT,LT__BOOL__LREAL((BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (LREAL)__GET_VAR(data__->PNNEG),
    (LREAL)-10.0));
  __SET_VAR(data__->MC_SETOVERRIDE1.,AXIS,__GET_VAR(data__->NEGAXIS));
  __SET_VAR(data__->MC_SETOVERRIDE1.,ENABLE,__GET_VAR(data__->LT88_OUT));
  __SET_VAR(data__->MC_SETOVERRIDE1.,VELFACTOR,2.0);
  __SET_VAR(data__->MC_SETOVERRIDE1.,ACCFACTOR,2.0);
  MC_SETOVERRIDE_body__(&data__->MC_SETOVERRIDE1);
  __SET_VAR(data__->,NEGAXIS,__GET_VAR(data__->MC_SETOVERRIDE1.AXIS));

  goto __end;

__end:
  return;
} // MAIN_body__() 





