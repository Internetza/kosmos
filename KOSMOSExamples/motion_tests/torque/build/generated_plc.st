PROGRAM main
  VAR
    clock : ULINT;
    Done : BOOL;
    Axis : AXIS_REF;
    switch : MC_Power;
    readtorque : MC_ReadActualTorque;
    Tn : LREAL;
    settorque : MC_TorqueControl;
    my_sim : MC_Sim;
    resettorque : MC_TorqueControl;
    TON0 : TON;
    SR0 : SR;
    SR1 : SR;
    ADD6_OUT : ULINT;
    GT3_OUT : BOOL;
    switch_Axis : AXIS_REF;
    GT11_OUT : BOOL;
    SEL45_OUT : LREAL;
    settorque_Axis : AXIS_REF;
    LT23_OUT : BOOL;
    AND49_OUT : BOOL;
    SEL28_OUT : LREAL;
  END_VAR

  my_sim();
  Axis := my_sim.Axis;
  ADD6_OUT := ADD(clock, 1);
  clock := ADD6_OUT;
  readtorque(Axis := Axis, Enable := BOOL#TRUE);
  Tn := readtorque.Torque;
  GT3_OUT := GT(clock, 1000);
  switch(Axis := Axis, Enable := GT3_OUT);
  switch_Axis := switch.Axis;
  GT11_OUT := GT(Tn, 40.0);
  SR0(S1 := GT11_OUT);
  SEL45_OUT := SEL(SR0.Q1, 50.0, 30.0);
  settorque(Axis := switch_Axis, Execute := switch.Status, ContinuousUpdate := BOOL#TRUE, Torque := SEL45_OUT);
  settorque_Axis := settorque.Axis;
  TON0(IN := settorque.InTorque, PT := T#5s);
  LT23_OUT := LT(Tn, 10.0);
  AND49_OUT := AND(TON0.Q, LT23_OUT);
  SR1(S1 := AND49_OUT);
  SEL28_OUT := SEL(SR1.Q1, 0.0, 20.0);
  resettorque(Axis := settorque_Axis, Execute := TON0.Q, Torque := SEL28_OUT, TorqueRamp := 5.0);
  Done := resettorque.InTorque;
END_PROGRAM


CONFIGURATION conf

  RESOURCE res ON PLC
    TASK milisecond(INTERVAL := T#1ms,PRIORITY := 0);
    PROGRAM maininstance WITH milisecond : main;
  END_RESOURCE
END_CONFIGURATION
