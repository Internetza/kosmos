FUNCTION_BLOCK wxgetval
  VAR
    getpy : python_gear;
    latch : SR;
  END_VAR
  VAR_INPUT
    name : STRING;
  END_VAR
  VAR_OUTPUT
    val : BOOL;
  END_VAR
  VAR
    CONCAT44_OUT : STRING;
    EQ41_ENO : BOOL;
    EQ41_OUT : BOOL;
    EQ3_ENO : BOOL;
    EQ3_OUT : BOOL;
  END_VAR

  CONCAT44_OUT := CONCAT('ButtonBox.', name, '.GetValue()');
  getpy(N := 100, TRIG := BOOL#TRUE, CODE := CONCAT44_OUT);
  EQ41_OUT := EQ(EN := getpy.ACK, IN1 := getpy.RESULT, IN2 := 'True', ENO => EQ41_ENO);
  EQ3_OUT := EQ(EN := getpy.ACK, IN1 := getpy.RESULT, IN2 := 'False', ENO => EQ3_ENO);
  latch(S1 := EQ41_OUT, R := EQ3_OUT);
  val := latch.Q1;
END_FUNCTION_BLOCK

PROGRAM main
  VAR
    Done : BOOL;
    Slave : AXIS_REF;
    Master : AXIS_REF;
    switch : MC_Power;
    readpos : MC_ReadActualPosition;
    sPn : LREAL;
    mPn : LREAL;
    sVn : LREAL;
    mVn : LREAL;
    setpos : MC_SetPosition;
    readspeed : MC_ReadParameter;
    sim : MC_Sim;
    bt1 : wxgetval;
    Block1 : wxgetval;
    Block2 : wxgetval;
    Block3 : wxgetval;
    Block6 : wxgetval;
    Block4 : wxgetval;
    Block7 : wxgetval;
    MC_Sim0 : MC_Sim;
    MC_Power0 : MC_Power;
    MC_ReadActualPosition0 : MC_ReadActualPosition;
    MC_SetPosition0 : MC_SetPosition;
    MC_ReadParameter0 : MC_ReadParameter;
    MC_MoveAbsolute0 : MC_MoveAbsolute;
    MC_GearIn0 : MC_GearInPos;
    MC_GearOut0 : MC_GearOut;
    MC_Stop0 : MC_Stop;
    MC_ReadStatus0 : MC_ReadStatus;
    readpos_Axis : AXIS_REF;
    MC_ReadActualPosition0_Axis : AXIS_REF;
    switch_Axis : AXIS_REF;
    MC_Power0_Axis : AXIS_REF;
  END_VAR

  sim(MaxAcc := 100.0, MaxDec := 100.0);
  Slave := sim.Axis;
  MC_Sim0(MaxAcc := 10.0, MaxDec := 10.0);
  Master := MC_Sim0.Axis;
  readpos(Axis := Slave, Enable := BOOL#TRUE);
  sPn := readpos.Position;
  readpos_Axis := readpos.Axis;
  readspeed(Axis := readpos_Axis, Enable := BOOL#TRUE, ParameterNumber := 10);
  sVn := readspeed.Value;
  MC_ReadActualPosition0(Axis := Master, Enable := BOOL#TRUE);
  mPn := MC_ReadActualPosition0.Position;
  MC_ReadActualPosition0_Axis := MC_ReadActualPosition0.Axis;
  MC_ReadParameter0(Axis := MC_ReadActualPosition0_Axis, Enable := BOOL#TRUE, ParameterNumber := 10);
  mVn := MC_ReadParameter0.Value;
  bt1(name := 'button_1');
  Block1(name := 'button_2');
  Block6(name := 'button_3');
  Block2(name := 'button_4');
  Block3(name := 'button_5');
  Block4(name := 'button_6');
  Block7(name := 'button_7');
  switch(Axis := Slave, Enable := bt1.val);
  switch_Axis := switch.Axis;
  setpos(Axis := switch_Axis, Execute := switch.Status, Position := 5.0);
  MC_Power0(Axis := Master, Enable := bt1.val);
  MC_Power0_Axis := MC_Power0.Axis;
  MC_SetPosition0(Axis := MC_Power0_Axis, Execute := MC_Power0.Status, Position := 10.0);
  MC_GearIn0(Master := Master, Slave := Slave, Execute := Block6.val, RatioNumerator := 1, RatioDenominator := 1, Velocity := 8);
  MC_MoveAbsolute0(Axis := Master, Execute := Block1.val, Position := 50.0, Velocity := 10.0, Acceleration := 10.0, Deceleration := 10.0);
  MC_GearOut0(Slave := Slave, Execute := Block2.val);
  MC_ReadStatus0(Axis := Slave, Enable := BOOL#TRUE);
  MC_Stop0(Axis := Slave, Execute := Block4.val, Deceleration := 100.0);
END_PROGRAM


CONFIGURATION conf

  RESOURCE res ON PLC
    TASK milisecond(INTERVAL := T#10ms,PRIORITY := 0);
    PROGRAM maininstance WITH milisecond : main;
  END_RESOURCE
END_CONFIGURATION
