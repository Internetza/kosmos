PROGRAM main
  VAR
    clock : ULINT;
    Done : BOOL;
    Axis : AXIS_REF;
    moveA : MC_MoveAbsolute;
    switch : MC_Power;
    readpos : MC_ReadActualPosition;
    Pn : LREAL;
    Vn : LREAL;
    setpos : MC_SetPosition;
    readspeed : MC_ReadParameter;
    sim : MC_WriteBoolParameter;
    block2 : MC_MoveRelative;
    MC_Sim0 : MC_Sim;
    ADD6_OUT : ULINT;
    readpos_Axis : AXIS_REF;
    GT3_OUT : BOOL;
    switch_Axis : AXIS_REF;
    setpos_Axis : AXIS_REF;
    moveA_Axis : AXIS_REF;
    GT28_OUT : BOOL;
  END_VAR

  ADD6_OUT := ADD(clock, 1);
  clock := ADD6_OUT;
  MC_Sim0();
  Axis := MC_Sim0.Axis;
  readpos(Axis := Axis, Enable := BOOL#TRUE);
  Pn := readpos.Position;
  readpos_Axis := readpos.Axis;
  readspeed(Axis := readpos_Axis, Enable := BOOL#TRUE, ParameterNumber := 10);
  Vn := readspeed.Value;
  GT3_OUT := GT(clock, 10);
  switch(Axis := Axis, Enable := GT3_OUT);
  switch_Axis := switch.Axis;
  setpos(Axis := switch_Axis, Execute := switch.Status, Position := 1.0);
  setpos_Axis := setpos.Axis;
  moveA(Axis := setpos_Axis, Execute := setpos.Done, Position := 50.0, Velocity := 5.0, Acceleration := 20.0, Deceleration := 20.0);
  moveA_Axis := moveA.Axis;
  GT28_OUT := GT(clock, 2000);
  block2(Axis := moveA_Axis, Execute := GT28_OUT, Distance := -25.0, Velocity := 5.0, Acceleration := 20.0, Deceleration := 20.0, BufferMode := mcBuffered);
  Done := block2.Done;
  sim(Axis := Axis, Execute := BOOL#TRUE, ParameterNumber := 1000, Value := BOOL#TRUE);
END_PROGRAM


CONFIGURATION conf

  RESOURCE res ON PLC
    TASK milisecond(INTERVAL := T#1ms,PRIORITY := 0);
    PROGRAM maininstance WITH milisecond : main;
  END_RESOURCE
END_CONFIGURATION
