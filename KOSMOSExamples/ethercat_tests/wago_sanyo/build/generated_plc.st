PROGRAM Prog
  VAR
    CLOCK AT %QX0.0.2.28672.1 : BOOL;
    GREENLED AT %QX0.0.2.28688.1 : BOOL := TRUE;
    ControlWord AT %QW0.0.3.24640.0 : UINT;
    StatusWord AT %IW0.0.3.24641.0 : UINT;
    ErrorCode AT %IW0.0.3.24639.0 : UINT;
    ModeDisplay AT %IB0.0.3.24673.0 : SINT;
    ActualPosition AT %ID0.0.3.24676.0 : DINT;
    TargetPosition AT %QD0.0.3.24698.0 : DINT;
  END_VAR

  CLOCK := NOT CLOCK;
END_PROGRAM

PROGRAM test_MCL
  VAR
    CLOCK : UDINT;
  END_VAR
  VAR
    Axis AT %IW0.0.3.0 : INT;
  END_VAR
  VAR
    switch : MC_Power;
    move1 : MC_MoveAbsolute;
    ADD2_OUT : UDINT;
    GT7_OUT : BOOL;
    switch_Axis : AXIS_REF;
  END_VAR

  ADD2_OUT := ADD(CLOCK, 1);
  CLOCK := ADD2_OUT;
  GT7_OUT := GT(CLOCK, 4000);
  switch(Axis := Axis, Enable := GT7_OUT);
  switch_Axis := switch.Axis;
  move1(Axis := switch_Axis, Execute := switch.Status, Position := 1800.0, Velocity := 360.0, Acceleration := 360.0, Deceleration := 360.0);
END_PROGRAM


CONFIGURATION conf
  VAR_GLOBAL
    GetTorqueLimit_0_0_3 : EtherLabGetTorqueLimit;
    SetTorqueLimit_0_0_3 : EtherLabSetTorqueLimit;
  END_VAR

  RESOURCE res ON PLC
    TASK main_task(INTERVAL := T#0.5ms,PRIORITY := 0);
    PROGRAM main_instance WITH main_task : Prog;
    PROGRAM test_MCL_instance WITH main_task : test_MCL;
  END_RESOURCE
END_CONFIGURATION
