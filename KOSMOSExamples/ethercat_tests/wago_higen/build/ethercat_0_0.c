/*
 * Etherlab execution code
 *
 * */

#include <rtdm/rtdm.h>
#include <native/task.h>
#include <native/timer.h>

#include "ecrt.h"

#include "beremiz.h"
#include "iec_types_all.h"

// declaration of interface variables
IEC_BOOL beremiz__QX0_0_2_28672_1;
IEC_BOOL *__QX0_0_2_28672_1 = &beremiz__QX0_0_2_28672_1;
IEC_BOOL beremiz__QX0_0_2_28688_1;
IEC_BOOL *__QX0_0_2_28688_1 = &beremiz__QX0_0_2_28688_1;
IEC_UINT beremiz__IW0_0_3_24641_0;
IEC_UINT *__IW0_0_3_24641_0 = &beremiz__IW0_0_3_24641_0;
IEC_DINT beremiz__ID0_0_3_24676_0;
IEC_DINT *__ID0_0_3_24676_0 = &beremiz__ID0_0_3_24676_0;
IEC_INT beremiz__IW0_0_3_24695_0;
IEC_INT *__IW0_0_3_24695_0 = &beremiz__IW0_0_3_24695_0;
IEC_SINT beremiz__IB0_0_3_24673_0;
IEC_SINT *__IB0_0_3_24673_0 = &beremiz__IB0_0_3_24673_0;
IEC_UINT beremiz__QW0_0_3_24640_0;
IEC_UINT *__QW0_0_3_24640_0 = &beremiz__QW0_0_3_24640_0;
IEC_DINT beremiz__QD0_0_3_24698_0;
IEC_DINT *__QD0_0_3_24698_0 = &beremiz__QD0_0_3_24698_0;
IEC_DINT beremiz__QD0_0_3_24831_0;
IEC_DINT *__QD0_0_3_24831_0 = &beremiz__QD0_0_3_24831_0;
IEC_INT beremiz__QW0_0_3_24689_0;
IEC_INT *__QW0_0_3_24689_0 = &beremiz__QW0_0_3_24689_0;
IEC_SINT beremiz__QB0_0_3_24672_0;
IEC_SINT *__QB0_0_3_24672_0 = &beremiz__QB0_0_3_24672_0;
IEC_DINT beremiz__ID0_0_3_24684_0;
IEC_DINT *__ID0_0_3_24684_0 = &beremiz__ID0_0_3_24684_0;
IEC_UDINT beremiz__ID0_0_3_24829_0;
IEC_UDINT *__ID0_0_3_24829_0 = &beremiz__ID0_0_3_24829_0;

// process data
uint8_t *domain1_pd = NULL;
unsigned int slave2_7000_01;
unsigned int slave2_7000_01_bit;
unsigned int slave2_7010_01;
unsigned int slave2_7010_01_bit;
unsigned int slave3_6041_00;
unsigned int slave3_6064_00;
unsigned int slave3_6077_00;
unsigned int slave3_6061_00;
unsigned int slave3_6040_00;
unsigned int slave3_607a_00;
unsigned int slave3_60ff_00;
unsigned int slave3_6071_00;
unsigned int slave3_6060_00;
unsigned int slave3_606c_00;
unsigned int slave3_60fd_00;

const static ec_pdo_entry_reg_t domain1_regs[] = {
    {0, 2, 0x00000002, 0x08283052, 0x7000, 1, &slave2_7000_01, &slave2_7000_01_bit},
    {0, 2, 0x00000002, 0x08283052, 0x7010, 1, &slave2_7010_01, &slave2_7010_01_bit},
    {0, 3, 0x00000625, 0x69686555, 0x6041, 0, &slave3_6041_00},
    {0, 3, 0x00000625, 0x69686555, 0x6064, 0, &slave3_6064_00},
    {0, 3, 0x00000625, 0x69686555, 0x6077, 0, &slave3_6077_00},
    {0, 3, 0x00000625, 0x69686555, 0x6061, 0, &slave3_6061_00},
    {0, 3, 0x00000625, 0x69686555, 0x6040, 0, &slave3_6040_00},
    {0, 3, 0x00000625, 0x69686555, 0x607a, 0, &slave3_607a_00},
    {0, 3, 0x00000625, 0x69686555, 0x60ff, 0, &slave3_60ff_00},
    {0, 3, 0x00000625, 0x69686555, 0x6071, 0, &slave3_6071_00},
    {0, 3, 0x00000625, 0x69686555, 0x6060, 0, &slave3_6060_00},
    {0, 3, 0x00000625, 0x69686555, 0x606c, 0, &slave3_606c_00},
    {0, 3, 0x00000625, 0x69686555, 0x60fd, 0, &slave3_60fd_00},
    {}
};

// Distributed Clock variables;

unsigned long long comp_period_ns = 500000ULL;

int comp_count = 1;
int comp_count_max;

#define DC_FILTER_CNT          1024

// EtherCAT slave-time-based DC Synchronization variables.
static uint64_t dc_start_time_ns = 0LL;
static uint64_t dc_time_ns = 0;
static uint8_t  dc_started = 0;
static int32_t  dc_diff_ns = 0;
static int32_t  prev_dc_diff_ns = 0;
static int64_t  dc_diff_total_ns = 0LL;
static int64_t  dc_delta_total_ns = 0LL;
static int      dc_filter_idx = 0;
static int64_t  dc_adjust_ns;
static int64_t  system_time_base = 0LL;

static uint64_t dc_first_app_time = 0LL;

unsigned long long frame_period_ns = 0ULL;

int debug_count = 0;
int slave_dc_used = 0;

void dc_init(void);
uint64_t system_time_ns(void);
RTIME system2count(uint64_t time);
void sync_distributed_clocks(void);
void update_master_clock(void);
RTIME calculate_sleeptime(uint64_t wakeup_time);
uint64_t calculate_first(void);

/*****************************************************************************/


/* Slave 1, "EL1088"
 * Vendor ID:       0x00000002
 * Product code:    0x04403052
 * Revision number: 0x00100000
 */

ec_pdo_entry_info_t slave_1_pdo_entries[] = {

};

ec_pdo_info_t slave_1_pdos[] = {

};

ec_sync_info_t slave_1_syncs[] = {
    {0, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {0xff}
};

/* Slave 2, "EL2088"
 * Vendor ID:       0x00000002
 * Product code:    0x08283052
 * Revision number: 0x00100000
 */

ec_pdo_entry_info_t slave_2_pdo_entries[] = {
    {0x7000, 0x01, 1}, /* Output */
    {0x7010, 0x01, 1}, /* Output */
};

ec_pdo_info_t slave_2_pdos[] = {
    {0x1600, 1, slave_2_pdo_entries + 0}, /* Channel 1 */
    {0x1601, 1, slave_2_pdo_entries + 1}, /* Channel 2 */
};

ec_sync_info_t slave_2_syncs[] = {
    {0, EC_DIR_OUTPUT, 2, slave_2_pdos + 0, EC_WD_ENABLE},
    {0xff}
};

/* Slave 3, "EDA7000 CoE Drive"
 * Vendor ID:       0x00000625
 * Product code:    0x69686555
 * Revision number: 0x00000003
 */

ec_pdo_entry_info_t slave_3_pdo_entries[] = {
    {0x6040, 0x00, 16}, /* Controlword */
    {0x607a, 0x00, 32}, /* Target Position */
    {0x60ff, 0x00, 32}, /* Target Velocity */
    {0x6071, 0x00, 16}, /* Target Torque */
    {0x6060, 0x00, 8}, /* Mode of Operation */
    {0x0000, 0x00, 8}, /* None */
    {0x6041, 0x00, 16}, /* Statusword */
    {0x6064, 0x00, 32}, /* Position actual value */
    {0x6077, 0x00, 16}, /* Torque actual value */
    {0x6061, 0x00, 8}, /* Modes of operation display */
    {0x0000, 0x00, 8}, /* None */
    {0x606c, 0x00, 32}, /* Velocity Actual Value */
    {0x60fd, 0x00, 32}, /* Digital Inputs */
};

ec_pdo_info_t slave_3_pdos[] = {
    {0x1600, 6, slave_3_pdo_entries + 0}, /* Receive PDO mapping */
    {0x1a00, 7, slave_3_pdo_entries + 6}, /* Transmit PDO mapping */
};

ec_sync_info_t slave_3_syncs[] = {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {2, EC_DIR_OUTPUT, 1, slave_3_pdos + 0, EC_WD_DISABLE},
    {3, EC_DIR_INPUT, 1, slave_3_pdos + 1, EC_WD_DISABLE},
    {0xff}
};


long long wait_period_ns = 100000LL;

// EtherCAT
static ec_master_t *master = NULL;
static ec_domain_t *domain1 = NULL;
static int first_sent=0;
static ec_slave_config_t *slave1 = NULL;
static ec_slave_config_t *slave2 = NULL;
static ec_slave_config_t *slave3 = NULL;

#define SLOGF(level, format, args...)\
{\
    char sbuf[256];\
    int slen = snprintf(sbuf , sizeof(sbuf) , format , ##args);\
    LogMessage(level, sbuf, slen);\
}

/* EtherCAT plugin functions */
int __init_0_0(int argc,char **argv)
{
    uint32_t abort_code;
    size_t result_size;
    
    abort_code = 0;
    result_size = 0;

    master = ecrt_request_master(0);
    if (!master) {
        SLOGF(LOG_CRITICAL, "EtherCAT master request failed!");
        return -1;
    }

    if(!(domain1 = ecrt_master_create_domain(master))){
        SLOGF(LOG_CRITICAL, "EtherCAT Domain Creation failed!");
        goto ecat_failed;
    }

    // slaves PDO configuration

    if (!(slave1 = ecrt_master_slave_config(master, 0, 1, 0x00000002, 0x04403052))) {
        SLOGF(LOG_CRITICAL, "EtherCAT failed to get slave EL1088 configuration at alias 0 and position 1.");
        goto ecat_failed;
    }

    if (ecrt_slave_config_pdos(slave1, EC_END, slave_1_syncs)) {
        SLOGF(LOG_CRITICAL, "EtherCAT failed to configure PDOs for slave EL1088 at alias 0 and position 1.");
        goto ecat_failed;
    }

    if (!(slave2 = ecrt_master_slave_config(master, 0, 2, 0x00000002, 0x08283052))) {
        SLOGF(LOG_CRITICAL, "EtherCAT failed to get slave EL2088 configuration at alias 0 and position 2.");
        goto ecat_failed;
    }

    if (ecrt_slave_config_pdos(slave2, EC_END, slave_2_syncs)) {
        SLOGF(LOG_CRITICAL, "EtherCAT failed to configure PDOs for slave EL2088 at alias 0 and position 2.");
        goto ecat_failed;
    }

    if (!(slave3 = ecrt_master_slave_config(master, 0, 3, 0x00000625, 0x69686555))) {
        SLOGF(LOG_CRITICAL, "EtherCAT failed to get slave EDA7000 CoE Drive configuration at alias 0 and position 3.");
        goto ecat_failed;
    }

    if (ecrt_slave_config_pdos(slave3, EC_END, slave_3_syncs)) {
        SLOGF(LOG_CRITICAL, "EtherCAT failed to configure PDOs for slave EDA7000 CoE Drive at alias 0 and position 3.");
        goto ecat_failed;
    }


    if (ecrt_domain_reg_pdo_entry_list(domain1, domain1_regs)) {
        SLOGF(LOG_CRITICAL, "EtherCAT PDO registration failed!");
        goto ecat_failed;
    }

    ecrt_master_set_send_interval(master, common_ticktime__);

    // slaves initialization
/*

    {
        uint8_t value[2];
        EC_WRITE_U16((uint8_t *)value, 0x0007);
        if (ecrt_master_sdo_download(master, 3, 0x2025, 0x00, (uint8_t *)value, 2, &abort_code)) {
            SLOGF(LOG_CRITICAL, "EtherCAT Failed to initialize slave EDA7000 CoE Drive at alias 0 and position 3. Error: %d", abort_code);
            goto ecat_failed;
        }
    }

    {
        uint8_t value[2];
        EC_WRITE_U16((uint8_t *)value, 0x05dc);
        if (ecrt_master_sdo_download(master, 3, 0x60e0, 0x00, (uint8_t *)value, 2, &abort_code)) {
            SLOGF(LOG_CRITICAL, "EtherCAT Failed to initialize slave EDA7000 CoE Drive at alias 0 and position 3. Error: %d", abort_code);
            goto ecat_failed;
        }
    }

    {
        uint8_t value[2];
        EC_WRITE_U16((uint8_t *)value, 0x05dc);
        if (ecrt_master_sdo_download(master, 3, 0x60e1, 0x00, (uint8_t *)value, 2, &abort_code)) {
            SLOGF(LOG_CRITICAL, "EtherCAT Failed to initialize slave EDA7000 CoE Drive at alias 0 and position 3. Error: %d", abort_code);
            goto ecat_failed;
        }
    }

*/
    // configure DC SYNC0/1 Signal


    // select reference clock
#if DC_ENABLE
    {
        int ret;
        
        ret = ecrt_master_select_reference_clock(master, slave0);
        if (ret <0) {
            fprintf(stderr, "Failed to select reference clock : %s\n",
                strerror(-ret));
            return ret;
        }
    }
#endif

    // extracting default value for not mapped entry in output PDOs
/*

*/

#if DC_ENABLE
    dc_init();
#endif

    if (ecrt_master_activate(master)){
        SLOGF(LOG_CRITICAL, "EtherCAT Master activation failed");
        goto ecat_failed;
    }

    if (!(domain1_pd = ecrt_domain_data(domain1))) {
        SLOGF(LOG_CRITICAL, "Failed to map EtherCAT process data");
        goto ecat_failed;
    }

    SLOGF(LOG_INFO, "Master 0 activated.");
    
    first_sent = 0;

    return 0;

ecat_failed:
    ecrt_release_master(master);
    return -1;

}

void __cleanup_0_0(void)
{
    //release master
    ecrt_release_master(master);
    first_sent = 0;
}

void __retrieve_0_0(void)
{
    // receive ethercat
    if(first_sent){
        ecrt_master_receive(master);
        ecrt_domain_process(domain1);
    beremiz__IW0_0_3_24641_0 = EC_READ_U16(domain1_pd + slave3_6041_00);
    beremiz__ID0_0_3_24676_0 = EC_READ_S32(domain1_pd + slave3_6064_00);
    beremiz__IW0_0_3_24695_0 = EC_READ_S16(domain1_pd + slave3_6077_00);
    beremiz__IB0_0_3_24673_0 = EC_READ_S8(domain1_pd + slave3_6061_00);
    beremiz__ID0_0_3_24684_0 = EC_READ_S32(domain1_pd + slave3_606c_00);
    beremiz__ID0_0_3_24829_0 = EC_READ_U32(domain1_pd + slave3_60fd_00);
    }

}

/*
static RTIME _last_occur=0;
static RTIME _last_publish=0;
RTIME _current_lag=0;
RTIME _max_jitter=0;
static inline RTIME max(RTIME a,RTIME b){return a>b?a:b;}
*/

void __publish_0_0(void)
{
    EC_WRITE_BIT(domain1_pd + slave2_7000_01, slave2_7000_01_bit, beremiz__QX0_0_2_28672_1);
    EC_WRITE_BIT(domain1_pd + slave2_7010_01, slave2_7010_01_bit, beremiz__QX0_0_2_28688_1);
    EC_WRITE_U16(domain1_pd + slave3_6040_00, beremiz__QW0_0_3_24640_0);
    EC_WRITE_S32(domain1_pd + slave3_607a_00, beremiz__QD0_0_3_24698_0);
    EC_WRITE_S32(domain1_pd + slave3_60ff_00, beremiz__QD0_0_3_24831_0);
    EC_WRITE_S16(domain1_pd + slave3_6071_00, beremiz__QW0_0_3_24689_0);
    EC_WRITE_S8(domain1_pd + slave3_6060_00, beremiz__QB0_0_3_24672_0);
    ecrt_domain_queue(domain1);
    {
        /*
        RTIME current_time = rt_timer_read();
        // Limit spining max 1/5 of common_ticktime
        RTIME maxdeadline = current_time + (common_ticktime__ / 5);
        RTIME deadline = _last_occur ? 
            _last_occur + common_ticktime__ : 
            current_time + _max_jitter; 
        if(deadline > maxdeadline) deadline = maxdeadline;
        _current_lag = deadline - current_time;
        if(_last_publish != 0){
            RTIME period = current_time - _last_publish;
            if(period > common_ticktime__ )
                _max_jitter = max(_max_jitter, period - common_ticktime__);
            else
                _max_jitter = max(_max_jitter, common_ticktime__ - period);
        }
        _last_publish = current_time;
        _last_occur = current_time;
        while(current_time < deadline) {
            _last_occur = current_time; //Drift backward by default
            current_time = rt_timer_read();
        }
        if( _max_jitter * 10 < common_ticktime__ && _current_lag < _max_jitter){
            //Consuming security margin ?
            _last_occur = current_time; //Drift forward
        }
        */
    }

#if DC_ENABLE
    if (comp_count == 0)
        sync_distributed_clocks();
#endif

    ecrt_master_send(master);
    first_sent = 1;

#if DC_ENABLE
    if (comp_count == 0)
        update_master_clock();

    comp_count++;
    
    if (comp_count == comp_count_max)
        comp_count = 0;
#endif

}

/* Test Function For Parameter (SDO) Set */

/*
void GetSDOData(void){
    uint32_t abort_code, test_value;
    size_t result_size;
    uint8_t value[4];

    abort_code = 0;
    result_size = 0;
    test_value = 0;

    if (ecrt_master_sdo_upload(master, 0, 0x1000, 0x0, (uint8_t *)value, 4, &result_size, &abort_code)) {
        SLOGF(LOG_CRITICAL, "EtherCAT failed to get SDO Value");
        }
        test_value = EC_READ_S32((uint8_t *)value);
        SLOGF(LOG_INFO, "SDO Value %d", test_value);
}
*/

int GetMasterData(void){
    master = ecrt_open_master(0);
    if (!master) {
        SLOGF(LOG_CRITICAL, "EtherCAT master request failed!");
        return -1;
    }
    return 0;
}

void ReleaseMasterData(void){
    ecrt_release_master(master);
}

uint32_t GetSDOData(uint16_t slave_pos, uint16_t idx, uint8_t subidx, int size){
    uint32_t abort_code, return_value;
    size_t result_size;
    uint8_t value[size];

    abort_code = 0;
    result_size = 0;

    if (ecrt_master_sdo_upload(master, slave_pos, idx, subidx, (uint8_t *)value, size, &result_size, &abort_code)) {
        SLOGF(LOG_CRITICAL, "EtherCAT failed to get SDO Value %d %d", idx, subidx);
    }

    return_value = EC_READ_S32((uint8_t *)value);
    //SLOGF(LOG_INFO, "SDO Value %d", return_value);

    return return_value;
}

/*****************************************************************************/

void dc_init(void)
{
    slave_dc_used = 1;

    frame_period_ns = common_ticktime__;
    if (frame_period_ns <= comp_period_ns) {
        comp_count_max = comp_period_ns / frame_period_ns;
        comp_count = 0;
    } else  {
        comp_count_max = 1;
        comp_count = 0;
    }

    /* Set the initial master time */
    dc_start_time_ns = system_time_ns();
    dc_time_ns = dc_start_time_ns;

    /* by woonggy */
    dc_first_app_time = dc_start_time_ns;

    /*
     * Attention : The initial application time is also used for phase
     * calculation for the SYNC0/1 interrupts. Please be sure to call it at
     * the correct phase to the realtime cycle.
     */
    ecrt_master_application_time(master, dc_start_time_ns);
}

/****************************************************************************/

/*
 * Get the time in ns for the current cpu, adjusted by system_time_base.
 *
 * \attention Rather than calling rt_timer_read() directly, all application
 * time calls should use this method instead.
 *
 * \ret The time in ns.
 */
uint64_t system_time_ns(void)
{
    RTIME time = rt_timer_read();   // wkk

    if (unlikely(system_time_base > (SRTIME) time)) {
        fprintf(stderr, "%s() error: system_time_base greater than"
                " system time (system_time_base: %ld, time: %llu\n",
                __func__, system_time_base, time);
        return time;
    }
    else {
        return time - system_time_base;
    }
}

/****************************************************************************/

// Convert system time to Xenomai time in counts (via the system_time_base).
RTIME system2count(uint64_t time)
{
    RTIME ret;

    if ((system_time_base < 0) &&
            ((uint64_t) (-system_time_base) > time)) {
        fprintf(stderr, "%s() error: system_time_base less than"
                " system time (system_time_base: %I64d, time: %ld\n",
                __func__, system_time_base, time);
        ret = time;
    }
    else {
        ret = time + system_time_base;
    }

    return (RTIME) rt_timer_ns2ticks(ret); // wkk
}

/*****************************************************************************/

// Synchronise the distributed clocks
void sync_distributed_clocks(void)
{
    uint32_t ref_time = 0;
    RTIME prev_app_time = dc_time_ns;

    // get reference clock time to synchronize master cycle
    if(!ecrt_master_reference_clock_time(master, &ref_time)) {
        dc_diff_ns = (uint32_t) prev_app_time - ref_time;
    }
    // call to sync slaves to ref slave
    ecrt_master_sync_slave_clocks(master);
    // set master time in nano-seconds
    dc_time_ns = system_time_ns();
    ecrt_master_application_time(master, dc_time_ns);
}

/*****************************************************************************/

/*
 * Return the sign of a number
 * ie -1 for -ve value, 0 for 0, +1 for +ve value
 * \ret val the sign of the value
 */
#define sign(val) \
        ({ typeof (val) _val = (val); \
        ((_val > 0) - (_val < 0)); })

/*****************************************************************************/

/*
 * Update the master time based on ref slaves time diff
 * called after the ethercat frame is sent to avoid time jitter in
 * sync_distributed_clocks()
 */
void update_master_clock(void)
{
    // calc drift (via un-normalised time diff)
    int32_t delta = dc_diff_ns - prev_dc_diff_ns;
    prev_dc_diff_ns = dc_diff_ns;

    // normalise the time diff
    dc_diff_ns = dc_diff_ns >= 0 ?
            ((dc_diff_ns + (int32_t)(frame_period_ns / 2)) %
                    (int32_t)frame_period_ns) - (frame_period_ns / 2) :
                    ((dc_diff_ns - (int32_t)(frame_period_ns / 2)) %
                            (int32_t)frame_period_ns) - (frame_period_ns / 2) ;

    // only update if primary master
    if (dc_started) {
        // add to totals
        dc_diff_total_ns += dc_diff_ns;
        dc_delta_total_ns += delta;
        dc_filter_idx++;

        if (dc_filter_idx >= DC_FILTER_CNT) {
            dc_adjust_ns += dc_delta_total_ns >= 0 ?
                    ((dc_delta_total_ns + (DC_FILTER_CNT / 2)) / DC_FILTER_CNT) :
                    ((dc_delta_total_ns - (DC_FILTER_CNT / 2)) / DC_FILTER_CNT) ;

            // and add adjustment for general diff (to pull in drift)
            dc_adjust_ns += sign(dc_diff_total_ns / DC_FILTER_CNT);

            // limit crazy numbers (0.1% of std cycle time)
            if (dc_adjust_ns < -1000) {
                dc_adjust_ns = -1000;
            }
            if (dc_adjust_ns > 1000) {
                dc_adjust_ns =  1000;
            }
            // reset
            dc_diff_total_ns = 0LL;
            dc_delta_total_ns = 0LL;
            dc_filter_idx = 0;
        }
        // add cycles adjustment to time base (including a spot adjustment)
        system_time_base += dc_adjust_ns + sign(dc_diff_ns);
    }
    else {
        dc_started = (dc_diff_ns != 0);

        if (dc_started) {
#if DC_ENABLE && DEBUG_MODE
            // output first diff
            fprintf(stderr, "First master diff: %d\n", dc_diff_ns);
#endif
            // record the time of this initial cycle
            dc_start_time_ns = dc_time_ns;
        }
    }
}

/*****************************************************************************/

/*
 * Calculate the sleeptime
 */
RTIME calculate_sleeptime(uint64_t wakeup_time)
{
    RTIME wakeup_count = system2count (wakeup_time);
    RTIME current_count = rt_timer_read();

    if ((wakeup_count < current_count) || (wakeup_count > current_count + (50 * frame_period_ns)))  {
        fprintf(stderr, "%s(): unexpected wake time! wc = %lld\tcc = %lld\n", __func__, wakeup_count, current_count);
    }

    return wakeup_count;
}

/*****************************************************************************/

/*
 * Calculate the sleeptime
 */
uint64_t calculate_first(void)
{
    uint64_t dc_remainder = 0LL;
    uint64_t dc_phase_set_time = 0LL;
    
    dc_phase_set_time = system_time_ns()+ frame_period_ns * 10;
    dc_remainder = (dc_phase_set_time - dc_first_app_time) % frame_period_ns;

    return dc_phase_set_time + frame_period_ns - dc_remainder;
}

/*****************************************************************************/
