/*
 * Ethercat CIA402 node execution code
 *
 * */

#include "ecrt.h"

#include "beremiz.h"
#include "iec_types_all.h"

#include "accessor.h"
#include "POUS.h"

/* From CiA402, page 27

        Table 30 - State coding
    Statusword      |      PDS FSA state
xxxx xxxx x0xx 0000 | Not ready to switch on
xxxx xxxx x1xx 0000 | Switch on disabled
xxxx xxxx x01x 0001 | Ready to switch on
xxxx xxxx x01x 0011 | Switched on
xxxx xxxx x01x 0111 | Operation enabled
xxxx xxxx x00x 0111 | Quick stop active
xxxx xxxx x0xx 1111 | Fault reaction active
xxxx xxxx x0xx 1000 | Fault
*/

//ssh_add
/* From CiA402, Page 63 Statusword for homing mode

		Table 106 - Definition of bit 10, bit 12, bit 13

xx00 x0xx xxxx xxxx | Homing procedure is in progress
xx00 x1xx xxxx xxxx | Homing procedure is interrupted or not started
xx01 x0xx xxxx xxxx | Homing is attained, but target is not reached
xx01 x1xx xxxx xxxx | Homing procedure is completed successfully
xx10 x0xx xxxx xxxx | Homing error occurred, velocity is not 0
xx10 x1xx xxxx xxxx | Homing error occurred, velocity is 0
xx11 xxxx xxxx xxxx | reserved
*/

#define FSAFromStatusWord(SW) (SW & 0x006f)
//ssh_add
#define HomingStatusWord(SW) (SW & 0x3400)
#define FaultFromStatusWord(SW) (SW & 0x0008)
#define NotReadyToSwitchOn  0b00000000 FSA_sep 0b00100000
#define SwitchOnDisabled    0b01000000 FSA_sep 0b01100000
#define ReadyToSwitchOn     0b00100001
#define SwitchedOn          0b00100011
#define OperationEnabled    0b00100111
#define QuickStopActive     0b00000111
#define FaultReactionActive 0b00001111 FSA_sep 0b00101111
#define Fault               0b00001000 FSA_sep 0b00101000

//ssh_add
#define HomingInProgress	0b0000000000000000
#define HomingNotRunning	0b0000010000000000
#define HomingNotReached	0b0001000000000000
#define Homing_Completed	0b0001010000000000
#define HomingErrorInVelo	0b0010000000000000
#define HomingErrorNotVelo	0b0010010000000000
#define HomingReserved		0b0011000000000000 FSA_sep 0b0011010000000000

// StatusWord bits :
#define SW_ReadyToSwitchOn     0x0001
#define SW_SwitchedOn          0x0002
#define SW_OperationEnabled    0x0004
#define SW_Fault               0x0008
#define SW_VoltageEnabled      0x0010
#define SW_QuickStop           0x0020
#define SW_SwitchOnDisabled    0x0040
#define SW_Warning             0x0080
#define SW_Remote              0x0200
#define SW_TargetReached       0x0400
#define SW_InternalLimitActive 0x0800

//ssh_add
#define SW_HomingAttained		0x1000
#define SW_HomingError			0x2000

// ControlWord bits :
#define SwitchOn        0x0001
#define EnableVoltage   0x0002
#define QuickStop       0x0004
#define EnableOperation 0x0008
#define FaultReset      0x0080
#define Halt            0x0100

//ssh_add
//#define Homing_OperationStart 0x0010
#define Homing_OperationStart_Origin 0x0010
#define Homing_OperationStart_Edit 0x001F

IEC_INT beremiz__IW0_0_3 = 3;
IEC_INT *__IW0_0_3 = &beremiz__IW0_0_3;
IEC_INT beremiz__IW0_0_3_402;
IEC_INT *__IW0_0_3_402 = &beremiz__IW0_0_3_402;



typedef struct {
    MC_REAL_ARRAY CenterPoint;
    MC_REAL_ARRAY CosRay;
    MC_REAL_ARRAY SinRay;
    MC_REAL_ARRAY CosAxis;
    MC_REAL_ARRAY SinAxis;
    double Radius;
    double Angle;
    double DistanceCurv;
    MC_REAL_ARRAY Pf;
    MC_REAL_ARRAY Vf;
} CircleData;
    int __MK_Init();
    void __MK_Cleanup();
    void __MK_Retrieve();
    void __MK_Publish();
    void __MK_ComputeAxis(int);
    
    typedef enum {
        mc_mode_none, // No motion mode
        mc_mode_csp,  // Continuous Synchronous Positionning mode
        mc_mode_csv,  // Continuous Synchronous Velocity mode
        mc_mode_cst,  // Continuous Synchronous Torque mode
        mc_mode_hm, // Homing mode
    } mc_axismotionmode_enum;
    
    typedef struct {
       IEC_BOOL Power;
       IEC_BOOL CommunicationReady;
       IEC_UINT NetworkPosition;
       IEC_BOOL ReadyForPowerOn;
       IEC_BOOL PowerFeedback;
       IEC_BOOL HomingOperationStart;
       IEC_BOOL HomingCompleted;
       IEC_BOOL ErrorCodeEnabled;
       IEC_UINT ErrorCode;
       IEC_BOOL DriveFault;
       IEC_BOOL DriveFaultReset;
       IEC_BOOL DigitalInputsEnabled;
       IEC_DWORD DigitalInputs;
       IEC_BOOL DigitalOutputsEnabled;
       IEC_DWORD DigitalOutputs;
       IEC_BOOL TouchProbeEnabled;
       IEC_WORD TouchProbeFunction;
       IEC_WORD TouchProbeStatus;
       IEC_DINT TouchProbePos1PosValue;
       IEC_DINT TouchProbePos1NegValue;
       IEC_BOOL TouchProbeRisingTrigger;
       IEC_BOOL TouchProbeSignalFound;
       IEC_LREAL Axis_Zpulse;
       IEC_DINT ActualRawPosition;
       IEC_DINT ActualRawVelocity;
       IEC_DINT ActualRawTorque;
       IEC_DINT RawPositionSetPoint;
       IEC_DINT RawVelocitySetPoint;
       IEC_DINT RawTorqueSetPoint;
       mc_axismotionmode_enum AxisMotionMode;
       /*PLCopen TC2 parameters (MC_{Read,Write}{,Bool}Parameter)*/
       IEC_LREAL CommandedPosition; /*Commanded position (#1,R)*/
       IEC_LREAL SWLimitPos; /*Positive Software limit switch position (#2,R/W)*/
       IEC_LREAL SWLimitNeg; /*Negative Software limit switch position (#3,R/W)*/
       IEC_BOOL EnableLimitPos; /*Enable positive software limit switch (#4,R/W)*/
       IEC_BOOL EnableLimitNeg; /*Enable negative software limit switch (#5,R/W)*/
       IEC_BOOL EnablePosLagMonitoring; /*Enable monitoring of position lag (#6,R/W)*/
       IEC_LREAL MaxPositionLag; /*Maximal position lag (#7,R/W)*/
       IEC_LREAL MaxVelocitySystem; /*Maximal allowed velocity of the axis in the motion system (#8,R)*/
       IEC_LREAL MaxVelocityAppl; /*Maximal allowed velocity of the axis in the application (#9,R/W)*/
       IEC_LREAL ActualVelocity; /*Actual velocity (#10,R)*/
       IEC_LREAL CommandedVelocity; /*Commanded velocity (#11,R)*/
       IEC_LREAL MaxAccelerationSystem; /*Maximal allowed acceleration of the axis in themotion system (#12,R)*/
       IEC_LREAL MaxAccelerationAppl; /*Maximal allowed acceleration of the axis in theapplication (#13,R/W)*/
       IEC_LREAL MaxDecelerationSystem; /*Maximal allowed deceleration of the axis in themotion system (#14,R)*/
       IEC_LREAL MaxDecelerationAppl; /*Maximal allowed deceleration of the axis in theapplication (#15,R/W)*/
       IEC_LREAL MaxJerkSystem; /*Maximum allowed jerk of the axis in the motionsystem (#16,R)*/
       IEC_LREAL MaxJerkAppl; /*Maximum allowed jerk of the axis in the application (#17,R/W)*/
       IEC_BOOL Simulation; /*Simulation Mode (#1000,R/W)*/
       IEC_LREAL PositionSetPoint; /*Position SetPoint (#1001,R)*/
       IEC_LREAL VelocitySetPoint; /*Velocity SetPoint (#1002,R)*/
       IEC_LREAL RatioNumerator; /*Drive_Unit = PLCopen_Unit * RatioNumerator / RatioDenominator (#1003,R/W)*/
       IEC_LREAL RatioDenominator; /*Drive_Unit = PLCopen_Unit * RatioNumerator / RatioDenominator (#1004,R/W)*/
       IEC_LREAL PositionOffset; /*SentPosition = (PositionSepoint + PosotionOffset) * RatioNumerator / RatioDenominator (#1005,R/W)*/
       IEC_BOOL LimitSwitchNC; /*Set if limit switches are normaly closed (#1006,R/W)*/
       IEC_LREAL JerkSetPoint; /*Jerk setpoint (#1007,R/W)*/
       IEC_LREAL ActualPosition; /*Position from drive, scaled but without offset. (#1008,R)*/
       IEC_LREAL HomingLimitWindow; /*Distance at which soft limit is alredy valid during homing (#1009,R/W)*/
       IEC_LREAL HomingVelocity; /*Velocity applied on drive while homing (#1010,R/W)*/
       IEC_LREAL HomingVelocitySearchIndexPulse; /*Velocity applied on drive while Search Index Pulse (#1018,R/W)*/
       IEC_LREAL TorqueSetPoint; /*Torque SetPoint (#1011,R)*/
       IEC_LREAL ActualTorque; /*Torque from drive scaled (#1012,R)*/
       IEC_LREAL TorqueRatioNumerator; /*Drive_Unit = PLCopen_Unit * TorqueRatioNumerator / TorqueRatioDenominator (#1013,R/W)*/
       IEC_LREAL TorqueRatioDenominator; /*Drive_Unit = PLCopen_Unit * TorqueRatioNumerator / TorqueRatioDenominator (#1014,R/W)*/
       IEC_LREAL AccelerationSetPoint; /*AccelerationSetPoint (#1015,R)*/
       IEC_LREAL ModuloAxisRange; /*Range of valid positions in modulo axis (#1016     ,R/W)*/
       IEC_LREAL ActualAcceleration; /*Acceleration calculated from velocity variation (#1017,R)*/
       void (*__mcl_func_MC_GetTorqueLimit)(MC_GETTORQUELIMIT *data__);
       void (*__mcl_func_MC_SetTorqueLimit)(MC_SETTORQUELIMIT *data__);
    }axis_s;
    
    typedef struct {
      double x;
      double y;
    } point2d_s;
    
    typedef struct {
      int count;
      point2d_s *points;
    } array2d_s;
    
    typedef struct {
      int64_t OnCompensation;
      int64_t OffCompensation;
      double Hysteresis;
    } track_s;
    
    typedef struct {
      int count;
      track_s *tracks;
    } tracklist_s;
    
    #define AD_BOTH     0
    #define AD_POSITIVE 1
    #define AD_NEGATIVE 2
    
    #define CSM_POSITION 0
    #define CSM_TIME     1
    
    typedef struct {
      int TrackNumber;
      double FirstOnPosition;
      double LastOnPosition;
      int AxisDirection;
      int CamSwitchMode;
      uint64_t Duration;
    } camswitch_s;
    
    typedef struct {
      int count;
      camswitch_s *camswitches;
    } camswitchlist_s;
    
    typedef struct {
       double _[9];
    } mc_matrix;
    
    typedef struct {
       double dist_curv;
       CircleData segment_profile;
       double Vmvt;
       double Vf;
       double Acc;
       double Dec;
    } path_data_segment_s;
    
    typedef struct {
      int count;
      path_data_segment_s *segments;
    } path_data_s;
    
    int __MK_Alloc_AXIS_REF();
    axis_s* __MK_GetPublic_AXIS_REF(int index);
    int __MK_CheckPublicValid_AXIS_REF(int index);
    void __MK_Set_AXIS_REF_Pos(int index, int pos);
    int __MK_Get_AXIS_REF_Pos(int index);
    int __MK_Alloc_MC_TP_REF();
    array2d_s* __MK_GetPublic_MC_TP_REF(int index);
    int __MK_CheckPublicValid_MC_TP_REF(int index);
    int __MK_Alloc_MC_TV_REF();
    array2d_s* __MK_GetPublic_MC_TV_REF(int index);
    int __MK_CheckPublicValid_MC_TV_REF(int index);
    int __MK_Alloc_MC_TA_REF();
    array2d_s* __MK_GetPublic_MC_TA_REF(int index);
    int __MK_CheckPublicValid_MC_TA_REF(int index);
    int __MK_Alloc_MC_CAMSWITCH_REF();
    camswitchlist_s* __MK_GetPublic_MC_CAMSWITCH_REF(int index);
    int __MK_CheckPublicValid_MC_CAMSWITCH_REF(int index);
    int __MK_Alloc_MC_OUTPUT_REF();
    uint32_t* __MK_GetPublic_MC_OUTPUT_REF(int index);
    int __MK_CheckPublicValid_MC_OUTPUT_REF(int index);
    int __MK_Alloc_MC_TRACK_REF();
    tracklist_s* __MK_GetPublic_MC_TRACK_REF(int index);
    int __MK_CheckPublicValid_MC_TRACK_REF(int index);
    int __MK_Alloc_MC_TRIGGER_REF();
    uint8_t* __MK_GetPublic_MC_TRIGGER_REF(int index);
    int __MK_CheckPublicValid_MC_TRIGGER_REF(int index);
    int __MK_Alloc_MC_CAM_REF();
    array2d_s* __MK_GetPublic_MC_CAM_REF(int index);
    int __MK_CheckPublicValid_MC_CAM_REF(int index);
    int __MK_Alloc_MC_CAM_ID();


static IEC_BOOL __FirstTick = 1;

typedef struct {
    IEC_UINT *ControlWord;
    IEC_SINT *ModesOfOperation;
    IEC_UINT *StatusWord;
    IEC_SINT *ModesOfOperationDisplay;
    IEC_DINT *TargetPosition;
    IEC_DINT *TargetVelocity;
    IEC_INT *TargetTorque;
    IEC_DINT *ActualPosition;
    IEC_INT *ActualTorque;
    IEC_UDINT *DigitalInputs;
    axis_s* axis;
} __CIA402Node;

#define AxsPub __CIA402Node_0_0_3

static __CIA402Node AxsPub;

IEC_UINT *__QW0_0_3_24640_0;
IEC_SINT *__QB0_0_3_24672_0;
IEC_UINT *__IW0_0_3_24641_0;
IEC_SINT *__IB0_0_3_24673_0;
IEC_DINT *__QD0_0_3_24698_0;
IEC_DINT *__QD0_0_3_24831_0;
IEC_INT *__QW0_0_3_24689_0;
IEC_DINT *__ID0_0_3_24676_0;
IEC_INT *__IW0_0_3_24695_0;
IEC_UDINT *__ID0_0_3_24829_0;


extern void ETHERLABGETTORQUELIMIT_body__(ETHERLABGETTORQUELIMIT* data__);
void __GetTorqueLimit_0_0_3(MC_GETTORQUELIMIT *data__) {
__DECLARE_GLOBAL_PROTOTYPE(ETHERLABGETTORQUELIMIT, GETTORQUELIMIT_0_0_3);
ETHERLABGETTORQUELIMIT* GETTORQUELIMIT_0_0_3 = __GET_GLOBAL_GETTORQUELIMIT_0_0_3();
__SET_VAR(GETTORQUELIMIT_0_0_3->, POS, AxsPub.axis->NetworkPosition);
    __SET_VAR(GETTORQUELIMIT_0_0_3->,POS, 3);
    __SET_VAR(GETTORQUELIMIT_0_0_3->,EXECUTE, __GET_VAR(data__->EXECUTE));
ETHERLABGETTORQUELIMIT_body__(GETTORQUELIMIT_0_0_3);
    __SET_VAR(data__->,DONE, __GET_VAR(GETTORQUELIMIT_0_0_3->DONE));
    __SET_VAR(data__->,BUSY, __GET_VAR(GETTORQUELIMIT_0_0_3->BUSY));
    __SET_VAR(data__->,ERROR, __GET_VAR(GETTORQUELIMIT_0_0_3->ERROR));
    __SET_VAR(data__->,TORQUELIMITPOS, __GET_VAR(GETTORQUELIMIT_0_0_3->TORQUELIMITPOS));
    __SET_VAR(data__->,TORQUELIMITNEG, __GET_VAR(GETTORQUELIMIT_0_0_3->TORQUELIMITNEG));
}


extern void ETHERLABSETTORQUELIMIT_body__(ETHERLABSETTORQUELIMIT* data__);
void __SetTorqueLimit_0_0_3(MC_SETTORQUELIMIT *data__) {
__DECLARE_GLOBAL_PROTOTYPE(ETHERLABSETTORQUELIMIT, SETTORQUELIMIT_0_0_3);
ETHERLABSETTORQUELIMIT* SETTORQUELIMIT_0_0_3 = __GET_GLOBAL_SETTORQUELIMIT_0_0_3();
__SET_VAR(SETTORQUELIMIT_0_0_3->, POS, AxsPub.axis->NetworkPosition);
    __SET_VAR(SETTORQUELIMIT_0_0_3->,POS, 3);
    __SET_VAR(SETTORQUELIMIT_0_0_3->,EXECUTE, __GET_VAR(data__->EXECUTE));
    __SET_VAR(SETTORQUELIMIT_0_0_3->,TORQUELIMITPOS, __GET_VAR(data__->TORQUELIMITPOS));
    __SET_VAR(SETTORQUELIMIT_0_0_3->,TORQUELIMITNEG, __GET_VAR(data__->TORQUELIMITNEG));
ETHERLABSETTORQUELIMIT_body__(SETTORQUELIMIT_0_0_3);
    __SET_VAR(data__->,DONE, __GET_VAR(SETTORQUELIMIT_0_0_3->DONE));
    __SET_VAR(data__->,BUSY, __GET_VAR(SETTORQUELIMIT_0_0_3->BUSY));
    __SET_VAR(data__->,ERROR, __GET_VAR(SETTORQUELIMIT_0_0_3->ERROR));
}


int __init_0_0_3()
{
    __FirstTick = 1;
    __CIA402Node_0_0_3.ControlWord = __QW0_0_3_24640_0;
    __CIA402Node_0_0_3.ModesOfOperation = __QB0_0_3_24672_0;
    __CIA402Node_0_0_3.StatusWord = __IW0_0_3_24641_0;
    __CIA402Node_0_0_3.ModesOfOperationDisplay = __IB0_0_3_24673_0;
    __CIA402Node_0_0_3.TargetPosition = __QD0_0_3_24698_0;
    __CIA402Node_0_0_3.TargetVelocity = __QD0_0_3_24831_0;
    __CIA402Node_0_0_3.TargetTorque = __QW0_0_3_24689_0;
    __CIA402Node_0_0_3.ActualPosition = __ID0_0_3_24676_0;
    __CIA402Node_0_0_3.ActualTorque = __IW0_0_3_24695_0;
    __CIA402Node_0_0_3.DigitalInputs = __ID0_0_3_24829_0;
    return 0;
}

void __cleanup_0_0_3()
{
}

void __retrieve_0_0_3()
{
	if (__FirstTick) {
		*__IW0_0_3_402 = __MK_Alloc_AXIS_REF();
		AxsPub.axis = 
            __MK_GetPublic_AXIS_REF(*__IW0_0_3_402);
		AxsPub.axis->NetworkPosition = beremiz__IW0_0_3;
        __CIA402Node_0_0_3.axis->DigitalInputsEnabled = 1;
        __CIA402Node_0_0_3.axis->RatioNumerator = 65536.0;
        __CIA402Node_0_0_3.axis->RatioDenominator = 360.0;
        __CIA402Node_0_0_3.axis->PositionOffset = 0.0;
        __CIA402Node_0_0_3.axis->JerkSetPoint = 0.0;
        __CIA402Node_0_0_3.axis->HomingLimitWindow = 10.0;
        __CIA402Node_0_0_3.axis->HomingVelocity = 360.0;
        __CIA402Node_0_0_3.axis->HomingVelocitySearchIndexPulse = 0.0;
        __CIA402Node_0_0_3.axis->TorqueRatioNumerator = 10.0;
        __CIA402Node_0_0_3.axis->TorqueRatioDenominator = 1.0;
        __CIA402Node_0_0_3.axis->ModuloAxisRange = 0.0;
        __CIA402Node_0_0_3.axis->__mcl_func_MC_GetTorqueLimit = __GetTorqueLimit_0_0_3;
        __CIA402Node_0_0_3.axis->__mcl_func_MC_SetTorqueLimit = __SetTorqueLimit_0_0_3;
		__FirstTick = 0;
	}

	// Default variables retrieve
	AxsPub.axis->CommunicationReady = 
        *(AxsPub.StatusWord) != 0;
#define FSA_sep || FSA ==
    {
        uint16_t FSA = FSAFromStatusWord(*(AxsPub.StatusWord));
        AxsPub.axis->ReadyForPowerOn = FSA == ReadyToSwitchOn;
        AxsPub.axis->PowerFeedback = FSA == OperationEnabled;
    }
#undef FSA_sep 
    AxsPub.axis->ActualRawPosition = *(AxsPub.ActualPosition);
    AxsPub.axis->ActualRawTorque = *(AxsPub.ActualTorque);

	// Extra variables retrieve
    __CIA402Node_0_0_3.axis->DigitalInputs = *(__CIA402Node_0_0_3.DigitalInputs);
}

void __publish_0_0_3()
{
	IEC_BOOL power = 
        ((*(AxsPub.StatusWord) & SW_VoltageEnabled) != 0) 
        && AxsPub.axis->Power;
    uint16_t CW = *(AxsPub.ControlWord);

#define FSA_sep : case
	// CIA402 node state transition computation
	switch (FSAFromStatusWord(*(AxsPub.StatusWord))) {
	    case SwitchOnDisabled :
            CW &= ~(SwitchOn | FaultReset);
            CW |= EnableVoltage | QuickStop;
	    	break;
	    case ReadyToSwitchOn :
	    case OperationEnabled :
	    	if (!power) {
                CW &= ~(FaultReset | EnableOperation);
                CW |= SwitchOn | EnableVoltage | QuickStop;
	    		break;
	    	}
	    case SwitchedOn :
	    	if (power) {
                CW &= ~(FaultReset);
                CW |= SwitchOn | EnableVoltage | QuickStop | EnableOperation;
	    	}
	    	break;
			//ssh_check
//	    case Fault :
//            /* TODO reset fault only when MC_Reset */
//	    	AxsPub.axis->DriveFault = 1;
//            CW &= ~(SwitchOn | EnableVoltage | QuickStop | EnableOperation);
//            CW |= FaultReset;
//	    	break;
	    default:
	    	break;
	}
	//ssh_add
	if(FaultFromStatusWord(*(AxsPub.StatusWord)) == SW_Fault)
		AxsPub.axis->DriveFault = 1;
	else{
		AxsPub.axis->DriveFault = 0;
		AxsPub.axis->DriveFaultReset = 0;
	}
	if(AxsPub.axis->DriveFaultReset){
		CW &= ~(SwitchOn | EnableVoltage | QuickStop | EnableOperation);
		CW |= FaultReset;
	}

	//ssh_add
	switch (HomingStatusWord(*(AxsPub.StatusWord))) {
		case HomingInProgress:
			break;
		case HomingNotRunning:
			break;
		case HomingNotReached:
			break;
		case Homing_Completed:
			if(!AxsPub.axis->HomingCompleted)
				AxsPub.axis->HomingCompleted = 1;
			break;
		case HomingErrorInVelo:
		case HomingErrorNotVelo:
			if(!AxsPub.axis->HomingCompleted)
				AxsPub.axis->HomingCompleted = 1;
			break;
		case HomingReserved:
			break;
	}
#undef FSA_sep 

	//ssh_add

	if(*(AxsPub.ModesOfOperation) == 0x06){
		IEC_BOOL homing = AxsPub.axis->HomingOperationStart;
		if(power){
			if (homing)
				CW |= Homing_OperationStart_Origin;
			else
				CW &= ~(Homing_OperationStart_Origin);
		}
		else{
			if (homing)
				CW |= Homing_OperationStart_Edit;
			else
				CW &= ~(EnableOperation);
		}

	}


	*(AxsPub.ControlWord) = CW;


	// CIA402 node modes of operation computation according to axis motion mode

	switch (AxsPub.axis->AxisMotionMode) {
		//ssh_add
		case mc_mode_hm:
			*(AxsPub.ModesOfOperation) = 0x06;
			break;
		case mc_mode_cst:
			*(AxsPub.ModesOfOperation) = 0x0a;
			break;
		case mc_mode_csv:
			*(AxsPub.ModesOfOperation) = 0x09;
			break;
		default:
			*(AxsPub.ModesOfOperation) = 0x08;
			break;
	}


	// Default variables publish
    *(AxsPub.TargetPosition) = AxsPub.axis->RawPositionSetPoint;
    *(AxsPub.TargetVelocity) = AxsPub.axis->RawVelocitySetPoint;
    *(AxsPub.TargetTorque) = AxsPub.axis->RawTorqueSetPoint;

	// Extra variables publish

}
