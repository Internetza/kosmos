PROGRAM Prog
  VAR
    CLOCK AT %QX0.0.2.28672.1 : BOOL;
    GREENLED AT %QX0.0.2.28688.1 : BOOL := TRUE;
    ControlWord AT %QW0.0.3.24640.0 : UINT;
    StatusWord AT %IW0.0.3.24641.0 : UINT;
    Mode AT %QB0.0.3.24672.0 : SINT;
    ModeDisplay AT %IB0.0.3.24673.0 : SINT;
    ActualPosition AT %ID0.0.3.24676.0 : DINT;
    ActualVelocity AT %ID0.0.3.24684.0 : DINT;
    TargetPosition AT %QD0.0.3.24698.0 : DINT;
    TargetVelocity AT %QD0.0.3.24831.0 : DINT;
    DigitalInputs AT %ID0.0.3.24829.0 : UDINT;
  END_VAR

  CLOCK := NOT CLOCK;
END_PROGRAM

PROGRAM test_MCL
  VAR
    CLOCK : UDINT;
  END_VAR
  VAR
    Axis AT %IW0.0.3.402 : AXIS_REF;
  END_VAR
  VAR
    switch : MC_Power;
    move1 : MC_MoveAbsolute;
    ActualPosition : DINT;
    Valid : BOOL;
    Pn : LREAL;
    Vn : LREAL;
    MC_MoveVelocity0 : MC_MoveVelocity;
    MC_MoveAbsolute0 : MC_MoveAbsolute;
    readpos : MC_ReadActualPosition;
    readspeed : MC_ReadParameter;
    MC_SetPosition0 : MC_SetPosition;
    ADD2_OUT : UDINT;
    readpos_Axis : AXIS_REF;
    GT7_OUT : BOOL;
    switch_Axis : AXIS_REF;
    MC_SetPosition0_Axis : AXIS_REF;
    move1_Axis : AXIS_REF;
    MC_MoveVelocity0_Axis : AXIS_REF;
    LT26_OUT : BOOL;
  END_VAR

  ADD2_OUT := ADD(CLOCK, 1);
  CLOCK := ADD2_OUT;
  readpos(Axis := Axis, Enable := BOOL#TRUE);
  Pn := readpos.Position;
  readpos_Axis := readpos.Axis;
  readspeed(Axis := readpos_Axis, Enable := BOOL#TRUE, ParameterNumber := 10);
  Vn := readspeed.Value;
  GT7_OUT := GT(CLOCK, 4000);
  switch(Axis := Axis, Enable := GT7_OUT);
  switch_Axis := switch.Axis;
  MC_SetPosition0(Axis := switch_Axis, Execute := switch.Status, Position := 0.0);
  MC_SetPosition0_Axis := MC_SetPosition0.Axis;
  move1(Axis := MC_SetPosition0_Axis, Execute := MC_SetPosition0.Done, Position := 1800.0, Velocity := 360.0, Acceleration := 360.0, Deceleration := 360.0);
  move1_Axis := move1.Axis;
  MC_MoveVelocity0(Axis := move1_Axis, Execute := move1.Done, Velocity := -360.0, Acceleration := 360.0, Deceleration := 360.0);
  MC_MoveVelocity0_Axis := MC_MoveVelocity0.Axis;
  LT26_OUT := LT(Pn, -1800.0);
  MC_MoveAbsolute0(Axis := MC_MoveVelocity0_Axis, Execute := LT26_OUT, Position := 0.0, Velocity := 360.0, Acceleration := 360.0, Deceleration := 360.0);
END_PROGRAM


CONFIGURATION conf
  VAR_GLOBAL
    GetTorqueLimit_0_0_3 : EtherLabGetTorqueLimit;
    SetTorqueLimit_0_0_3 : EtherLabSetTorqueLimit;
  END_VAR

  RESOURCE res ON PLC
    TASK main_task(INTERVAL := T#0.5ms,PRIORITY := 0);
    PROGRAM main_instance WITH main_task : Prog;
    PROGRAM test_MCL_instance WITH main_task : test_MCL;
  END_RESOURCE
END_CONFIGURATION
