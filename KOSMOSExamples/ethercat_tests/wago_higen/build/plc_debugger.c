/*
 * DEBUGGER code
 * 
 * On "publish", when buffer is free, debugger stores arbitrary variables 
 * content into, and mark this buffer as filled
 * 
 * 
 * Buffer content is read asynchronously, (from non real time part), 
 * and then buffer marked free again.
 *  
 * 
 * */
#include "iec_types_all.h"
#include "POUS.h"
#include "retain.h"
/*for memcpy*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define BUFFER_SIZE 5818

/* Atomically accessed variable for buffer state */
#define BUFFER_FREE 0
#define BUFFER_BUSY 1
static long buffer_state = BUFFER_FREE;

/* The buffer itself */
char debug_buffer[BUFFER_SIZE];

/* Buffer's cursor*/
static char* buffer_cursor = debug_buffer;

/* Retain's valuables */
static unsigned int retain_offset = 0;
static int retain_channel = 0;
#ifdef __XENO__
retain_struct_t *in_data;
int validate_flag;
#endif

/***
 * Declare programs 
 **/
extern PROG RES__MAIN_INSTANCE;
extern TEST_MCL RES__TEST_MCL_INSTANCE;

/***
 * Declare global variables from resources and conf 
 **/
extern ETHERLABGETTORQUELIMIT CONF__GETTORQUELIMIT_0_0_3;
extern ETHERLABSETTORQUELIMIT CONF__SETTORQUELIMIT_0_0_3;
extern PROG RES__MAIN_INSTANCE;
extern TEST_MCL RES__TEST_MCL_INSTANCE;

typedef const struct {
    void *ptr;
    __IEC_types_enum type;
} dbgvardsc_t;

static dbgvardsc_t dbgvardsc[] = {
{&(CONF__GETTORQUELIMIT_0_0_3.EN), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ENO), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.POS), INT_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.EXECUTE), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.DONE), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.BUSY), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ERROR), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ERRORID), WORD_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.TORQUELIMITPOS), UINT_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.TORQUELIMITNEG), UINT_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.EN), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.ENO), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.EXECUTE), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.POS), INT_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.INDEX), UINT_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.SUBINDEX), USINT_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.VARTYPE), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.ACK), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.VALID), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.VALUE), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.EN), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.ENO), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.TRIG), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.CODE), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.ACK), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.RESULT), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.STATE), DWORD_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.BUFFER), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.PREBUFFER), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.TRIGM1), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.TRIGGED), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.EN), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.ENO), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.TRIG), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.CODE), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.ACK), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.RESULT), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.STATE), DWORD_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.BUFFER), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.PREBUFFER), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.TRIGM1), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.TRIGGED), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.EXECUTE0), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.STATE), INT_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.EN), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.ENO), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.EXECUTE), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.POS), INT_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.INDEX), UINT_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.SUBINDEX), USINT_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.VARTYPE), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.ACK), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.VALID), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.VALUE), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.EN), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.ENO), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.TRIG), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.CODE), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.ACK), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.RESULT), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.STATE), DWORD_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.BUFFER), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.PREBUFFER), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.TRIGM1), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.TRIGGED), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.EN), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.ENO), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.TRIG), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.CODE), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.ACK), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.RESULT), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.STATE), DWORD_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.BUFFER), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.PREBUFFER), STRING_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.TRIGM1), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.TRIGGED), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.EXECUTE0), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.STATE), INT_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.RS0.EN), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.RS0.ENO), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.RS0.S), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.RS0.R1), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.RS0.Q1), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.AND16_OUT), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.STRING_TO_UINT12_OUT), UINT_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.OR18_OUT), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.AND19_OUT), BOOL_ENUM},
{&(CONF__GETTORQUELIMIT_0_0_3.STRING_TO_UINT13_OUT), UINT_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.EN), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ENO), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.POS), INT_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.EXECUTE), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.TORQUELIMITPOS), UINT_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.TORQUELIMITNEG), UINT_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.DONE), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.BUSY), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ERROR), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ERRORID), WORD_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.EN), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.ENO), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.EXECUTE), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.POS), INT_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.INDEX), UINT_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.SUBINDEX), USINT_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.VARTYPE), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.VALUE), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.ACK), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.ERROR), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.EN), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.ENO), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.TRIG), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.CODE), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.ACK), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.RESULT), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.STATE), DWORD_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.BUFFER), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.PREBUFFER), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.TRIGM1), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY0.TRIGGED), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.EN), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.ENO), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.TRIG), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.CODE), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.ACK), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.RESULT), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.STATE), DWORD_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.BUFFER), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.PREBUFFER), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.TRIGM1), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.PY1.TRIGGED), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.EXECUTE0), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD0.STATE), INT_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.EN), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.ENO), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.EXECUTE), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.POS), INT_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.INDEX), UINT_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.SUBINDEX), USINT_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.VARTYPE), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.VALUE), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.ACK), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.ERROR), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.EN), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.ENO), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.TRIG), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.CODE), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.ACK), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.RESULT), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.STATE), DWORD_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.BUFFER), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.PREBUFFER), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.TRIGM1), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY0.TRIGGED), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.EN), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.ENO), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.TRIG), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.CODE), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.ACK), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.RESULT), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.STATE), DWORD_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.BUFFER), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.PREBUFFER), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.TRIGM1), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.PY1.TRIGGED), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.EXECUTE0), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.ETHERLABSDOUPLOAD1.STATE), INT_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.RS0.EN), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.RS0.ENO), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.RS0.S), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.RS0.R1), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.RS0.Q1), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.UINT_TO_STRING25_OUT), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.UINT_TO_STRING12_OUT), STRING_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.AND16_OUT), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.OR18_OUT), BOOL_ENUM},
{&(CONF__SETTORQUELIMIT_0_0_3.AND19_OUT), BOOL_ENUM},
{&(RES__MAIN_INSTANCE.CLOCK), BOOL_O_ENUM},
{&(RES__MAIN_INSTANCE.GREENLED), BOOL_O_ENUM},
{&(RES__MAIN_INSTANCE.CONTROLWORD), UINT_O_ENUM},
{&(RES__MAIN_INSTANCE.STATUSWORD), UINT_P_ENUM},
{&(RES__MAIN_INSTANCE.MODE), SINT_O_ENUM},
{&(RES__MAIN_INSTANCE.MODEDISPLAY), SINT_P_ENUM},
{&(RES__MAIN_INSTANCE.ACTUALPOSITION), DINT_P_ENUM},
{&(RES__MAIN_INSTANCE.ACTUALVELOCITY), DINT_P_ENUM},
{&(RES__MAIN_INSTANCE.TARGETPOSITION), DINT_O_ENUM},
{&(RES__MAIN_INSTANCE.TARGETVELOCITY), DINT_O_ENUM},
{&(RES__MAIN_INSTANCE.DIGITALINPUTS), UDINT_P_ENUM},
{&(RES__TEST_MCL_INSTANCE.CLOCK), UDINT_ENUM},
{&(RES__TEST_MCL_INSTANCE.AXIS), INT_P_ENUM},
{&(RES__TEST_MCL_INSTANCE.SWITCH.EN), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.SWITCH.ENO), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.SWITCH.AXIS), INT_ENUM},
{&(RES__TEST_MCL_INSTANCE.SWITCH.ENABLE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.SWITCH.ENABLEPOSITIVE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.SWITCH.ENABLENEGATIVE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.SWITCH.STATUS), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.SWITCH.VALID), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.SWITCH.ERROR), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.SWITCH.ERRORID), WORD_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.EN), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.ENO), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.AXIS), INT_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.EXECUTE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.CONTINUOUSUPDATE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.POSITION), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.VELOCITY), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.ACCELERATION), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.DECELERATION), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.JERK), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.DONE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.BUSY), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.ACTIVE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.COMMANDABORTED), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.ERROR), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.ERRORID), WORD_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.EXECUTE0), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.DONE0), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1.ACTIVE0), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.ACTUALPOSITION), DINT_ENUM},
{&(RES__TEST_MCL_INSTANCE.VALID), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.PN), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.VN), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.EN), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.ENO), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.AXIS), INT_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.EXECUTE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.CONTINUOUSUPDATE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.VELOCITY), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.ACCELERATION), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.DECELERATION), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.JERK), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.INVELOCITY), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.BUSY), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.ACTIVE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.COMMANDABORTED), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.ERROR), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.ERRORID), WORD_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.EXECUTE0), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.INVELOCITY0), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0.ACTIVE0), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.EN), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.ENO), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.AXIS), INT_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.EXECUTE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.CONTINUOUSUPDATE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.POSITION), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.VELOCITY), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.ACCELERATION), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.DECELERATION), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.JERK), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.DONE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.BUSY), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.ACTIVE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.COMMANDABORTED), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.ERROR), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.ERRORID), WORD_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.EXECUTE0), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.DONE0), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEABSOLUTE0.ACTIVE0), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READPOS.EN), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READPOS.ENO), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READPOS.AXIS), INT_ENUM},
{&(RES__TEST_MCL_INSTANCE.READPOS.ENABLE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READPOS.VALID), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READPOS.BUSY), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READPOS.ERROR), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READPOS.ERRORID), WORD_ENUM},
{&(RES__TEST_MCL_INSTANCE.READPOS.POSITION), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READPOS.ENABLE0), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READSPEED.EN), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READSPEED.ENO), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READSPEED.AXIS), INT_ENUM},
{&(RES__TEST_MCL_INSTANCE.READSPEED.ENABLE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READSPEED.PARAMETERNUMBER), INT_ENUM},
{&(RES__TEST_MCL_INSTANCE.READSPEED.VALID), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READSPEED.BUSY), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READSPEED.ERROR), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READSPEED.ERRORID), WORD_ENUM},
{&(RES__TEST_MCL_INSTANCE.READSPEED.VALUE), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.READSPEED.ENABLE0), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_SETPOSITION0.EN), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_SETPOSITION0.ENO), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_SETPOSITION0.AXIS), INT_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_SETPOSITION0.EXECUTE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_SETPOSITION0.POSITION), LREAL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_SETPOSITION0.RELATIVE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_SETPOSITION0.DONE), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_SETPOSITION0.BUSY), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_SETPOSITION0.ERROR), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_SETPOSITION0.ERRORID), WORD_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_SETPOSITION0.EXECUTE0), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_SETPOSITION0.DONE0), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.ADD2_OUT), UDINT_ENUM},
{&(RES__TEST_MCL_INSTANCE.READPOS_AXIS), INT_ENUM},
{&(RES__TEST_MCL_INSTANCE.GT7_OUT), BOOL_ENUM},
{&(RES__TEST_MCL_INSTANCE.SWITCH_AXIS), INT_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_SETPOSITION0_AXIS), INT_ENUM},
{&(RES__TEST_MCL_INSTANCE.MOVE1_AXIS), INT_ENUM},
{&(RES__TEST_MCL_INSTANCE.MC_MOVEVELOCITY0_AXIS), INT_ENUM},
{&(RES__TEST_MCL_INSTANCE.LT26_OUT), BOOL_ENUM}
};

typedef void(*__for_each_variable_do_fp)(dbgvardsc_t*);
void __for_each_variable_do(__for_each_variable_do_fp fp)
{
    int i;
    
    for(i = 0; i < sizeof(dbgvardsc)/sizeof(dbgvardsc_t); i++){
        dbgvardsc_t *dsc = &dbgvardsc[i];
        if(dsc->type != UNKNOWN_ENUM) 
            (*fp)(dsc);
    }
}

#define __Unpack_case_t(TYPENAME) \
        case TYPENAME##_ENUM :\
            *flags = ((__IEC_##TYPENAME##_t *)varp)->flags;\
            forced_value_p = *real_value_p = &((__IEC_##TYPENAME##_t *)varp)->value;\
            break;

#define __Unpack_case_p(TYPENAME)\
        case TYPENAME##_O_ENUM :\
            *flags = __IEC_OUTPUT_FLAG;\
        case TYPENAME##_P_ENUM :\
            *flags |= ((__IEC_##TYPENAME##_p *)varp)->flags;\
            *real_value_p = ((__IEC_##TYPENAME##_p *)varp)->value;\
            forced_value_p = &((__IEC_##TYPENAME##_p *)varp)->fvalue;\
            break;

void* UnpackVar(dbgvardsc_t *dsc, void **real_value_p, char *flags)
{
    void *varp = dsc->ptr;
    void *forced_value_p = NULL;
    *flags = 0;
    /* find data to copy*/
    switch(dsc->type){
        __ANY(__Unpack_case_t)
        __ANY(__Unpack_case_p)
    default:
        break;
    }
    if (*flags & __IEC_FORCE_FLAG)
        return forced_value_p;
    return *real_value_p;
}

void Remind(unsigned int offset, unsigned int count, void * p, int ch);

void RemindIterator(dbgvardsc_t *dsc)
{
    void *real_value_p = NULL;
    char flags = 0;
    UnpackVar(dsc, &real_value_p, &flags);

    if(flags & __IEC_RETAIN_FLAG){
        USINT size = __get_type_enum_size(dsc->type);
        /* compute next cursor positon*/
        unsigned int next_retain_offset = retain_offset + size;
        /* if buffer not full */
        Remind(retain_offset, size, real_value_p, retain_channel);
        /* increment cursor according size*/
        retain_offset = next_retain_offset;

    }
}

extern int CheckRetainBuffer(int ch);

void __init_debug(void)
{
    /* init local static vars */
    buffer_cursor = debug_buffer;
	buffer_state = BUFFER_FREE;
	#ifdef __XENO__
	/* init retain value */
	/*
    retain_offset = 0;
	retain_channel = retain_channel_open();
	in_data = (retain_struct_t *)malloc(sizeof(retain_struct_t));
	in_data -> size = 0;
	in_data -> value = (uint64_t *)malloc(sizeof(uint64_t));
	*/
	#endif
    /* Iterate over all variables to fill debug buffer */
	/*
    if(CheckRetainBuffer(retain_channel))
        __for_each_variable_do(RemindIterator);
	*/
    //__for_each_variable_do(RemindIterator);
}

extern void InitiateDebugTransfer(void);

extern unsigned long __tick;

void __cleanup_debug(void)
{
    buffer_cursor = debug_buffer;
    InitiateDebugTransfer();
	#ifdef __XENO__
	//retain_channel_close(retain_channel);
	#endif
}

void __retrieve_debug(void)
{
}

void Retain(unsigned int offset, unsigned int count, void * p, int ch);

inline void BufferIterator(dbgvardsc_t *dsc, int do_debug)
{
    void *real_value_p = NULL;
    void *visible_value_p = NULL;
    char flags = 0;

    visible_value_p = UnpackVar(dsc, &real_value_p, &flags);

    if(flags & ( __IEC_DEBUG_FLAG | __IEC_RETAIN_FLAG)){
        USINT size = __get_type_enum_size(dsc->type);
        if(flags & __IEC_DEBUG_FLAG){
            /* copy visible variable to buffer */;
            if(do_debug){
                /* compute next cursor positon.
                   No need to check overflow, as BUFFER_SIZE
                   is computed large enough */
                if(dsc->type == STRING_ENUM){
                    /* optimization for strings */
                    size = ((STRING*)visible_value_p)->len + 1;
                }
                char* next_cursor = buffer_cursor + size;
                /* copy data to the buffer */
                memcpy(buffer_cursor, visible_value_p, size);
                /* increment cursor according size*/
                buffer_cursor = next_cursor;
            }
            /* re-force real value of outputs (M and Q)*/
            if((flags & __IEC_FORCE_FLAG) && (flags & __IEC_OUTPUT_FLAG)){
                memcpy(real_value_p, visible_value_p, size);
            }
        }

        if(flags & __IEC_RETAIN_FLAG){
            /* compute next cursor positon*/
            unsigned int next_retain_offset = retain_offset + size;
            /* if buffer not full */
            //Retain(retain_offset, size, real_value_p, retain_channel);
            Retain(retain_offset, size, real_value_p, 0);
            /* increment cursor according size*/
            retain_offset = next_retain_offset;
        }
    }
}

void DebugIterator(dbgvardsc_t *dsc){
    BufferIterator(dsc, 1);
}

void RetainIterator(dbgvardsc_t *dsc){
    BufferIterator(dsc, 0);
}

extern void PLC_GetTime(IEC_TIME*);
extern int TryEnterDebugSection(void);
extern long AtomicCompareExchange(long*, long, long);
extern long long AtomicCompareExchange64(long long* , long long , long long);
extern void LeaveDebugSection(void);
extern void ValidateRetainBuffer(int ch);
extern void InValidateRetainBuffer(int ch);

void __publish_debug(void)
{
    retain_offset = 0;
    //ValidateRetainBuffer(retain_channel);
    ValidateRetainBuffer(0);
    
    // Check there is no running debugger re-configuration
    if(TryEnterDebugSection()){
        // Lock buffer
        long latest_state = AtomicCompareExchange(
            &buffer_state,
            BUFFER_FREE,
            BUFFER_BUSY);
            
        // If buffer was free
        if(latest_state == BUFFER_FREE)
        {
            // Reset buffer cursor 
            buffer_cursor = debug_buffer;
            // Iterate over all variables to fill debug buffer 
            __for_each_variable_do(DebugIterator);

            // Leave debug section,
            // Trigger asynchronous transmission 
            // (returns immediately)
            InitiateDebugTransfer(); // size
        }else{
            // when not debugging, do only retain
            __for_each_variable_do(RetainIterator);
        }
        LeaveDebugSection();
    }else{
        // when not debugging, do only retain 
        __for_each_variable_do(RetainIterator);
    }
    
    //InValidateRetainBuffer(retain_channel);
    InValidateRetainBuffer(0);
}

#define __RegisterDebugVariable_case_t(TYPENAME) \
        case TYPENAME##_ENUM :\
            ((__IEC_##TYPENAME##_t *)varp)->flags |= flags;\
            if(force)\
             ((__IEC_##TYPENAME##_t *)varp)->value = *((TYPENAME *)force);\
            break;
#define __RegisterDebugVariable_case_p(TYPENAME)\
        case TYPENAME##_P_ENUM :\
            ((__IEC_##TYPENAME##_p *)varp)->flags |= flags;\
            if(force)\
             ((__IEC_##TYPENAME##_p *)varp)->fvalue = *((TYPENAME *)force);\
            break;\
        case TYPENAME##_O_ENUM :\
            ((__IEC_##TYPENAME##_p *)varp)->flags |= flags;\
            if(force){\
             ((__IEC_##TYPENAME##_p *)varp)->fvalue = *((TYPENAME *)force);\
             *(((__IEC_##TYPENAME##_p *)varp)->value) = *((TYPENAME *)force);\
            }\
            break;

void RegisterDebugVariable(int idx, void* force)
{
    if(idx  < sizeof(dbgvardsc)/sizeof(dbgvardsc_t)){
        unsigned char flags = force ?
            __IEC_DEBUG_FLAG | __IEC_FORCE_FLAG :
            __IEC_DEBUG_FLAG;
        dbgvardsc_t *dsc = &dbgvardsc[idx];
        void *varp = dsc->ptr;
        switch(dsc->type){
            __ANY(__RegisterDebugVariable_case_t)
            __ANY(__RegisterDebugVariable_case_p)
        default:
            break;
        }
    }
}

#define __ResetDebugVariablesIterator_case_t(TYPENAME) \
        case TYPENAME##_ENUM :\
            ((__IEC_##TYPENAME##_t *)varp)->flags &= ~(__IEC_DEBUG_FLAG|__IEC_FORCE_FLAG);\
            break;

#define __ResetDebugVariablesIterator_case_p(TYPENAME)\
        case TYPENAME##_P_ENUM :\
        case TYPENAME##_O_ENUM :\
            ((__IEC_##TYPENAME##_p *)varp)->flags &= ~(__IEC_DEBUG_FLAG|__IEC_FORCE_FLAG);\
            break;

void ResetDebugVariablesIterator(dbgvardsc_t *dsc)
{
    /* force debug flag to 0*/
    void *varp = dsc->ptr;
    switch(dsc->type){
        __ANY(__ResetDebugVariablesIterator_case_t)
        __ANY(__ResetDebugVariablesIterator_case_p)
    default:
        break;
    }
}

void ResetDebugVariables(void)
{
    __for_each_variable_do(ResetDebugVariablesIterator);
}

void FreeDebugData(void)
{
    /* atomically mark buffer as free */
    AtomicCompareExchange(
        &buffer_state,
        BUFFER_BUSY,
        BUFFER_FREE);
}

int WaitDebugData(unsigned long *tick);
/* Wait until debug data ready and return pointer to it */
int GetDebugData(unsigned long *tick, unsigned long *size, void **buffer){
    int wait_error = WaitDebugData(tick);
    if(!wait_error){
        *size = buffer_cursor - debug_buffer;
        *buffer = debug_buffer;
    }
    return wait_error;
}

/************************** Modbus Section **********************************/

int *idx_list;
// The buffer itself
char modbus_read_buffer[20000];
char modbus_write_buffer[20000];
int size_sum = 0;

// Buffer's cursor
static char* modbus_read_buffer_cursor = modbus_read_buffer;
static char* modbus_write_buffer_cursor = modbus_write_buffer;
extern void WriteModbusData(void *buffer, int size);
extern int ReadModbusData(void *buffer, int size);

void* UnpackVar_test(dbgvardsc_t *dsc, void **real_value_p, char *flags)
{
    void *varp = dsc->ptr;
    void *forced_value_p = NULL;
    *flags = 0;
    switch(dsc->type){
        __ANY(__Unpack_case_t)
        __ANY(__Unpack_case_p)
    default:
        break;
    }
    //if (*flags & __IEC_FORCE_FLAG)
    //    return forced_value_p;
    return *real_value_p;
}

void __for_each_modbus_read_variable_do(int length, int data[])
{
    int i;
    void *real_value_p = NULL;
    void *visible_value_p = NULL;
    char flags = 0;
    size_sum = 0;

    for(i = 0; i < length; i++){
        dbgvardsc_t *dsc = &dbgvardsc[data[i]];
        if(dsc->type != UNKNOWN_ENUM){ 
            visible_value_p = UnpackVar_test(dsc, &real_value_p, &flags);
        }
        USINT size = __get_type_enum_size(dsc->type);

        if(dsc->type == STRING_ENUM){
            // optimization for strings 
            size = ((STRING*)visible_value_p)->len + 1;
        }
        char* next_cursor = modbus_write_buffer_cursor + size;
        size_sum = size_sum + size;
        // copy data to the buffer 
        memcpy(modbus_write_buffer_cursor, visible_value_p, size);
        // increment cursor according size
        modbus_write_buffer_cursor = next_cursor;
    }
}

void __for_each_modbus_write_variable_do(void)
{
    void *real_value_p = NULL;
    void *visible_value_p = NULL;
    char flags = 0;
    
    int *idx = (int *)malloc(sizeof(int));
    memcpy(idx, modbus_read_buffer_cursor, sizeof(int));

    /*
    float *idx = (float *)malloc(sizeof(float));
    memcpy(idx, modbus_read_buffer_cursor, sizeof(float));
    SLOGF(LOG_CRITICAL, "idx %d\n", (int)*idx);
    //*/

    dbgvardsc_t *dsc = &dbgvardsc[(int)(*idx)];
    if(dsc->type != UNKNOWN_ENUM){ 
        visible_value_p = UnpackVar_test(dsc, &real_value_p, &flags);
    }
    USINT size = __get_type_enum_size(dsc->type);

    if(dsc->type == STRING_ENUM){
        // optimization for strings 
        size = ((STRING*)visible_value_p)->len + 1;
    }
    char* next_cursor = modbus_read_buffer_cursor + sizeof(int);
    // copy data to the buffer  
    if(!((flags & __IEC_FORCE_FLAG) && (flags & __IEC_OUTPUT_FLAG))){
        memcpy(visible_value_p, next_cursor, size);
    }
}

void __retrieve_modbus(void)
{
    int res;
    // Read Variable from Pipe
    res = ReadModbusData(modbus_read_buffer, 1024);
    // Reset buffer cursor 
    modbus_read_buffer_cursor = modbus_read_buffer;
    // Iterate about ModbusRTU variables to overwrite IDE Variable
    if (res){
        __for_each_modbus_write_variable_do();
    }
}

void __publish_modbus(int length, int data[])
{
    // Reset buffer cursor 
    modbus_write_buffer_cursor = modbus_write_buffer;
    // Iterate about ModbusRTU variables to fill debug buffer 
    __for_each_modbus_read_variable_do(length, data);
    // Write Variable to Pipe
    WriteModbusData(modbus_write_buffer, size_sum);

}
